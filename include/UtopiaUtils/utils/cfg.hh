#ifndef UTOPIA_UTILS_CFG_HH
#define UTOPIA_UTILS_CFG_HH

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <type_traits>

#include <armadillo>
#include <yaml-cpp/yaml.h>

#include <utopia/core/exceptions.hh>
#include <utopia/data_io/cfg_utils.hh>

#include "types.hh"
#include "meta.hh"
#include "str_tools.hh"


namespace Utopia::Utils {

using namespace Utopia::Utils::tags;

namespace _details {
    /// Helper function for recursive_setitem
    /** Expects an (already deep-copied) node that is then recursively iterated
      * through along the key sequence. For the last key in the sequence, the
      * value is being set.
      */
    template<class T>
    Config __recursive_setitem (Config d,
                                std::list<std::string> key_sequence,
                                const T& val)
    {
        const auto key = key_sequence.front();
        key_sequence.pop_front();

        if (key_sequence.empty()) {
            d[key] = val;
            return d;
        }

        if (not d[key]) {
            d[key] = Config{};
        }
        d[key] = __recursive_setitem(d[key], key_sequence, val);
        return d;
    }
}


using Utopia::DataIO::get_as_arma_vec;

/// For fixed-size armadillo vectors, reads an input from the configuration
template<class CVecT, class... Args>
CVecT get_as_vec (Args&&... args) {
    return get_as_arma_vec<CVecT, CVecT::n_elem>(std::forward<Args>(args)...);
}


/// Recursively retrieve an element from the configuration tree
/**
  * Actually uses a loop internally to avoid recursion... ¯\_(ツ)_/¯
  *
  * \note   Only works with string-like keys, i.e. can't access sequences
  *
  * \TODO   Find an implementation that does not require YAML::Clone ...
  *         Without it, always had mutability issues and side effects. Ungood.
  */
Config recursive_getitem (const Config& d,
                          const std::vector<std::string>& key_sequence)
{
    Config rv = YAML::Clone(d);
    for (const auto& key : key_sequence ) {
        try {
            rv = get_as<Config>(key, rv);
        }
        catch (KeyError& err) {
            throw KeyError(key, rv,
                           "recursive_getitem failed for key or key sequence '"
                           + join(key_sequence, " -> ") + "'!");
        }
    }
    return rv;
}

/// Overload for recursive_getitem that splits a string into a key sequence
/** Uses *any* of the given characters in `delims` to split.
  */
auto recursive_getitem (const Config& d, const std::string& key_sequence,
                        const std::string& delims = ".")
{
    return recursive_getitem(d, split(key_sequence, delims));
}


/// Recursively sets an element in a configuration tree
/** This also creates all intermediate segments that may be missing.
  *
  * As the YAML::Node::operator[] does not return references, this internally
  * has to create a (deep) copy of the given node `d`. After recursing through
  * it and setting the value, it will finally assign to the `d`.
  *
  * \note   Only works with string-like keys, i.e. can't access sequences.
  */
template<class T>
void recursive_setitem (Config& d,
                        const std::list<std::string>& key_sequence,
                        const T& val)
{
    if (key_sequence.empty()) {
        throw std::invalid_argument("Key sequence for recursive_setitem may "
                                    "not be empty!");
    }
    d = _details::__recursive_setitem(YAML::Clone(d), key_sequence, val);
}

/// Overload for recursive_setitem that splits a string into a key sequence
/** Uses *any* of the given characters in `delims` to split.
  */
template<class T>
void recursive_setitem (Config& d,
                        const std::string& key_sequence,
                        const T& val,
                        const std::string& delims = ".")
{
    recursive_setitem(d, split<std::list>(key_sequence, delims), val);
}


/// Recursively updates the given configuration tree
/**
  *
  * \warning The key needs to be string-representable; this means that the
  *          recursive update will not work when using non-scalar
  *
  * \tparam copy_mode   If the deep_copy tag is given, ``d`` is cloned prior
  *                     to updating values. This is the default, because it
  *                     can lead to undesired behaviour otherwise.
  *                     Note that the cloning deep-copies only the Config tree,
  *                     not its content; thus, mutable leaves may still remain
  *                     but there will be no side effects on changing the
  *                     mappings and sequences inside the Config tree.
  * \tparam Key         The key type to use during updating. The default is
  *                     std::string here, because it's the universal native
  *                     type for representing any YAML entities. This type is
  *                     needed because entity lookup is not possible
  *
  * \param  d           The Config tree to recursively update
  * \param  u           The tree to use for the recursive update
  */
template<class copy_mode = deep_copy, class Key = std::string>
Config recursive_update (Config d, const Config& u) {
    static_assert(is_any<copy_mode, shallow_copy, deep_copy>(),
                  "Unsupported copy_mode! Can be: shallow_copy, deep_copy");

    if constexpr (is_same<copy_mode, deep_copy>()) {
        d = YAML::Clone(d);
    }

    Key k;

    for (auto& kv_pair : u) {
        // For key lookup to work with operator[], it need be a native type
        /** FIXME Node Node::operator[](const Node& key) *is* available; this
          *       Key hack should not be necessary! However, naively invoking
          *       it leads to duplicate elements somehow ...
          */
        try {
            k = kv_pair.first.template as<Key>();
        }
        catch (std::exception& exc) {
            std::cerr << "Failed representing key node as the specified "
                      << "native Key type (default: std::string):" << std::endl
                      << kv_pair.first << std::endl << std::endl
                      << "Failed recursively updating" << std::endl
                      << d << std::endl
                      << "... with ..." << std::endl
                      << u << std::endl;
            throw;
        }

        const auto& v = kv_pair.second;

        if (v.IsMap()) {
            // Is a mapping, continue recursively. As the deep copy already
            // happened at recursion level zero, can use shallow copy now.
            d[k] = recursive_update<shallow_copy>(d[k], v);
        }
        else {
            // Not a mapping ==> can't continue recursion ==> update value
            d[k] = v;
        }
    }
    return d;
}


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_CFG_HH
