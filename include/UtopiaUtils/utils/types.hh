#ifndef UTOPIA_UTILS_TYPES_HH
#define UTOPIA_UTILS_TYPES_HH

#include <memory>
#include <vector>
#include <tuple>
#include <type_traits>

#include <armadillo>

#include <utopia/data_io/cfg_utils.hh>

#include "tags.hh"


namespace Utopia::Utils {

// -- General type definitions ------------------------------------------------

/// The configuration type used throughout this model
using Config = Utopia::DataIO::Config;

/// Type of the ID used for identification of entities
using IDType = unsigned int;




} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_TYPES_HH
