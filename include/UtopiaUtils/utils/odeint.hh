#ifndef UTOPIA_UTILS_ODEINT_HH
#define UTOPIA_UTILS_ODEINT_HH

#include <memory>
#include <vector>
#include <limits>
#include <cmath>

#include <boost/numeric/odeint.hpp>
#include <fmt/core.h>

#include <utopia/core/type_traits.hh>

#include "types.hh"
#include "iterators.hh"



namespace Utopia::Utils {

namespace odeint = boost::numeric::odeint;

const double _NaN = std::numeric_limits<double>::quiet_NaN();


/// A base class for an ODEIntegrator System, storing the system's state
template<class _State = std::vector<double>>
struct BaseODESystem {
    using State = _State;

private:
    State state;

public:
    BaseODESystem () = default;

    BaseODESystem (State state)
    :
        state(state)
    {}

    void set_state (State new_state) {
        state = new_state;
    }

    State& get_state () {
        return state;
    }

    const State& get_state () const {
        return state;
    }

    auto size () const {
        return state.size();
    }

    virtual void operator() (const State&, State&, double) = 0;
};


/// An ODE system that has the system function defined as a lambda
template<
    class State,
    class Func = std::function<void(const State&, State&, double)>
>
struct SimpleODESystem : public BaseODESystem<State> {
    Func func;

    SimpleODESystem (State state, Func func)
    :
        BaseODESystem<State>(state)
    ,   func(func)
    {}

    void operator() (const State& x, State& dxdt, double t) {
        func(x, dxdt, t);
    }
};

/// Factory for simple lambda-based ODE system
template<class State, class Func>
auto make_ODESystem (State state, Func func) {
    return SimpleODESystem(state, func);
};


/// Whether to use on-the-fly system generation or not
/** If on, the ODEIntegrator will not work directly with the system object but
  * will instead pass a _generated_ system object, e.g. a lambda, to the
  * boost::odeint functions.
  *
  * The boost::odeint functions frequently call the System's copy constructor.
  * The larger the ODESystem object gets, the more overhead is produced by
  * copying the object around.
  * In such a scenario, it can be useful to let the ODESystem generate a *new*
  * system object which is more light-weight, e.g. a lambda.
  */
enum class SystemGeneration { Off, On };



/// Wrapper for boost::numeric::odeint procedures with controlled steppers
/** This class is meant for repeatedly performing integrations with a set of
  * fixed integration parameters and infrastructure. It wraps a subset of the
  * functionality of boost::numeric::odeint, exposing it in an object-oriented
  * interface.
  *
  * It works mainly on the ODESystem class, which holds the update function
  * as well as the system's state. To work with the ODEIntegrator, one way is
  * to use the make_ODESystem factory to store the state and the function in
  * a struct. Another way is to derive from BaseODESystem and override the
  * virtual operator() method with the desired update function.
  *
  * With SystemGeneration::On, it is expected that there is an operator()
  * implemented that returns the system that is then to be used.
  */
template<
    class System,
    SystemGeneration sysgen = SystemGeneration::Off,
    class ErrorStepper
        = odeint::runge_kutta_cash_karp54<typename System::State>,
    class ControlledStepper
        = odeint::controlled_runge_kutta<ErrorStepper>
>
class ODEIntegrator {
    using State = typename System::State;

    System system;
    ControlledStepper controlled_stepper;

    double time;
    double default_dt;


public:
    /// Construct integrator using parameters from a configuration
    /** This constructor sets up an controlled error stepper and passes the
      * parameters to it via the configuration.
      */
    ODEIntegrator (
        System system,
        const Config& cfg
    ):
        ODEIntegrator(
            system,
            odeint::make_controlled(
                get_as<double>("eps_abs", cfg),
                get_as<double>("eps_rel", cfg),
                ErrorStepper()
            ),
            get_as<double>("default_dt", cfg),
            get_as<double>("t0", cfg, 0.)
        )
    {}

    /// Construct the integrator with some initial state
    ODEIntegrator (
        System system,
        ControlledStepper controlled_stepper = {},
        double default_dt = 0.001,
        double t0 = 0.
    ):
        system(system)
    ,   controlled_stepper(controlled_stepper)

    ,   time(t0)
    ,   default_dt(default_dt)
    {}

    // -- Getters & Setters ---------------------------------------------------

    void set_state (const State& new_state) {
        system.set_state(new_state);
    }

    State& get_state () {
        return system.get_state();
    }

    const State& get_state () const {
        return system.get_state();
    }

    void set_system (System new_system) {
        system = new_system;
    }

    System& get_system () {
        return system;
    }

    const System& get_system () const {
        return system;
    }

    void set_controlled_stepper (ControlledStepper new_controlled_stepper) {
        controlled_stepper = new_controlled_stepper;
    }

    void set_time (double new_time) {
        time = new_time;
    }

    const double& get_time () const {
        return time;
    }

    void set_default_dt (double new_default_dt) {
        default_dt = new_default_dt;
    }

    const double& get_default_dt () const {
        return default_dt;
    }

    static constexpr bool using_system_generation () {
        return (sysgen == SystemGeneration::On);
    }

    // -- Integration Methods -------------------------------------------------

    /// Integrate the system with the convenience integrate routine
    /** TODO
      *
      * \todo Error handling, i.e. resetting state? Do sth. with steps?!
      */
    auto integrate (double t, double t1 = _NaN, double dt = _NaN) {
        _determine_times(t, t1, dt);

        std::size_t steps;
        if constexpr (sysgen == SystemGeneration::Off) {
            steps = odeint::integrate(
                system, system.get_state(), t, t1, dt
            );
        }
        else {
            steps = odeint::integrate(
                system(), system.get_state(), t, t1, dt
            );
        }

        time = t1;

        return steps;
    }

    /// Integrate the system with a constant dt
    auto integrate_const (double t, double t1 = _NaN, double dt = _NaN) {
        _determine_times(t, t1, dt);

        std::size_t steps;
        if constexpr (sysgen == SystemGeneration::Off) {
            steps = odeint::integrate_const(
                controlled_stepper, system, system.get_state(), t, t1, dt
            );
        }
        else {
            steps = odeint::integrate_const(
                controlled_stepper, system(), system.get_state(), t, t1, dt
            );
        }

        time = t1;

        return steps;
    }

    /// Integrate the system using the controlled stepper
    auto integrate_adaptive (double t, double t1 = _NaN, double dt = _NaN) {
        _determine_times(t, t1, dt);

        std::size_t steps;
        if constexpr (sysgen == SystemGeneration::Off) {
            steps = odeint::integrate_adaptive(
                controlled_stepper, system, system.get_state(), t, t1, dt
            );
        }
        else {
            steps = odeint::integrate_adaptive(
                controlled_stepper, system(), system.get_state(), t, t1, dt
            );
        }

        time = t1;

        return steps;
    }


private:
    /// Parses the arguments to the integration methods
    /** This allows a more compact syntax and "continuing" the previous
      * integrations from the last-used time value.
      *
      * Behaviour:
      *     - If `dt` is NaN, use `default_dt`
      *     - If `t1` is NaN, interpret `t` as _length_ of iteration, i.e. use
      *       `time + t` as `t1` and `time` as
      */
    void _determine_times (double& t, double& t1, double& dt) const {
        if (std::isnan(t1)) {
            if (t < 0.) {
                throw std::invalid_argument("t < 0"); // TODO
            }
            t1 = time + t;
            t = time;
        }

        if (std::isnan(dt)) {
            dt = default_dt;
            // TODO If possible, continue from last-used adaptive dt instead?!
        }
    }
};




} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_ODEINT_HH
