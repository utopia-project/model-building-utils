#ifndef UTOPIA_UTILS_NW_FILTER_HH
#define UTOPIA_UTILS_NW_FILTER_HH

#include <string>
#include <unordered_map>
#include <type_traits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>

#include "graph_iterators.hh"


namespace Utopia::Utils::NW {

enum class FilterMode : char {
    unfiltered = 0,           /// No filters in place
    edges_only = 1,           /// Only edge filter in place
    vertices_only = 2,        /// Only vertex filter in place
    edges_and_vertices = 3,   /// Edge and vertex filter in place
};

inline const auto keep_all = [](auto){ return true; };
inline const auto keep_none = [](auto){ return false; };


/// A NetworkFilter gathers an edge and vertex predicate for network filtering
/** Additionally, the `mode` carries information on which filters will actually
  * have an effect. This can avoid redundant `keep_all` filter evaluation.
  */
template<class _NW = boost::adjacency_list<>>
struct NetworkFilter {
    /// The network type
    using NW = _NW;

    /// The edge descriptor type
    using EdgeDesc = typename boost::graph_traits<NW>::edge_descriptor;

    /// The vertex descriptor type
    using VertexDesc = typename boost::graph_traits<NW>::vertex_descriptor;

    /// Edge predicate type
    using EdgePred = std::function<bool(EdgeDesc)>;

    /// Vertex predicate type
    using VertexPred = std::function<bool(VertexDesc)>;


    // .. Members .............................................................
    /// The edge predicate; if evaluated to true, the edge is used
    const EdgePred edge_filter;

    /// The vertex predicate; if evaluated to true, the vertex is used
    const VertexPred vertex_filter;

    /// The filtering mode, i.e. which filters are set to be used
    const FilterMode mode;


    // .. Constructors ........................................................

    NetworkFilter () = delete;

    NetworkFilter (const NW&,
                   EdgePred edge_pred,
                   VertexPred vertex_pred,
                   FilterMode mode = FilterMode::edges_and_vertices)
    :   edge_filter(edge_pred)
    ,   vertex_filter(vertex_pred)
    ,   mode(mode)
    {}

    /// Named constructor for NetworkFilter without any actual filters
    template<class NW>
    inline static NetworkFilter<NW>
    unfiltered (const NW& nw)
    {
        return {nw, keep_all, keep_all, FilterMode::unfiltered};
    }

    /// Named constructor for edge-only NetworkFilter
    template<class NW>
    inline static NetworkFilter<NW>
    edges (const NW& nw, EdgePred edge_pred)
    {
        return {nw, edge_pred, keep_all, FilterMode::edges_only};
    }

    /// Named constructor for edge-only NetworkFilter
    template<class NW>
    inline static NetworkFilter<NW>
    vertices (const NW& nw, VertexPred vertex_pred)
    {
        return {nw, keep_all, vertex_pred, FilterMode::vertices_only};
    }

    // .. Getters .............................................................

    /// Whether an edge filter is attached
    bool has_edge_filter () const {
        return static_cast<char>(mode) % 2 == 1;
    }

    /// Whether a vertex filter is attached
    bool has_vertex_filter () const {
        return static_cast<char>(mode) >= 2;
    }
};



/// Create a FilteredRange from a NWFilter
template<class NW, class It>
auto make_filtered_range (
    const NetworkFilter<NW>& nw_filter,
    It base, It end = {}
){
    using NWFilter = NetworkFilter<NW>;

    static_assert(
            std::is_same<It, typename NW::vertex_iterator>()
        or  std::is_same<It, typename NW::adjacency_iterator>()
        or  std::is_same<It, typename NW::edge_iterator>()
        or  std::is_same<It, typename NW::out_edge_iterator>()
        or  std::is_same<It, typename NW::in_edge_iterator>()
        ,   "FilteredRange creation from NWFilter needs vertex or edge "
        "iterators that match the associated network type!"
    );

    // Distinguish by iterator type
    if constexpr (   std::is_same<It, typename NW::vertex_iterator>()
                  or std::is_same<It, typename NW::adjacency_iterator>())
    {
        using Pred = typename NWFilter::VertexPred;

        return FilteredRange<Pred, It>(nw_filter.vertex_filter,
                                       base, end,
                                       nw_filter.has_vertex_filter());
    }
    else {
        using Pred = typename NWFilter::EdgePred;

        return FilteredRange<Pred, It>(nw_filter.edge_filter,
                                       base, end,
                                       nw_filter.has_edge_filter());
    }
}



} // namespace Utopia::Utils::NW

#endif // UTOPIA_UTILS_NW_FILTER_HH
