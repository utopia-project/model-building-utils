#ifndef UTOPIA_UTILS_GRAPH_ITERATORS_HH
#define UTOPIA_UTILS_GRAPH_ITERATORS_HH

#include <iterator>
#include <type_traits>

#include <boost/range/iterator_range.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <utopia/core/graph/iterator.hh>

#include "contains.hh"
#include "iterators.hh"


namespace Utopia::Utils {


/// Generates a transformed graph iterator range
/** This uses boost::transform_iterator to generate a transformed iterator from
  * the specified graph iterators. The transformation is specified via a
  * callable, which receives the edge or vertex descriptor and may return any
  * object it desires.
  *
  * The iterator range returned by this function can be used in range-based
  * for loops, yay!
  */
template<IterateOver iterate_over,
         class Callable,
         class G,
         class... Args>
auto make_transformed_graph_iterator_range (Callable&& transformator,
                                            G& g,
                                            Args&&... args)
{
    using Utopia::GraphUtils::iterator_pair;

    auto [d_begin, d_end] =
        iterator_pair<iterate_over>(std::forward<Args>(args)..., g);

    auto entity_begin =
        boost::make_transform_iterator(d_begin, transformator);
    auto entity_end =
        boost::make_transform_iterator(d_end, transformator);

    return boost::make_iterator_range(std::move(entity_begin),
                                      std::move(entity_end));
}



/// Allows range iteration using a predicate for filtering
/** Only elements that evaluate to true via the predicate are elements of
  * iterations done with this iterator.
  *
  * This is a bit faster than boost::adaptors::filtered but importantly, it
  * gives more control over the implementation ...
  *
  * \TODO const iterator implementation
  */
template<class FilterPredicate, class BaseIterator>
class FilteredRange : public BaseIterator {
    using Self = FilteredRange<FilterPredicate, BaseIterator>;

    FilterPredicate _pred;
    BaseIterator _base;
    BaseIterator _end;
    const bool _enabled;

public:

    /// Demote to forward iterator, regardless of BaseIterator category
    using iterator_category = std::forward_iterator_tag;

    FilteredRange () = default;
    FilteredRange (
        FilterPredicate pred,
        BaseIterator base,
        BaseIterator end = {},
        bool enabled = true
    )
    :   BaseIterator(base)
    ,   _pred(pred)
    ,   _base(base)
    ,   _end(end)
    ,   _enabled(enabled)
    {
        // Move to the first element where the predicate evaluates to true (and
        // not beyond the end)
        while (*this != _end and not _pred(**this)) {
            ++*this;
        }
        // TODO include enabled
    }

    /// Prefix increment
    FilteredRange& operator++ () {
        // Iterate until the predicate returns true (or the end is reached)
        // TODO include enabled
        do {
            BaseIterator::operator++();
        } while (*this != _end and not _pred(**this));

        return *this;
    }

    /// Postfix increment, creating a copy...
    FilteredRange operator++ (int) {
        FilteredRange copy = *this;
        ++*this;
        return copy;
    }

    FilteredRange begin () const { return {_pred, _base, _end, _enabled}; }
    FilteredRange end () const { return {_pred, _end, _end, _enabled}; }
};


/// Create a FilteredRange
template<class FilterPredicate, class BaseIterator>
FilteredRange<FilterPredicate, BaseIterator>
make_filtered_range (
    FilterPredicate pred,
    BaseIterator base,
    BaseIterator end = {},
    bool enabled = true
){
    return {pred, base, end, enabled};
}




} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_GRAPH_ITERATORS_HH
