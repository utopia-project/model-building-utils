#ifndef UTOPIA_UTILS_CONTAINS_HH
#define UTOPIA_UTILS_CONTAINS_HH

#include <algorithm>

namespace Utopia::Utils {

/// Whether a container contains a certain value
/** Just a shortcut around find
 */
template <class T, class Cont = std::vector<T>>
bool contains(const Cont& cont, const T& value) {
    return (std::find(cont.begin(), cont.end(), value) != cont.end());
}

/// Whether a mapping contains a key
/** \warning Becomes obsolete with C++20
 */
template <class Map>
bool contains_key(const Map& map, const typename Map::key_type& key) {
    return (map.find(key) != map.end());
}

}  // namespace Utopia::Utils

#endif  // UTOPIA_UTILS_CONTAINS_HH
