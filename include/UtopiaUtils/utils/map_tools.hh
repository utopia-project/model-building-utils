#ifndef UTOPIA_UTILS_MAP_TOOLS_HH
#define UTOPIA_UTILS_MAP_TOOLS_HH

#include <string>
#include <algorithm>
#include <type_traits>

#include "contains.hh"
#include "str_tools.hh"


namespace Utopia::Utils {

/// Joins together the keys of a mapping, in iteration order
template<class Map>
std::string join_keys (const Map& map, const std::string& delim = ", ") {
    static_assert(std::is_convertible<typename Map::key_type, std::string>(),
                  "Map keys need to be convertible to std::string");

    std::vector<std::string> keys;
    keys.reserve(map.size());
    std::transform(
        map.begin(), map.end(), std::back_inserter(keys),
        [](const auto& kv){ return kv.first; }
    );

    return join(keys, delim);
}


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_MAP_TOOLS_HH
