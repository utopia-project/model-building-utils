#ifndef UTOPIA_UTILS_ACTION_MANAGER_HH
#define UTOPIA_UTILS_ACTION_MANAGER_HH

#include <memory>
#include <vector>
#include <tuple>
#include <functional>
#include <unordered_map>
#include <string>
#include <sstream>

#include <yaml-cpp/yaml.h>
#include <spdlog/spdlog.h>   // also supplies fmt::
#include <spdlog/sinks/stdout_color_sinks.h>
#include <boost/core/demangle.hpp>

#include "cfg.hh"
#include "map_tools.hh"
#include "str_tools.hh"


/** TODO
  *     - Improve recursive updating capabilities of procedures and hooks
  *         - Meaning: Working more with mappings then with sequences.
  *     - Allow defining action defaults, which are then recursively updated
  *         - Do this efficiently, though, i.e.: not at every invocation
  */

namespace Utopia::Utils {


struct Procedure {
    /// Action or trigger information: (name, parameters as config node)
    using KeyAndParams = std::pair<std::string, Config>;

    /// Trigger information, connected with boolean logic
    /** Outer container: OR-connected; inner: AND-connected
      */
    using TriggerCondition = std::vector<std::vector<KeyAndParams>>;

    /// Map of default parameters
    using ActionDefaults = std::unordered_map<std::string, Config>;

    /// Sequence of (action name, params)
    using ActionSequence = std::vector<KeyAndParams>;


    bool enabled;
    const std::string name;
    const std::string description;
    const TriggerCondition condition;
    const ActionSequence action_sequence;


    Procedure (const std::string& name, const Config& cfg)
    :   enabled(get_as<bool>("enabled", cfg, true))
    ,   name(name)
    ,   description(get_as<std::string>("description", cfg, ""))
    ,   condition(
            parse_trigger_condition(get_as<Config>("condition", cfg, {}))
        )
    ,   action_sequence(
            parse_actions(get_as<Config>("actions", cfg),
                          get_as<Config>("defaults", cfg, {}))
        )
    {}

    Procedure (const Procedure&) = default;
    Procedure (Procedure&&) = default;
    virtual ~Procedure () = default;

    Procedure& operator= (const Procedure&) = delete;
    Procedure& operator= (Procedure&&) = delete;


    template<class TriggerMap>
    bool evaluate_condition (const TriggerMap& triggers) const {
        if (not enabled or condition.empty()) {
            return false;
        }
        bool state;
        bool triggered;

        // Outer loop: OR-connected
        for (const auto& and_conds : condition) {
            state = true;

            // Inner loop: AND-connected
            for (const auto& [trigger_name, trigger_params] : and_conds) {
                // Evaluate the trigger
                try {
                    triggered = triggers.at(trigger_name)(trigger_params);
                }
                catch (std::out_of_range& exc) {
                    throw std::out_of_range(fmt::format(
                        "A trigger with name '{}' was not found! Available "
                        "triggers: {}", trigger_name, join_keys(triggers)
                    ));
                }

                if (not triggered) {
                    // No point in continuing inner loop
                    state = false;
                    break;
                }
            }

            if (state == false) {
                // Can't return yet; check next condition in outer loop.
                continue;
            }
            // One of the OR-connected conditions was true; can return here
            return true;
        }
        // No trigger was evaluated to true
        return false;
    }

    template<class ActionMap>
    void perform_action_sequence (const ActionMap& actions) const {
        for (const auto& [action_name, action_params] : action_sequence) {
            try {
                actions.at(action_name)(action_params);
            }
            catch (std::out_of_range& exc) {
                throw std::out_of_range(fmt::format(
                    "An action with name '{}' was not found! Available "
                    "actions: {}", action_name, join_keys(actions)
                ));
            }
        }
    }

    template<class TriggerMap, class ActionMap>
    bool operator() (const TriggerMap& triggers,
                     const ActionMap& actions) const
    {
        if (not evaluate_condition(triggers)) {
            return false;
        }
        perform_action_sequence(actions);
        return true;
    }


    // .. Helpers .............................................................

    static KeyAndParams parse_spec (const Config& node) {
        if (node.IsScalar()) {
            return {node.as<std::string>(), Config{}};
        }
        else if (node.IsMap()) {
            if (node.size() != 1) {
                throw std::invalid_argument(
                    "Mapping-like action or trigger specification may only "
                    "contain a single key (specifying the action's or "
                    "trigger's name)!"
                );
            }

            std::string name;
            Config params;
            for (const auto& kv : node) {
                name = kv.first.as<std::string>();
                params = kv.second;
                break;
            }
            return {name, params};
        }
        // else: bad node type
        throw std::invalid_argument(
            "Invalid action or trigger specification! Need scalar or mapping!"
        );
    }

    static TriggerCondition parse_trigger_condition (const Config& cond) {
        TriggerCondition tc;

        if (not cond or cond.IsNull()) {
            // No trigger condition, meaning it will never be triggered
            return tc;
        }

        try {
            if (cond.IsScalar() or cond.IsMap()) {
                tc.push_back({{parse_spec(cond)}});
            }
            else if (cond.IsSequence()) {
                // Iterate over OR-connected sequence of AND-connected
                // conditions...
                for (const auto& or_cond : cond) {
                    // Aggregate and-connected conditions
                    std::vector<KeyAndParams> and_conds;

                    if (or_cond.IsScalar() or or_cond.IsMap()) {
                        // Only one AND-connected entry
                        and_conds.push_back({parse_spec(or_cond)});
                    }
                    else {
                        // More than one AND-connected entry
                        // Assume sequence
                        for (auto& and_cond : or_cond) {
                            and_conds.push_back(
                                {parse_spec(and_cond)}
                            );
                        }
                    }
                    tc.push_back(and_conds);
                }
            }
        }
        catch (std::exception& exc) {
            std::stringstream cond_s;
            cond_s << cond;
            throw std::invalid_argument(fmt::format(
                "Failed parsing trigger condition! Make sure to pass them in "
                "the correct format! Got:\n{}\n\nError message: {}",
                cond_s.str(), exc.what()
            ));
        }

        return tc;
    }

    static ActionSequence parse_actions (const Config& actions_cfg,
                                         const Config& defaults = {})
    {
        if (not actions_cfg.IsSequence()) {
            throw std::invalid_argument(
                "Procedure actions need be a sequence of scalars or mappings!"
            );
        }

        // Now, get the actions themselves. If extra parameters are given,
        // recursively update the defaults with those extra parameters.
        ActionSequence action_seq{};

        for (const auto& action_spec : actions_cfg) {
            auto [action_name, action_params] = parse_spec(action_spec);

            if (defaults[action_name]) {
                action_params = recursive_update(defaults[action_name],
                                                 action_params);
            }

            action_seq.push_back({action_name, action_params});
        }
        return action_seq;
    }
};


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// An ActionManager manages actions
class ActionManager {
public:
    using TriggerFunc = std::function<bool(const Config&)>;
    using ActionFunc = std::function<void(const Config&)>;

    using TriggerMap = std::unordered_map<std::string, TriggerFunc>;
    using ActionMap = std::unordered_map<std::string, ActionFunc>;

    using ProcedureSequence = std::vector<std::string>;

    const inline static TriggerMap default_triggers = {
        {"always",  [](const auto&){ return true; }},
        {"never",   [](const auto&){ return false; }},
    };

protected:
    Config cfg;
    std::shared_ptr<spdlog::logger> log;

    /// In debug mode, exceptions are thrown that would otherwise be logged
    bool debug_mode;

    TriggerMap _triggers;
    ActionMap _actions;
    std::unordered_map<std::string, Procedure> _procedures;
    std::unordered_map<std::string, ProcedureSequence> _hooks;

public:
    ActionManager (const std::shared_ptr<spdlog::logger>& parent_logger,
                   const Config& cfg,
                   const TriggerMap& triggers = {},
                   const ActionMap& actions = {})
    :   cfg(cfg)
    ,   log(spdlog::stdout_color_mt(parent_logger->name() + ".actions"))
    ,   debug_mode(get_as<bool>("debug_mode", cfg, true))
    ,   _triggers(triggers)
    ,   _actions(actions)
    ,   _procedures{}
    ,   _hooks{{"all", {}}}
    {
        // Set the log level
        if (cfg["log_level"]) {
            log->set_level(
                spdlog::level::from_str(get_as<std::string>("log_level", cfg))
            );
        }

        // Register the default triggers
        for (const auto& [trigger_name, trigger_func] : default_triggers) {
            register_trigger(trigger_name, trigger_func);
        }

        // Populate procedures and procedure groups, if given
        if (cfg["procedures"]) {
            for (const auto& kv : cfg["procedures"]) {
                register_procedure(kv.first.as<std::string>(), kv.second);
            }
        }
        if (cfg["hooks"]) {
            for (const auto& kv : cfg["hooks"]) {
                register_hook(kv.first.as<std::string>(),
                              kv.second.as<ProcedureSequence>());
            }
        }

        log->info("ActionManager ready.");
        log->debug("Was set up with {:d} procedures and {:d} hooks.",
                   _procedures.size(), _hooks.size());
    }

    ~ActionManager () {
        spdlog::drop(log->name());
    }

    const TriggerMap& triggers () const {
        return _triggers;
    }

    const ActionMap& actions () const {
        return _actions;
    }

    const auto& procedures () const {
        return _procedures;
    }

    const auto& hooks () const {
        return _hooks;
    }

    // .. Registration ........................................................

    /// Register a new trigger if it does not already exist
    void register_trigger (const std::string& name, TriggerFunc func) {
        if (contains_key(_triggers, name)) {
            throw_or_log<std::invalid_argument>(
                "A trigger named '{}' already exists!", name
            );
        }
        _triggers.insert({name, func});
        log->debug("Registered trigger '{}'.", name);
    }

    /// Register a new action if it does not already exist
    void register_action (const std::string& name, ActionFunc func) {
        if (contains_key(_actions, name)) {
            throw_or_log<std::invalid_argument>(
                "An action named '{}' already exists!", name
            );
        }
        _actions.insert({name, func});
        log->debug("Registered action '{}'.", name);
    }

    /// Register a new procedure, if it does not already exist
    void register_procedure (const std::string& name, const Config& proc_cfg) {
        if (contains_key(_procedures, name)) {
            throw_or_log<std::invalid_argument>(
                "A procedure named '{}' already exists!", name
            );
        }
        _procedures.insert({name, Procedure(name, proc_cfg)});
        log->debug("Registered procedure '{}'.", name);

        // Keep track of it in procedure group "all"
        _hooks["all"].push_back(name);
    }

    /// Register a new hook that will carry out a procedure sequence
    void register_hook (const std::string& name,
                        const ProcedureSequence& group)
    {
        if (contains_key(_hooks, name)) {
            throw_or_log<std::invalid_argument>(
                "A hook named '{}' already exists!", name
            );
        }
        _hooks.insert({name, group});
        log->debug("Registered hook '{}'.", name);
    }


    // .. Performing procedures ...............................................

    /// Perform a single procedure
    bool perform_procedure (const Procedure& procedure) const {
        log->trace("Performing procedure '{}' ...", procedure.name);

        bool triggered = false;
        try {
            triggered = procedure(_triggers, _actions);
        }
        catch (std::exception& exc) {
            throw_or_log<std::runtime_error>(
                "Failed performing procedure '{}'! Got {}: {}",
                procedure.name,
                boost::core::demangle(typeid(exc).name()), exc.what()
            );
            return false;
        }

        if (triggered) {
            log->trace("Performed procedure '{}'.", procedure.name);
            return true;
        }
        log->trace("Procedure '{}' was not triggered.", procedure.name);
        return false;
    }

    /// Perform all procedures named in the given container
    /** Returns the number of triggered procedures.
      */
    template<class ProcNames = ProcedureSequence>
    std::size_t perform_procedures (const ProcNames& procedure_names) const {
        if (procedure_names.empty()) {
            log->trace("No procedures to perform.");
            return 0;
        }

        auto count = 0u;

        // To avoid unnecessary join operations, check log level beforehand
        if (log->should_log(spdlog::level::trace)) {
            log->trace("Performing {} procedures: {} ...",
                       procedure_names.size(), join(procedure_names));
        }

        for (const auto& procedure_name : procedure_names) {
            if (not contains_key(_procedures, procedure_name)) {
                throw_or_log<std::out_of_range>(
                    "No procedure '{}' available! Available procedures: {}",
                    procedure_name, join_keys(_procedures)
                );
            }

            if (perform_procedure(_procedures.at(procedure_name))) {
                count++;
            }
        }
        log->trace("Finished performing procedures. {}/{} were triggered.",
                   count, procedure_names.size());
        return count;
    }

    /// Perform all procedures of a certain hook
    /** Returns the number of triggered procedures.
      */
    std::size_t invoke_hook (const std::string& name) const {
        log->trace("Invoking procedure hook '{}' ...", name);

        if (not contains_key(_hooks, name)) {
            throw_or_log<std::out_of_range>(
                "No procedure hook '{}' available! Available hooks: {}",
                name, join_keys(_hooks)
            );
            return 0;
        }

        return perform_procedures(_hooks.at(name));
    }

protected:
    /// Depending on the ``debug_mode`` flag, throws or logs an error
    template<class Exception, class... Args>
    void throw_or_log (const std::string& msg, Args&&... args) const {
        const std::string err_msg = fmt::vformat(
            msg, fmt::make_format_args(std::forward<Args>(args)...)
        );

        if (debug_mode) {
            throw Exception(err_msg);
        }
        log->error(err_msg);
    }

};

} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_ACTION_MANAGER_HH
