#ifndef UTOPIA_UTILS_FORMATTERS_HH
#define UTOPIA_UTILS_FORMATTERS_HH

#include <sstream>
#include <string_view>

#include <yaml-cpp/yaml.h>

#include <spdlog/spdlog.h>
#include <fmt/core.h>


namespace fmt {

/// Custom formatter for the YAML::Node type (aka. Utopia::DataIO::Config)
/** See: https://fmt.dev/latest/api.html#formatting-user-defined-types
  *
  * \note Not working with fmtlib < 6.0
  */
template<>
struct formatter<YAML::Node> : formatter<std::string_view> {
    template<class FormatContext>
    auto format (const YAML::Node& node, FormatContext& ctx) {
        std::stringstream node_stream;
        node_stream << node;
        return formatter<std::string_view>::format(node_stream.str(), ctx);
    }
};


// TODO Armadillo vectors and matrices (inline and block-wise)


} // namespace fmt

#endif // UTOPIA_UTILS_FORMATTERS_HH
