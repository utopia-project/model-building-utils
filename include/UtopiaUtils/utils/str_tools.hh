#ifndef UTOPIA_UTILS_STR_TOOLS_HH
#define UTOPIA_UTILS_STR_TOOLS_HH

#include <string>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>


namespace Utopia::Utils {


/// Joins together the strings in a container
/** Wraps boost::algorithm::join
  */
template<class Cont>
std::string join (const Cont& cont, const std::string& delim = ", ") {
    if (cont.empty()) {
        return "";
    }
    return boost::algorithm::join(cont, delim);
}

/// Splits a string and returns a container of string segments
/** Wraps boost::algorithm::split
  *
  * \param  s       The string to split according to `delims`
  * \param  delims  The delimiters (plural!) to split by. Will be passed to
  *                 boost::is_any_of
  */
template<template<class, class> class SeqCont = std::vector,
         class T = std::string>
SeqCont<T, std::allocator<T>> split (const T& s,
                                     const std::string& delims = " ")
{
    auto segments = SeqCont<T, std::allocator<T>>{};
    boost::split(segments, s, boost::is_any_of(delims));
    return segments;
}


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_STR_TOOLS_HH
