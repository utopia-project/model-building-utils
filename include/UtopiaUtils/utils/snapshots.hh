#ifndef UTOPIA_UTILS_SNAPSHOTS_HH
#define UTOPIA_UTILS_SNAPSHOTS_HH

#include <functional>
#include <set>

#include <fmt/core.h>

#include "cfg.hh"


namespace Utopia::Utils {

/// Manages snapshots of objects S, updated via a conditional
template<class S>
struct SnapshotManager {
    using Condition = std::function<bool(const S&, const S&)>;

private:
    /// The latest snapshot
    S latest;

    /// A candidate for the new latest snapshot
    S candidate;

    /// Binary function; if true, replaces latest snapshot with the candidate
    /** Will receive as arguments: (latest, candidate)
      */
    Condition condition;

public:
    SnapshotManager (const S& l, const S& c, Condition cond)
    :
        latest(l), candidate(c), condition(cond)
    {}

    const S& get_latest () const {
        return latest;
    }

    const S& get_candidate () const {
        return candidate;
    }

    void set_candidate (const S& c) {
        candidate = c;
    }

    bool evaluate_condition (const S& l, const S& c) const {
        return condition(l, c);
    }

    /// Evaluates the candidate snapshot, potentially making it the latest
    bool evaluate_candidate () {
        if (evaluate_condition(latest, candidate)) {
            latest = candidate;
            return true;
        }
        return false;
    }

    /// Sets and directly evaluates the candidate against the latest snapshot
    bool set_and_evaluate_candidate (const S& c) {
        candidate = c;
        return evaluate_candidate();
    }
};



/// A snapshot manager holding multiple snapshots, ordered by an index
/**
  *
  * \tparam S       The snapshot type
  * \tparam I       The index type; typically a numeric value, but can be
  *                 anything that supports basic comparisons
  * \tparam Comp    Which comparator to use
  */
template<class S, class I, class Comp = std::greater<I>>
class MultiSnapshotManager {
public:
    using Snapshot = S;
    using Index = I;

    using IndexFunction = std::function<Index(const S&)>;
    using IndexSnapshotPair = std::pair<Index, S>;

    /// Comparator functional
    inline static const Comp compare = Comp();

private:
    /// Comparator that looks only at the index of the (index, snapshot) pairs
    struct CompareByIndexOnly {
        bool operator() (const IndexSnapshotPair& s1,
                         const IndexSnapshotPair& s2) const
        {
            return MultiSnapshotManager::compare(s2.first, s1.first);
        }
    };

    /// All snapshots, in ascending order of index value
    std::multiset<IndexSnapshotPair, CompareByIndexOnly> _snaps;

    /// The function retrieving the index value from the snapshots
    const IndexFunction _index_func;

    /// The minimum index value; below this value, snapshots are not added
    const Index _threshold;

    /// Maximum number of snapshots to keep track of
    const std::size_t _max_size;

    /// Whether any snapshots will be made at all; if False, `add` is a no-op
    bool _enabled;

public:
    MultiSnapshotManager () = delete;

    /// Construct a MultiSnapshotManager
    /**
     *  \param  index_func  The function to retrieve the index value from the
     *                      snapshot object
     *  \param  threshold   The threshold value beyond which a snapshot will
     *                      be added. Behaviour depends on the Comparator!
     *  \param  max_size    How many snapshots to store at most. If 0, will
     *                      NOT limit the number of snapshots.
     *  \param  enabled     If false, will not add any snapshots.
     */
    MultiSnapshotManager (
        IndexFunction index_func,
        Index threshold,
        std::size_t max_size = 0,
        bool enabled = true
    )
    :
        _snaps({})
    ,   _index_func(index_func)
    ,   _threshold(threshold)
    ,   _max_size(max_size > 0 ? max_size : _snaps.max_size())
    ,   _enabled(enabled)
    {}

    /// Config-based constructor
    MultiSnapshotManager (
        IndexFunction index_func,
        const Config& cfg
    )
    :
        MultiSnapshotManager(
            index_func,
            get_as<Index>("threshold", cfg),
            get_as<std::size_t>("max_size", cfg, 0),
            get_as<bool>("enabled", cfg, true)
        )
    {}

    /// Config-based constructor without index function
    MultiSnapshotManager (
        const Config& cfg
    )
    :
        MultiSnapshotManager(
            [](const auto&) -> Index {
                throw std::runtime_error(
                    "Attempted using index function for making a snapshot "
                    "for a manager that was not initialized with an index "
                    "function. Either specify the index explicitly or provide "
                    "an index function during construction."
                );
            },
            cfg
        )
    {}


    /// The number of currently stored snapshots
    auto size () const {
        return _snaps.size();
    }

    /// The maximum number of snapshots that will be stored
    auto max_size () const {
        return _max_size;
    }

    /// Whether the snapshot manager is enabled at all
    auto enabled () const {
        return _enabled;
    }

    /// The threshold index value
    /** Will only add new snapshots if `compare(thresold, index)` evaluates
      * to True.
      * For `std::less`, this is a minimum value; for `std::greater` a maximum
      */
    Index threshold () const {
        return _threshold;
    }

    /// The snapshots: (index value, snapshot) pairs in ascending order
    const auto& snapshots () const {
        return _snaps;
    }

    /// Add a snapshot
    /** If this would lead to exceeding the maximum size of the snapshots,
      * will only add if the snapshot has an index value for which
      * `not compare(front_index(), idx)` holds; in the case of `std::greater`
      * this means: will only add a new entry if its index is greater or equal
      * to the front index.
      * Note that this may lead to a previously added snapshot with the same
      * index value being removed in favour of the new snapshot.
      *
      * This method uses the index value retrieved by the index function.
      * If the index value is already known, use `add(Index, S)`.
      */
    bool add (const S& s) {
        if (not _enabled) {
            return false;
        };
        return add(_index_func(s), s);
    }

    /// Add a snapshot with a custom index value, ignoring the index function
    bool add (const Index idx, const S& s) {
        if (not _enabled) {
            return false;
        }

        if (compare(_threshold, idx)) {
            return false;
        }

        if (_snaps.size() < _max_size) {
            // Can just insert it
            _snaps.insert({idx, s});
        }
        else {
            // Reached maximum size.
            // Might not have to add it at all
            if (compare(front_index(), idx)) {
                return false;
            }

            // Add it, then pop the smallest element to stay below max_size
            _snaps.insert({idx, s});
            pop_front();
        }
        return true;
    }

    /// Const reference to the smallest (index, snapshot) pair
    const IndexSnapshotPair& front () const {
        return *(_snaps.begin());
    }

    /// Const reference to the largest (index, snapshot) pair
    const IndexSnapshotPair& back () const {
        return *(std::prev(_snaps.end()));
    }

    /// Retrieves the current minimum index value (i.e.: from front-most item)
    Index front_index () const {
        return front().first;
    }

    /// Retrieves the snapshot with the minimum index value
    const S& front_snapshot () const {
        return front().second;
    }

    /// Retrieves the current maximum index value (i.e.: from back-most item)
    Index back_index () const {
        return back().first;
    }

    /// Retrieves the snapshot with the maximum index value
    const S& back_snapshot () const {
        return back().second;
    }


private:
    /// Removes the front-most value, i.e. with smallest comparison value
    IndexSnapshotPair pop_front () {
        IndexSnapshotPair _front = front();
        _snaps.erase(_snaps.begin());
        return _front;
    }
};



} // namespace Utopia::Utils


#endif // UTOPIA_UTILS_SNAPSHOTS_HH
