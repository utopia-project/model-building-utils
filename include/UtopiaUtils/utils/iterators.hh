#ifndef UTOPIA_MODELS_MESONET_UTILS_ITERATORS_HH
#define UTOPIA_MODELS_MESONET_UTILS_ITERATORS_HH

#include <tuple>

#include <boost/range/irange.hpp>


namespace Utopia::Utils {

/// Wrapper around boost::irange, iterating through [0, stop)
template<class T>
auto in_range(const T stop) {
    return boost::irange(static_cast<T>(0), stop);
}

/// Wrapper around boost::irange, iterating through [start, stop)
template<class T>
auto in_range(const T start, const T stop) {
    return boost::irange(start, stop);
}

/// Wrapper around boost::irange, iterating through [start, stop) with step
template<class T>
auto in_range(const T start, const T stop, const T step) {
    return boost::irange(start, stop, step);
}


/// A Python-like `enumerate` function for use in range-based for loops
/** Original implementation by Nathan Reed,
  * see: http://reedbeta.com/blog/python-like-enumerate-in-cpp17/
  */
template<
    class T,
    class Iter = decltype(std::begin(std::declval<T>())),
    class = decltype(std::end(std::declval<T>()))
>
constexpr auto enumerate (T&& iterable) {
    struct iterator {
        std::size_t i;
        Iter iter;

        bool operator != (const iterator& other) const {
            return iter != other.iter;
        }
        void operator++ () {
            ++i;
            ++iter;
        }
        auto operator* () const {
            return std::tie(i, *iter);
        }
    };

    struct iterable_wrapper {
        T iterable;
        iterator begin () {
            return {0, std::begin(iterable)};
        }
        iterator end() {
            return {0, std::end(iterable)};
        }
    };

    return iterable_wrapper{ std::forward<T>(iterable) };
}

} // namespace Utopia::Utils

#endif // UTOPIA_MODELS_MESONET_UTILS_ITERATORS_HH
