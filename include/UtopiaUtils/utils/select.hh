#ifndef UTOPIA_MODELS_EECOSY_SELECT_HH
#define UTOPIA_MODELS_EECOSY_SELECT_HH

#include <vector>
#include <iterator>
#include <algorithm>

#include <boost/range/adaptor/reversed.hpp>

#include <spdlog/spdlog.h>

#include "math.hh"


namespace Utopia::Utils {


/// Select a number of entities using some selection mode
/** \TODO  Make this more modular and efficient, e.g. by allowing to work on a
  *        range for sampling... not copying EntityRange ...
  *
  * Available selection modes:
  *
  *     - `all`:      Return all available; `num` argument will be ignored
  *     - `random`:   Sample from all available entities, using std::sample
  *     - `first_n`:  Returns first `num` elements from the available container
  *     - `last_n`:   Returns last `num` elements from the available container
  *     - `slice`:    Uses the `params` argument to extract entities via the
  *                   `[start : stop : step]` slice. Will select at most `num`.
  *                   Note that `step` needs to be positive. The selection is
  *                   equivalent to the following expression (in Python):
  *                      `some_sequence[start:stop:step][:num]`
  *
  * \warning The interface of this function has not yet converged and it might
  *          change at any point!
  *
  * \note    The returned container is ensured to have a size `<= abs_num`
  *
  * \param available  The iterator range of available entities
  * \param num        The number of entities to select from the range. May also
  *                   be negative, in which case the number is added to the
  *                   number of available entities (plus 1) to lead to the
  *                   number of entities to be selected. In other words:
  *                   `num == -1` will lead to `available.size()` entities
  *                   being selected.
  * \param mode       The selection mode  TODO Make this an enum?
  * \param rng        A random number generator for non-deterministic selection
  * \param params     Used by some selection modes for further arguments.
  *
  */
template<class EntityRange,
         class NumT = int,
         class RNG,
         class EntityCont =
            std::vector<typename EntityRange::const_iterator::value_type>>
EntityCont select (EntityRange available,
                   const NumT num,
                   const std::string& mode,
                   const std::shared_ptr<RNG>& rng,
                   const Config& params = {})
{
    // Prepare output container
    EntityCont selected{};

    // Handle the easy cases first
    if (mode == "none") {
        return selected;
    }
    else if (mode == "all") {
        selected.insert(selected.end(),
                        std::begin(available), std::end(available));
        return selected;
    }

    if (num == 0) {
        return selected;
    }

    // Handle a negative `num`, which suggests evaluation relative to the
    // number of available entities
    const auto num_available = std::distance(available.begin(),
                                             available.end());
    std::size_t abs_num;
    try {
        abs_num = map_into_range<size_like>(num, num_available);
    }
    catch (std::invalid_argument& e) {
        throw std::invalid_argument(
            fmt::format("Invalid entity number {:d} specified for selection "
                        "from pool of {:d} available entities! {}",
                        num, num_available, e.what()));
    }

    // Can now reserve some space
    selected.reserve(abs_num);

    // Distinguish by mode ....................................................
    if (mode == "random") {
        // FIXME Can't sample directly in cases where a filtered view is given,
        //       which is not a proper LegacyForwardIterator. Thus, need to
        //       create an intermediate container.
        //       Ways to improve this: Only do this if EntityRange _really_ is
        //       not a valid iterator type.
        using EntityType = typename EntityRange::iterator::value_type;

        auto entities = std::vector<EntityType>(available.begin(),
                                                available.end());

        std::sample(std::begin(entities), std::end(entities),
                    std::back_inserter(selected), abs_num, *rng);
    }
    else if (mode == "first_n") {
        auto cnt = 0u;
        for (auto entity : available) {
            if (cnt >= abs_num) break;
            selected.push_back(std::move(entity));
            cnt++;
        }
    }
    else if (mode == "last_n") {
        using boost::adaptors::reverse;

        auto cnt = 0u;
        for (auto entity : reverse(available)) {
            if (cnt >= abs_num) break;
            selected.push_back(std::move(entity));
            cnt++;
        }
    }
    else if (mode == "slice") {
        auto start = get_as<int>("start", params, 0);
        auto stop = get_as<int>("stop", params, num_available);
        const auto step = get_as<int>("step", params, 1);

        if (step < 1) {
            throw std::invalid_argument(fmt::format(
                "Selection with mode 'slice' needs step >= 1, but got {}!",
                step
            ));
        }

        // Wrap values around to allow slicing to start from the back
        start = map_into_range<index_like>(start, num_available);
        stop = map_into_range<size_like>(stop, num_available);
        // NOTE stop is size-like, because the stop index is _excluded_!

        auto cnt = 0u;
        auto it = available.begin();
        std::advance(it, start);

        while (it != available.end()) {
            if (cnt >= abs_num) break;
            else if (static_cast<int>(cnt)*step + start >= stop) break;

            selected.push_back(*it);
            cnt++;

            std::advance(it, step);
        }
    }
    else {
        throw std::invalid_argument("Invalid selection mode: " + mode);
    }

    return selected;
}

/// Overload of select that extracts `mode` and `num` from a config node
/**
  * \warning The interface of this function has not yet converged and it might
  *          change at any point!
  *
  * \param available  The iterator range of available entities
  * \param params     The selection parameters. Required keys: `mode`, `num`.
  *                   The `num` key is optional when `mode == "all"`.
  * \param rng        A random number generator for non-deterministic selection
  */
template<class EntityRange, class RNG, class... Args>
auto select (EntityRange&& available,
             const Config& params,
             const std::shared_ptr<RNG>& rng,
             Args&&... args)
{
    const auto mode = get_as<std::string>("mode", params);

    int num = 0;
    if (mode != "all" and mode != "none") {
        num = get_as<int>("num", params);
    }

    return select(std::forward<EntityRange>(available),
                  std::move(num),
                  std::move(mode),
                  rng,
                  params,
                  std::forward<Args>(args)...);
}


} // namespace Utopia::Utils

#endif // UTOPIA_MODELS_EECOSY_SELECT_HH
