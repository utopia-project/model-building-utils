#ifndef UTOPIA_UTILS_TESTTOOLS_HH
#define UTOPIA_UTILS_TESTTOOLS_HH

#include <algorithm>
#include <chrono>
#include <tuple>
#include <random>
#include <map>

#include <armadillo>
#include <spdlog/spdlog.h>

#include <utopia/core/types.hh>
#include <utopia/core/testtools.hh>
#include <utopia/core/testtools/utils.hh>
#include <utopia/data_io/cfg_utils.hh>

#include "armadillo.hh"


namespace Utopia::TestTools {

template<typename Callable=std::function<double()>>
void sample_callable(const Callable& func,
                     const DataIO::Config& test_cfg,
                     const LocationInfo loc = {})
{
    BOOST_TEST_CONTEXT("Sampling callable...\n--- Test Config ---\n"
                       << test_cfg <<      "\n-------------------") {

    // Check if it this call is expected to throw . . . . . . . . . . . . . . .
    if (test_cfg["throws"]) {
        // Extract parameters
        const auto exc_type = get_as<std::string>("throws", test_cfg);
        const auto match = get_as<std::string>("match", test_cfg, "");

        // Call the boost-supporting check function
        if (exc_type == "std::invalid_argument") {
            check_exception<std::invalid_argument>(func, match, loc);
        }
        else if (exc_type == "std::runtime_error") {
            check_exception<std::runtime_error>(func, match, loc);
        }
        else if (exc_type == "Utopia::KeyError") {
            check_exception<Utopia::KeyError>(func, match, loc);
        }
        else {
            throw std::invalid_argument("Invalid exception type '"
                                        + exc_type + "' given in `throws` "
                                        "argument!");
        }

        // No need to check its value
        return;
    }
    // Not expecting to throw
    // Get the results . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    // Prepare container
    const auto sample_size = get_as<std::size_t>("sample_size", test_cfg);

    std::vector<double> results{};
    results.reserve(sample_size);

    // Gather all the values
    for (std::size_t i=0;  i < sample_size;  i++) {
        results.push_back(func());
    }

    // Test the results . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    // Count how many exceed the range (optional)
    if (test_cfg["expected_in"]) {
        const auto rg = get_as<std::pair<double, double>>("expected_in",
                                                          test_cfg);

        std::size_t num_under = 0;
        std::size_t num_over = 0;

        for (const auto& val : results) {
            num_under += (rg.first > val);
            num_over += (val > rg.second);
        }

        // Make sure all are in the range
        BOOST_TEST(num_under == 0);
        BOOST_TEST(num_over == 0);
    }

    // Test mean value
    if (test_cfg["mean_in"]) {
        const auto rg = get_as<std::pair<double, double>>("mean_in", test_cfg);

        // Calculate the mean
        const double mean =
            std::accumulate(results.begin(), results.end(), 0.,
                [](auto sum, auto val){
                    return sum + val;
                })
            / results.size();

        // Make sure it is in the interval
        BOOST_TEST(rg.first <= mean);
        BOOST_TEST(mean <= rg.second);
    }

    // Test standard deviation
    if (test_cfg["stddev_in"]) {
        const auto rg = get_as<std::pair<double, double>>("stddev_in",
                                                          test_cfg);

        // Calculate mean and standard deviation
        const double mean =
            std::accumulate(results.begin(), results.end(), 0.,
                [](auto sum, auto val){
                    return sum + val;
                })
            / results.size();

        const double stddev =
            sqrt(
                std::accumulate(results.begin(), results.end(), 0.,
                    [&](auto devsq, auto val){
                        return devsq + pow(val - mean, 2);
                    })
                / (results.size() - 1)
            );

        // Make sure it is in the interval
        BOOST_TEST(rg.first <= stddev);
        BOOST_TEST(stddev <= rg.second);
    }
    } // end of BOOST_TEST_CONTEXT
}


}


namespace Utopia::Utils {

// -- General Tools -----------------------------------------------------------

/// Get the size of a (non-random-access) range iterator
/** Useful for getting the size of iterators which don't have a size method,
  * e.g. forward iterators.
  */
template<class IterRange>
auto get_iter_size(IterRange&& rg) {
    return std::distance(rg.begin(), rg.end());
}

/// Populate a container with the elements of a range iteration
/** \TODO Auto-detect element type
  */
template<class ET, class IterRange>
auto get_elements(IterRange&& rg) {
    return get_elements<ET>(rg.begin(), rg.end());
}

/// Populate a container with the elements of an iteration
/** \TODO Auto-detect element type
  */
template<class ET, class It>
auto get_elements(It&& begin, It&& end) {
    std::vector<ET> elements(begin, end);
    return elements;
}


// -- Benchmarking Tools ------------------------------------------------------

/// Type of clock
using Clock = std::chrono::high_resolution_clock;

/// Type of a time point, retrieved from the clock
using Time = std::chrono::high_resolution_clock::time_point;

/// Type of the duration measure, should be a floating-point type
using DurationType = std::chrono::duration<double>;


template<class Cont>
auto mean_and_stddev(const Cont& times) {
    double sum = std::accumulate(times.begin(), times.end(), 0.0);
    double mean = sum / times.size();

    auto diff = std::vector<double>(times.size());

    std::transform(times.begin(), times.end(), diff.begin(),
                   [mean](double t) { return t - mean; });
    double sq_sum = std::inner_product(diff.begin(), diff.end(),
                                       diff.begin(), 0.0);
    double stddev = std::sqrt(sq_sum / times.size());

    return std::make_pair(mean, stddev);
}


/// Returns the absolute time (in seconds) between the given time points
double time_between(const Time& start, const Time& end) {
    const DurationType seconds = abs(end - start);
    return seconds.count();
}

/// Returns the time (in seconds) since the given time point
double time_since(const Time& start) {
    return time_between(start, Clock::now());
}

/// (Silently) performs a benchmark
template<class Func, class... Args>
auto perform_benchmark (const int num_samples, Func func, Args&&... args) {
    std::vector<double> times{};

    for (int i = 0; i < num_samples; i++) {
        const auto t = func(std::forward<Args>(args)...);
        times.push_back(t);
    }

    const auto total_time = std::accumulate(times.begin(), times.end(), 0.);
    const auto [mean, stddev] = mean_and_stddev(times);

    return std::make_tuple(mean, stddev, total_time, times);
}

/// Performs a benchmark and informs about some results
template<class Func, class Logger, class... Args>
auto perform_benchmark (std::string name, const int num_samples,
                        const Logger& log, Func func, Args&&... args)
{
    // log->info("Benchmark:  {}  ({} samples) ...", name, num_samples);

    auto [mean, stddev, total_time, times] =
        perform_benchmark(num_samples, func, std::forward<Args>(args)...);

    log->info(
        "Benchmark ({:d}x):   {}\n"
        "\tPer iteration:  ({:.2g} ± {:.2g}) s   \tTotal: {:.2g} s\n",
        num_samples, name, mean, stddev, total_time
    );

    return std::make_tuple(mean, stddev, total_time, times);
}


/// A class to repetitively perform benchmarks and store results
struct Benchmark {
    using Config = DataIO::Config;
    using Logger = std::shared_ptr<spdlog::logger>;
    using Result = std::tuple<double, double, double, std::vector<double>>;
    using Keys = std::vector<std::string>;

    Config cfg;
    const Logger log;
    const bool enabled;
    const bool show_results;
    unsigned num_samples;            /// *Default* number of samples

    struct Results {
        std::map<std::string, double> mean;
        std::map<std::string, double> stddev;
        std::map<std::string, double> total_time;
        std::map<std::string, std::vector<double>> times;

        void add (const std::string& key, const Result& result) {
            mean[key] = std::get<0>(result);
            stddev[key] = std::get<1>(result);
            total_time[key] = std::get<2>(result);
            times[key] = std::get<3>(result);
        }

        Keys keys () const {
            auto _keys = Keys();
            for (auto& kv_pair : mean) {
                _keys.push_back(kv_pair.first);
            }
            return _keys;
        }
    };
    Results results;

    Benchmark (const Config& _cfg,
               const Logger& log)
    :
        cfg(_cfg)
    ,   log(log)
    ,   enabled(get_as<bool>("enabled", cfg, true))
    ,   show_results(get_as<bool>("show_results", cfg, false))
    ,   num_samples(get_as<unsigned>("num_samples", cfg, 10))
    ,   results{}
    {}

    /// Wrapper around TestTools::perform_benchmark, storing the result
    template<class BenchmarkFunc, class... Args>
    Result perform (const std::string& name,
                    const BenchmarkFunc& func,
                    Args&&... args)
    {
        // TODO Name check

        const auto res = perform_benchmark(
            name,
            num_samples,
            log,
            func,
            std::forward<Args>(args)...
        );
        results.add(name, res);

        return res;
    }

    /// Compiles a summary of benchmarking results and prints them
    void print_summary () const {
        auto [cm, keys] = comparison_matrix();

        std::cout << "Benchmark names:" << std::endl;
        for (auto& k : keys) {
            std::cout << "   " << k << std::endl;
        }

        cm.print("\nMean relative iteration times (row & col order == names)");
    }

    /// Generates a comparison matrix of mean relative iteration times
    std::pair<arma::mat, Keys> comparison_matrix () const {
        using Utopia::Utils::imbue_row_col;

        auto keys = results.keys();
        auto& v = results.mean;

        // Create and populate the comparison matrix
        auto cm = arma::mat(keys.size(), keys.size(), arma::fill::zeros);
        imbue_row_col(cm, [&](auto i, auto j){
            return v.at(keys[j]) / v.at(keys[i]);
        });

        return std::make_pair(cm, keys);
    }
};


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_TESTTOOLS_HH
