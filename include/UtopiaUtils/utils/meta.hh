#ifndef UTOPIA_UTILS_META_HH
#define UTOPIA_UTILS_META_HH

#include <tuple>
#include <type_traits>

#include <utopia/core/type_traits.hh>


namespace Utopia::Utils {

/// Checks whether two *templated* types are the same -- false case
template <template <class...> class, template<class...> class>
struct is_same_template : std::false_type {};

/// Checks whether two *templated* types are the same -- true case
template <template <class...> class T>
struct is_same_template<T, T> : std::true_type {};


/// Whether a tuple-like contains the specified type; forward declaration
template <class T, class Tuple>
struct has_type;

/// Whether a tuple-like contains the specified type
/** Uses std::disjunction to perform the logical OR.
  *
  * \tparam  T      The type to look for
  * \tparam  Ts     The tuple-like type to look for T in
  */
template <class T, class... Ts>
struct has_type<T, std::tuple<Ts...>>
    : std::disjunction<std::is_same<T, Ts>...> {};


/// Whether a type T is contained in one of the variadically specified types
/** A wrapper around has_type that allows to specify a variadic template as
  * second template argument.
  *
  * \tparam  T      The type to look for in Ts
  * \tparam  Ts     If any of these Ts is the same as T, this struct is
  *                 an std::true_type, otherwise std::false_type.
  */
template <class T, class... Ts>
struct contains_type : has_type<T, std::tuple<Ts...>> {};


/// The size of a template parameter pack
template<class... Ts>
constexpr std::size_t size () {
    return std::tuple_size<std::tuple<Ts...>>::value;
}

/// Whether a template parameter pack is empty, i.e. contains no types
template<class... Ts>
constexpr bool is_empty () {
    return size<Ts...>() == 0;
}

/// True if T is any of the types Ts
template <class T, class... Ts>
struct is_any : std::disjunction<std::is_same<T, Ts>...> {};

/// True if both types are the same (alias for std::is_same)
using std::is_same;

/// True if all types are the same
template <class T, class... Ts>
struct are_same : std::conjunction<std::is_same<T, Ts>...> {};


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_META_HH
