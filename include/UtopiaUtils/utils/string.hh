#ifndef UTOPIA_MODELS_MESONET_UTILS_STRING_HH
#define UTOPIA_MODELS_MESONET_UTILS_STRING_HH

#include <string>
#include <stdexcept>


namespace Utopia::Utils {

/// Pads the given string with the given character to arrive at the ``width``
/** Padding occurs on the left. The pad character needs to be a string of
  * length 1.
  */
std::string pad_left(std::string s,
                     const unsigned short int width,
                     const std::string pad_char = "0")
{
    if (pad_char.size() != 1) {
        throw std::invalid_argument("Need a pad_char of length 1, but got: '"
                                    + pad_char + "'");
    }

    if (s.length() < width) {
        s.insert(0, width - s.length(), pad_char.c_str()[0]);
    }

    return s;
}

} // namespace Utopia::Utils

#endif // UTOPIA_MODELS_MESONET_UTILS_STRING_HH
