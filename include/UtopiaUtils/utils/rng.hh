#ifndef UTOPIA_UTILS_RNG_HH
#define UTOPIA_UTILS_RNG_HH

#include "generators.hh"    // By Harald Mack, copied from Utopia::Models::Amee


namespace Utopia::Utils {

/// The generators namespace, as implemented by Harald Mack (@mackharald89)
namespace RNGs = Utopia::Generators;

/// The default Random Number Generator: the fast and reliable `Xoroshiro128**`
using DefaultRNG = RNGs::Xoroshiro128starstar;


} // namespace Utopia::Utils


#endif // UTOPIA_UTILS_RNG_HH
