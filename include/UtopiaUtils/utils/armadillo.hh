#ifndef UTOPIA_UTILS_ARMADILLO_HH
#define UTOPIA_UTILS_ARMADILLO_HH

#include <armadillo>
#include <fmt/core.h>

#include "types.hh"
#include "contains.hh"
#include "iterators.hh"
#include "str_tools.hh"


namespace Utopia::Utils {

// .. Construction and Manipulation ...........................................

/// Like arma::Mat::imbue, but passes row and col indices to functor
/** See  https://stackoverflow.com/a/64593518/1827608  for some discussion on
  * ways to achieve this functionality.
  */
template<class Mat, class Func>
void imbue_row_col (Mat& m, Func func) {
    auto it = m.begin_row_col();
    const auto it_end = m.end_row_col();

    for (; it != it_end; ++it) {
        *it = func(it.row(), it.col());
    }
}

/// Like imbue_row_col, but with conditional
template<class Mat, class Func, class Cond>
void imbue_row_col_if (Mat& m, Func func, Cond cond) {
    auto it = m.begin_row_col();
    const auto it_end = m.end_row_col();

    for (; it != it_end; ++it) {
        if (cond(it.row(), it.col())) {
            *it = func(it.row(), it.col());
        }
    }
}

/// Like arma::Col/Row::imbue but passes iteration index to functor
template<class Vec, class Func>
void imbue_index (Vec& v, Func func) {
    auto n = 0u;
    for (auto& e : v) {
        e = func(n++);
    }
}

/// Like imbue_index, but with conditional
template<class Vec, class Func, class Cond>
void imbue_index_if (Vec& v, Func func, Cond cond) {
    auto n = 0u;
    for (auto& e : v) {
        if (cond(n++)) {
            e = func(n);
        }
    }
}

/// Appends an element to the given vector and (optionally) sets its value
/** If no value is given, explicitly sets it to zero.
  *
  * This works for both row and column vectors.
  */
template<class Vec, class T = typename Vec::elem_type>
void append_element (Vec& v, T value = 0) {
    v.resize(v.size()+1);
    v(v.size()-1) = value;
}

/// Appends a row and column to a matrix
template<class Mat, class T = typename Mat::elem_type>
void append_row_col (Mat& m, T value = 0) {
    m.resize(m.n_rows + 1, m.n_cols + 1);
    m.row(m.n_rows-1).fill(value);
    m.col(m.n_cols-1).fill(value);
}

/// Appends a column to a matrix
template<class Mat, class T = typename Mat::elem_type>
void append_col (Mat& m, T value = 0) {
    m.resize(m.n_rows, m.n_cols + 1);
    m.col(m.n_cols-1).fill(value);
}

/// Appends a row to a matrix
template<class Mat, class T = typename Mat::elem_type>
void append_row (Mat& m, T value = 0) {
    m.resize(m.n_rows + 1, m.n_cols);
    m.row(m.n_rows-1).fill(value);
}

/// Creates an object of the same size as the given object, filled with a value
template<class T>
T filled_like (const T& obj, typename T::elem_type fill_value) {
    T new_obj;
    new_obj.copy_size(obj);
    new_obj.fill(fill_value);
    return new_obj;
}

/// Like filled_like, but with zeros as fill value
template<class T>
T zeros_like (const T& obj) {
    return filled_like(obj, 0.);
}

/// Like filled_like, but with ones as fill value
template<class T>
T ones_like (const T& obj) {
    return filled_like(obj, 1.);
}


// .. String representation ...................................................

/// Represents a vector-like object as a string, using custom formatting
template<class T>
std::string format_vec (const T& obj,
                        const std::string& fstr_elem = "{}",
                        const std::string& fstr_full = "({})",
                        const std::string& delim = ", ")
{
    std::vector<std::string> elems;
    for (const auto& elem : obj) {
        elems.push_back(fmt::vformat(fstr_elem, fmt::make_format_args(elem)));
    }
    return fmt::vformat(fstr_full, fmt::make_format_args(join(elems, delim)));
}


// .. Computations ............................................................

/// Returns highest absolute and relative abs. difference of the two vectors
template<class T1, class T2>
std::pair<double, double> max_abs_rel_diff (const T1& v1, const T2& v2) {
    const auto diff = arma::abs(v1 - v2);
    const auto reldiff = diff / arma::abs((v1 + v2) / 2.);

    return { diff.max(), reldiff.max() };
}


// .. Search ..................................................................

/// Returns those indices of `v` that match a certain condition
/** The condition is given via the `mode` parameter and, in some cases, via
  * additional parameters specified in the `params` config node.
  *
  * \param   v      The vector to find the indices in
  * \param   mode   The search mode, e.g. `min`, `is_any_of`, `>=`, ...
  * \param   params Additional optional parameters for the modes, e.g. the
  *                 `rhs` parameter for binary comparisons
  *
  * \warning This function is not particularly optimized! Avoid repetitive
  *          invocation.
  */
template<class V>
arma::uvec find_indices (const V& v,
                         const std::string& mode,
                         const Config& params)
{
    arma::uvec idcs{};

    if (mode == "min") {
        append_element(idcs, v.index_min());
    }
    else if (mode == "max") {
        append_element(idcs, v.index_max());
    }
    else if (mode == "finite") {
        idcs = arma::find_finite(v);
    }
    else if (mode == "nonfinite") {
        idcs = arma::find_nonfinite(v);
    }
    else if (mode == "is_any_of") {
        const auto rhs = get_as<std::vector<double>>("rhs", params);

        for (auto&& [i, val] : enumerate(v)) {
            if (contains(rhs, val)) {
                append_element(idcs, i);
            }
        }
    }
    else if (mode == "is_none_of") {
        const auto rhs = get_as<std::vector<double>>("rhs", params);

        for (auto&& [i, val] : enumerate(v)) {
            if (not contains(rhs, val)) {
                append_element(idcs, i);
            }
        }
    }

    // simple comparisons
    else if (mode == ">=") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v >= rhs);
    }
    else if (mode == ">") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v > rhs);
    }
    else if (mode == "<") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v < rhs);
    }
    else if (mode == "<=") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v <= rhs);
    }
    else if (mode == "==") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v == rhs);
    }
    else if (mode == "!=") {
        const double rhs = get_as<double>("rhs", params);
        idcs = arma::find(v != rhs);
    }
    else {
        throw std::invalid_argument(fmt::format(
            "Invalid mode '{}' for find_indices!", mode
        ));
    }

    return idcs;
}

} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_ARMADILLO_HH
