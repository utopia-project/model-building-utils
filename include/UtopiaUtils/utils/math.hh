#ifndef UTOPIA_UTILS_MATH_HH
#define UTOPIA_UTILS_MATH_HH

#include <cmath>
#include <limits>
#include <type_traits>

#include <armadillo>
#include <spdlog/spdlog.h>

#include "types.hh"
#include "tags.hh"
#include "meta.hh"
#include "iterators.hh"


namespace Utopia::Utils {

using namespace Utopia::Utils::tags;

/// Floating point precision constant
const double PREC = 1e-13;


/// Maps a negative number into ``[0, bound)`` or ``[0, bound]``
/** The bound is chosen via a mandatory template argument.
  *
  * This allows "selecting from the back", where `n == -1` will either select
  * ``bound - 1`` (for index-like behaviour) or ``bound`` (for size-like
  * behaviour).
  *
  * \TODO   Can probably improve the type and sign handling here...
  *
  * \tparam mode   Specify index_like or size_like tags to define whether to
  *                map into the half-open or closed interval, respectively.
  *
  * \param  n      The number to map
  * \param  bound  The upper bound of the interval to map into
  *
  * \throws std::invalid_argument Upon too negative or too large ``idx``
  *                               argument.
  */
template<class mode, class... tags, class T=long int, class U=std::size_t>
std::size_t map_into_range (const T _n, const U _bound) {
    static_assert(
        is_any<mode, size_like, index_like>(),
        "Need mode tag specified! Can be size_like or index_like."
    );
    static_assert(
        std::is_integral<T>() and std::is_integral<U>(),
        "Need integer-type arguments!"
    );

    // Work on signed integer copies of the arguments; safer.
    long n = _n;
    long bound = _bound;

    // For closed intervals, simply increment the upper bound by one
    if constexpr (is_same<mode, size_like>()) {
        bound++;
    }

    // Perform clamping to catch upper and lower bound errors
    if constexpr (   contains_type<clamp_lower, tags...>()
                  or contains_type<clamp_to_bounds, tags...>())
    {
        if (n < -bound) {
            n = -bound;
        }
    }
    if constexpr (   contains_type<clamp_upper, tags...>()
                  or contains_type<clamp_to_bounds, tags...>())
    {
        if (n >= bound) {
            n = bound - 1;
        }
    }

    // Now, evaluate
    if (n < -bound) {
        if constexpr (is_same<mode, size_like>()) {
            throw std::invalid_argument(
                fmt::format("Cannot map {:d} into range [0, {:d}], was too "
                            "negative!", n, bound-1));
        }
        else {
            throw std::invalid_argument(
                fmt::format("Cannot map {:d} into range [0, {:d}), was too "
                            "negative!", n, bound));
        }
    }
    else if (n < 0) {
        return bound + n;
    }
    else if (n < bound) {
        return n;
    }
    else {
        if constexpr (is_same<mode, size_like>()) {
            throw std::invalid_argument(
                fmt::format("{:d} is out of range [0, {:d}]!", n, bound-1));
        }
        else {
            throw std::invalid_argument(
                fmt::format("{:d} is out of range [0, {:d})!", n, bound));
        }
    }
}


/// Computes the smallest quotient of the given element-wise division
/**
  * \note    Armadillo consistently works with non-finite values (+/-inf, NaN)
  *          that occur during the divison. Thus, division by zero is in itself
  *          not a problem.
  *
  * \warning Requires floating-point element types
  *
  * \tparam  tags      An arbitrary number of tags that change the behaviour of
  *                    this function. If this contains the ::use_absolute tag,
  *                    the quotient's absolute value will be used to determine
  *                    the minimum element. If the ::clean_elements tag is
  *                    given, the vector elements will be cleaned prior to
  *                    performing the division.
  *
  * \param   dividend  The dividend of the division
  * \param   divisor   The divisor of the division
  *
  * \returns The minimum element of the resulting element-wise division
  */
template<
    class... tags,
    class Vec1, class Vec2 = Vec1,
    std::enable_if_t<not contains_type<clean_elements, tags...>::value, int> =0
    >
double smallest_quotient (const Vec1& dividend, const Vec2& divisor) {
    static_assert(std::is_floating_point<typename Vec1::elem_type>());
    static_assert(std::is_floating_point<typename Vec2::elem_type>());

    // Perform the division
    const auto quotient = dividend / divisor;

    // Some logging output for debugging ...
    // std::cout << std::endl;
    // dividend.print("dividend");
    // divisor.print("divisor");
    // quotient.print("quotient");

    // Return the minimum (absolute or non-absolute) element
    if constexpr (contains_type<use_absolute, tags...>()) {
        return arma::abs(quotient).min();
    }
    return quotient.min();
}


/// Computes the smallest quotient of the given element-wise division
/** This overloads is activated when the clean_elements tag is given, which
  *
  * \note    Unlike the regular smallest_quotient function, this function works
  *          on copies of the given dividend and divisor.
  *
  * \tparam  tags      An arbitrary number of tags that change the behaviour of
  *                    this function. If this contains the ::use_absolute tag,
  *                    the quotient's absolute value will be used to determine
  *                    the minimum element. If the ::clean_elements tag is
  *                    given, the vector elements will be cleaned prior to
  *                    performing the division.
  *
  * \param   dividend  The dividend of the division
  * \param   divisor   The divisor of the division
  * \param   precision The threshold for the clean operation; defaults to PREC
  *
  * \returns The minimum element of the resulting element-wise division
  */
template<
    class... tags,
    class Vec1, class Vec2 = Vec1,
    std::enable_if_t<contains_type<clean_elements, tags...>::value, int> = 0
    >
double smallest_quotient (Vec1 dividend, Vec2 divisor,
                          const double precision = PREC)
{
    return smallest_quotient(dividend.clean(precision),
                             divisor.clean(precision));
}


/// Returns a vector like `v` but with values set to NaN where `mask` is True
template<class Vec, class Mask>
Vec apply_mask (Vec v, const Mask& mask) {
    static_assert(
        std::is_integral<typename Mask::elem_type>(),
        "Mask needs be of an integer element type!"
    );
    static_assert(
        std::is_floating_point<typename Vec::elem_type>()
    );

    const auto NaN = std::numeric_limits<typename Vec::elem_type>::quiet_NaN();

    if (v.n_elem != mask.n_elem) {
        std::invalid_argument(fmt::format(
            "Could not apply mask due to a size mismatch! {} != {}",
            v.n_elem, mask.n_elem
        ));
    }

    for (const auto i : in_range(mask.n_elem)) {
        if (mask[i]) {
            v[i] = NaN;
        }
    }

    return v;
}

/// Masks values that are equal or close to a given value (default: zero)
template<class T>
void mask_values (T& v,
                  const double mask_value = 0.,
                  const double prec = PREC)
{
    using eT = typename T::elem_type;
    static_assert(std::is_floating_point<eT>(),
        "mask_values is only available for floating point types");

    const eT NaN = std::numeric_limits<eT>::quiet_NaN();
    v.transform([NaN, mask_value, prec](const eT val) -> eT {
        if (fabs(val - mask_value) <= prec) {
            return NaN;
        }
        return val;
    });
}

/// Replaces NaNs in an armadillo object by the given replacement value
template<class T, class VT = typename T::elem_type>
void unmask (T& v, const VT replace_by = 0) {
    using eT = typename T::elem_type;
    static_assert(std::is_floating_point<eT>(),
        "unmask_NaN is only available for floating point types");

    v.replace(std::numeric_limits<eT>::quiet_NaN(), replace_by);
}


/// Applies the given operations on the given armadillo-like object
/** This does NOT occur in the order of the tags, but in the following order:
  *
  *   1.  clean_elements, calls .clean method with default precision (PREC)
  *   2.  mask_zeros, calls mask_values
  *   3.  unmask_NaNs, calls unmask
  *
  * The clean_and_mask tag is shorthand for clean_elements and mask_zeros.
  * The clean_and_unmask tag is shorthand for clean_elements and unmask_NaNs.
  *
  * \todo    Do this in a more clever way
  */
template<class... tags, class T>
auto apply_operations (T obj) {
    using eT = typename T::elem_type;

    if constexpr (   contains_type<clean_elements, tags...>()
                  or contains_type<clean_and_mask, tags...>()
                  or contains_type<clean_and_unmask, tags...>())
    {
        static_assert(std::is_floating_point<eT>(),
            "clean_elements is only available for floating point types");
        obj.clean(PREC);
    }

    if constexpr (   contains_type<mask_zeros, tags...>()
                  or contains_type<clean_and_mask, tags...>())
    {
        mask_values(obj, 0.);
    }

    if constexpr (   contains_type<unmask_NaNs, tags...>()
                  or contains_type<clean_and_unmask, tags...>())
    {
        unmask(obj);
    }

    return obj;
}


} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_MATH_HH
