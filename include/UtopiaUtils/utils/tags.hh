#ifndef UTOPIA_UTILS_TAGS_HH
#define UTOPIA_UTILS_TAGS_HH


namespace Utopia::Utils::tags {


// -- General type tags -------------------------------------------------------
/// Used to indicate that strict argument checks should be carried out
struct check_arguments;

/// Used to indicate that checks should be skipped
struct skip_checks;

/// Used to indicate that checks should be performed
struct perform_checks;

/// Used to indicate index-like behaviour (excluding upper bound)
struct index_like;

/// Used to indicate size-like behaviour (including upper bound)
struct size_like;

/// Indicates that a deep copy of the given object should be used
struct deep_copy;

/// Indicates that a shallpw copy of the given object may be used
struct shallow_copy;

/// A tag to denote that something is to be excluded
struct exclude;



// -- Math-related type tags --------------------------------------------------

/// Used to indicate that a cleaning operation should be performed on elements
struct clean_elements;

/// Indicates to use an absolute value
struct use_absolute;

/// Indicates that zero-like values should be masked
struct mask_zeros;

/// Indicates that NaN values should be turned to zeros
struct unmask_NaNs;

/// Indicates clean_elements and mask_zeros
struct clean_and_mask;

/// Indicates clean_elements and unmask_NaNs
struct clean_and_unmask;

/// Indicates to clamp at a upper bound
struct clamp_upper;

/// Indicates to clamp at a lower bound
struct clamp_lower;

/// Indicates to clamp at a lower and upper bound
struct clamp_to_bounds;



// -- Void-like type tags -----------------------------------------------------
/// A non-templated void type, allowing instantiation
struct Void {};

/// A templated void type, accepting a single template argument
template<class>
struct Void1;

/// A templated void type, accepting two template arguments
template<class, class>
struct Void2;

/// A templated void type, accepting arbitrary number of template arguments
template<class...>
struct VoidT;



} // namespace Utopia::Utils

#endif // UTOPIA_UTILS_TAGS_HH
