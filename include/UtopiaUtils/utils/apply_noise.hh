#ifndef UTOPIA_MODELS_MESONET_UTILS_APPLY_NOISE_HH
#define UTOPIA_MODELS_MESONET_UTILS_APPLY_NOISE_HH

#include <string>
#include <vector>
#include <random>
#include <algorithm>

#include <spdlog/spdlog.h>
#include <yaml-cpp/yaml.h>

#include <utopia/core/exceptions.hh>
#include <utopia/data_io/cfg_utils.hh>

#include "cfg.hh"


namespace Utopia::Utils {


/// Apply noise to a given scalar node and return the new value
/** \param value_node    The node to read the scalar value from
  * \param noise_params  A config node specifying _how_ noise is applied
  * \param rng           The RNG to use
  *
  * \return double       The value with noise applied to it. This value still
  *                      needs to be applied to the Config tree the value node
  *                      was initially embedded in!
  *
  * \note   The given `value` parameter is not changed in place, but it is
  *         evaluated as double and returned. The wrapper function takes care
  *         of setting the value in the mapping.
  *         Thus, the key `enabled` is also ignored here; it is evaluated
  *         by the wrapper.
  *
  * Possible `noise_params` are as follows:
  *
  *   - `distribution`: The distribution to calculate the real-valued noise
  *                     with. Available distributions:
  *                          - `uniform`:         [0.,  1.)
  *                          - `symmetric`:       [-1., 1.)
  *                          - `normal`:          (mu=0., sigma=1.)
  *                          - `lognormal`:       (mu=0., sigma=1.)
  *                          - `exponential`
  *                          - `extreme_value`:   (location: 0., scale: 1.)
  *                          - `deterministic_zero`: always 0, without RNG
  *                          - `deterministic_one`: always 1, without RNG
  *
  *   - `mode`: The mode to calculate the new value with.
  *               Available modes:
  *                    - `scale`:     `new = old * (factor * noise)`
  *                    - `shift`:     `new = old + (factor * noise)`
  *                    - `shift_rel`: `new = old + old * (factor * noise)`
  *                    - `random`:    `new = factor * noise + offset`
  *
  *   - `factor` (double, optional, default 1.)
  *   - `offset` (double, optional, default 0.)
  *   - `clamp_low` (double, optional, default: no lower bound)
  *   - `clamp_high` (double, optional, default: no upper bound)
  *   - `read_old_value_as` (std::string, optional, default: `double`)
  *              How to read the value from `value_node`. Can be:
  *              `double`, `int`, `unsigned`
  *
  * \TODO allow to pass parameters to distributions
  * \TODO Add a "redraw" feature if clamping was necessary. Otherwise, the
  *       distribution might get distorted from the clamping!
  */
template<class RNG>
double
apply_noise_to_value (const DataIO::Config& value_node,
                      const DataIO::Config& noise_params,
                      const std::shared_ptr<RNG>& rng)
{
    if (not value_node or not value_node.IsScalar()) {
        throw std::invalid_argument("Argument 'value_node' need be a scalar!");
    }

    // Find out how the old value should be read and get the value
    const auto read_as = get_as<std::string>("read_old_value_as",
                                             noise_params, "double");
    double value;

    if (read_as == "double") {
        value = value_node.template as<double>();
    }
    else if (read_as == "int") {
        value = value_node.template as<int>();
    }
    else if (read_as == "unsigned") {
        value = value_node.template as<unsigned int>();
    }
    else {
        throw std::invalid_argument("Invalid value for argument "
            "'read_old_value_as'! Got '" + read_as + "' but expected "
            "'double', 'int' or 'unsigned'.");
    }

    // The real-valued random number, depending on distribution
    double noise;

    // Get the name of the distribution to use
    const auto distr_name = get_as<std::string>("distribution", noise_params);

    if (distr_name == "none") {
        noise = 0.;
    }
    else if (distr_name == "uniform") {
        noise = std::uniform_real_distribution<double>(0., 1.)(*rng);
    }
    else if (distr_name == "symmetric") {
        noise = std::uniform_real_distribution<double>(-1., +1.)(*rng);
    }
    else if (distr_name == "normal") {
        noise = std::normal_distribution<double>(0., 1.)(*rng);
    }
    else if (distr_name == "lognormal") {
        noise = std::lognormal_distribution<double>(0., 1.)(*rng);
    }
    else if (distr_name == "exponential") {
        noise = std::exponential_distribution<double>()(*rng);
    }
    else if (distr_name == "extreme_value") {
        noise = std::extreme_value_distribution<double>(0., 1.)(*rng);
    }
    else if (distr_name == "deterministic_zero") {
        noise = 0.;
    }
    else if (distr_name == "deterministic_one") {
        noise = 1.;
    }
    else {
        throw std::invalid_argument("Got invalid 'distribution' argument: '"
            + distr_name + "'! Available values: uniform, symmetric, normal, "
            "lognormal, exponential, extreme_value, "
            "deterministic_one, deterministic_zero."
        );
    }

    // Find out the mode and the optional factor and offset parameters
    const auto mode = get_as<std::string>("mode", noise_params);
    const auto factor = get_as<double>("factor", noise_params, 1.);
    const auto offset = get_as<double>("offset", noise_params, 0.);

    // Depending on the mode, apply the noise in different ways
    if (mode == "scale") {
        value = value * (factor * noise);
    }
    else if (mode == "shift") {
        value = value + (factor * noise);
    }
    else if (mode == "shift_rel") {
        value = value + value * (factor * noise);
    }
    else if (mode == "random") {
        value = factor * noise + offset;
    }
    else {
        throw std::invalid_argument("Got invalid 'mode' argument: '"
            + mode + "'! Possible modes: scale, shift, shift_rel, random.");
    }

    // Perform clamp to make sure it is within desired bounds
    value = std::clamp(value,
                       get_as<double>("clamp_low", noise_params, value),
                       get_as<double>("clamp_high", noise_params, value));

    return value;
}


/// Applies noise to each element of a sequence
/** Currently, the same `noise_params` are used for all elements.
  */
template<class RNG>
std::vector<double>
apply_noise_to_sequence (const DataIO::Config& seq_node,
                         const DataIO::Config& noise_params,
                         const std::shared_ptr<RNG>& rng)
{
    if (not seq_node or not seq_node.IsSequence()) {
        throw std::invalid_argument("Argument 'seq_node' need be a sequence!");
    }

    // TODO add possibility to only apply noise to certain elements?

    auto seq = std::vector<double>{};
    for (auto e : seq_node) {
        seq.push_back(apply_noise_to_value(e, noise_params, rng));
    }
    return seq;
}



/// Apply noise to parameters specified in the given configuration
/** Invokes apply_noise_to_value on some specified configuration nodes.
  * Uses EEcosy::recursive_getitem and EEcosy::recursive_setitem for retrieving
  * elements from `cfg` and setting them, respectively.
  *
  * \param cfg           Node in which to find the entries to apply noise to
  * \param rng           The RNG to use
  * \param noise_params  Noise parameters. The top level keys inside this node
  *                      need to be keys (or key sequences, delimited by `.`)
  *                      that can be found (recursively) inside `cfg`.
  *                      If not given, will try and look for a key
  *                      `_apply_noise` inside `cfg`.
  *
  * \note  This works on a clone of the given config, i.e. a deep copy. This is
  *        required as values in the hierarchy can be changed, which would have
  *        side effects ...
  *
  * \note  The new value is always stored back into the node as a double as the
  *        universal numeric data type.
  *
  * \returns DataIO::Config A copy of the given configuration node with all
  *                      configured entries applied noise to.
  */
template<class RNG>
Config
apply_noise (const Config& cfg,
             const std::shared_ptr<RNG>& rng,
             const Config& noise_parameters = {})
{
    // Determine which noise parameters to use
    Config noise_params = cfg["_apply_noise"];  // ok to be a zombie

    if (noise_parameters and noise_parameters.size()) {
        // Use the given ones instead, ignoring any potentially available
        // _apply_noise entry ...
        noise_params = noise_parameters;
    }

    // Do not apply noise if no parameters were given or if explicitly disabled
    if (   not noise_params or noise_params.size() == 0
        or not get_as<bool>("_enabled", noise_params, true))
    {
        // Return a copy of the unchanged object
        return YAML::Clone(cfg);
    }

    // Work on a clone the given configuration
    Config new_cfg = YAML::Clone(cfg);

    // Go over all keys in the noise_params
    // TODO allow going deeper than top level in cfg... somehow!
    for (const auto& entry : noise_params) {
        // Unpack the returned <Node, Node> pair
        const auto key = entry.first.as<std::string>();
        const auto& params = entry.second;

        // Check for control parameters; cannot handle other scalars
        if (params.IsScalar()) {
            if (key.substr(0, 1) == "_") {
                continue;
            }
            throw std::invalid_argument("Got scalar entry '" + key + "' in "
                "apply noise parameters; at this level, only control "
                "parameters (starting with '_') may be scalars!");
        }

        // If the params state that this noise application is disabled, skip
        // it ... by default, this is enabled.
        if (not get_as<bool>("enabled", params, true)) {
            continue;
        }

        // Check if there is an entry with the specified key
        // if (not new_cfg[key]) {
        //     throw KeyError(key, new_cfg, "Could not apply noise!");
        // }
        // Get the value node
        Config item;
        try {
            item = recursive_getitem(new_cfg, key);
        }
        catch (Utopia::KeyError& err) {
            throw
                Utopia::KeyError(key, new_cfg,
                    fmt::format("During apply_noise, retrieving an original "
                                "config node failed! {}\n--- Error on root "
                                "level of the apply_noise mapping:",err.what())
                );
        }

        // Distinguish by node type, apply noise, and set the new value
        if (item.IsScalar()) {
            recursive_setitem(new_cfg, key,
                              apply_noise_to_value(item, params, rng));
        }
        else if (item.IsSequence()) {
            recursive_setitem(new_cfg, key,
                              apply_noise_to_sequence(item, params, rng));
        }
        else {
            const auto item_str = Utopia::DataIO::to_string(item);
            throw
                std::invalid_argument(fmt::format(
                    "Unexpected node type! Can only apply noise to scalar or "
                    "sequence nodes, but got:\n{}", item_str
                ));
        }
    }

    // Determine what to do with the `_apply_noise` node, if it is present
    if (new_cfg["_apply_noise"]) {
        const auto aa_action = get_as<std::string>("_after_application",
                                                   noise_params,
                                                   "disable");
        if (aa_action == "disable") {
            new_cfg["_apply_noise"]["_enabled"] = false;
        }
        else if (aa_action == "remove") {
            new_cfg.remove("_apply_noise");
        }
        else if (aa_action == "persist") {
            // Do nothing
        }
        else {
            throw std::invalid_argument("Invalid value '" + aa_action + "' "
                "for parameter '_after_application'! Possible values are: "
                "persist, remove, disable.");
        }
    }

    return new_cfg;
}


} // namespace Utopia::Utils

#endif // UTOPIA_MODELS_MESONET_UTILS_APPLY_NOISE_HH
