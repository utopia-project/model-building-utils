#ifndef UTOPIA_DATA_IO_DATA_WRITER_TIME_MANAGER_HH
#define UTOPIA_DATA_IO_DATA_WRITER_TIME_MANAGER_HH

#include <algorithm>
#include <vector>
#include <array>

#include <fmt/core.h>

#include "utils.hh"


namespace Utopia::DataIO::DataWriter {

/// Whether the TimeManager should use padding for the time string or not
enum class Padding { on, off };

/// Whether the TimeManager should check the new time values for correctness
enum class CheckTime { on, off };


/// Manages writing times for the DataWriter
/**
 *
 *  \tparam padding     If Padding::on, will pad the time with zeros.
 *                      In regular mode, the length of the stop time is used
 *                      to determine the padding width (even if that means that
 *                      there is an unnecessary zero in front). For irregular
 *                      times, the pad width can either be supplied directly
 *                      or the maximum of the `Time` type is used.
 *  \tparam Time        Type to use for times.
 */
template<Padding padding=Padding::off, class Time=uint_fast32_t>
class TimeManager {
public:
    /// Whether writing times can be represented as range expression
    const bool regular;

private:
    /// Whether tracking has started
    bool started_tracking;

    /// The latest tracked time of writing
    Time time;

public:
    /// The start of the range expression
    const Time start;

    /// The end of the range expression (excluded)
    const Time stop;

    /// The step width of the range expression
    const Time step;

private:
    /// For irregular mode, registered writing times
    std::vector<Time> times;

    /// For irregular writing, the maximum value
    const Time max_num_writes;

    /// A cache value for remaining write operations
    Time cache_num_remaining_writes;

    /// The length of the time string (needed for Padding::on)
    unsigned time_str_len;

    /// A string representation of the current time
    std::string time_str;

public:
    /// Construct a regular TimeManager given start, stop and step parameters
    template<class _Time>
    TimeManager (_Time start, _Time stop, _Time step = 1)
    :
        regular(true)
    ,   started_tracking(false)
    ,   time(0)
    ,   start(start)
    ,   stop(stop)
    ,   step(step)
    ,   times{}
    ,   max_num_writes(step ? num_remaining_writes() : 0)
    ,   time_str_len(std::to_string(stop).length())
    ,   time_str(create_current_time_str())
    {
        if (stop < start or step < 1) {
            throw std::invalid_argument(fmt::format(
                "Got invalid start ({}), stop ({}), and/or step ({}) argument "
                "for TimeManager!", start, stop, step
            ));
        }
    }

    /// Construct a TimeManager from a Model instance
    /** Will extract most information from the model, but allows to create a
     *  non-regular TimeManager.
     *
     *  \param model    The Utopia::Model instance to retrieve time info from,
     *                  particulary: start, stop and step in the case of
     *                  regular times.
     *  \param regular  Can set this to false to allow non-regular (but still
     *                  monotonically increasing) write times.
     *  \param max_num_writes  Defines the maximum number of times that will
     *                  be tracked.
     *  \param pad_width  If initialized with Padding::on, can explicitly
     *                  provide a padding width here. If not given, will use
     *                  the length of the maximum possible time value.
     */
    template<class Model>
    TimeManager (
        const Model& model,
        bool regular = true,
        Time max_num_writes = std::numeric_limits<Time>::max(),
        unsigned pad_width = 0
    )
    :
        regular(regular)
    ,   started_tracking(false)
    ,   time(0)
    ,   start(regular ? model.get_write_start() : 0)
    ,   stop(regular ? model.get_time_max() + 1 : 0)
    ,   step(regular ? model.get_write_every() : 0)
    ,   times{}
    ,   max_num_writes(regular ? num_remaining_writes() : max_num_writes)
    ,   time_str_len([&]() -> unsigned {
            if (pad_width) {
                if constexpr (padding == Padding::off) {
                    throw std::invalid_argument(
                        "With Padding::off, cannot specify non-zero value for "
                        "argument `pad_width` in TimeManager!"
                    );
                }
                return pad_width;
            }
            return std::to_string(
                regular ? stop : std::numeric_limits<Time>::max()
            ).length();
        }())
    ,   time_str(create_current_time_str())
    {}


    // .. Setters .............................................................

    /// Tracks a new writing time
    /** This should be called *before* performing a write operation.
     *
     *  In case of irregular write times, every new time value is stored.
     *  For regular times, there is no need to store them because they can be
     *  reconstructed.
     *
     *  The new tracked time values need to match certain values, especially
     *  in the regular case where they need to fit the range expression given
     *  by `[start, stop, step]`.
     *
     *  \note Times need to grow strictly monotonically! For regular write
     *        times, the new time needs to be the
     *
     *  \param time     The new time that is to be tracked.
     *
     *  \tparam check   If CheckTime::off, will *skip* the checks of the
     *                  new write time value.
     *                  Note that this may lead to invalid tracked time
     *                  values. Only do this if you can ensure that the times
     *                  will be correct!
     */
    template<CheckTime check = CheckTime::on>
    void track_write_time (const Time& new_time) {
        if constexpr (check == CheckTime::on) {
            if (regular) {
                if (not time_in_range(new_time)) {
                    throw std::invalid_argument(fmt::format(
                        "Invalid new write time: {}! Check that it is part of "
                        "the regular range (start: {}, stop: {}, step: {}).",
                        new_time, start, stop, step
                    ));
                }
                else if (started_tracking and (time + step != new_time)) {
                    throw std::invalid_argument(fmt::format(
                        "Invalid new write time: {}! The next valid regular "
                        "time is {}.", new_time, time + step
                    ));
                }
            }
            else if (started_tracking and new_time <= time) {
                throw std::invalid_argument(fmt::format(
                    "Invalid new write time: {}! Time needs to grow strictly "
                    "monotonically, but {} <= {}.", new_time, new_time, time
                ));
            }
            else if (num_remaining_writes() <= 0) {
                throw std::invalid_argument(fmt::format(
                    "Maximum number of write times ({}) exceeded! Use a "
                    "larger `max_num_writes` argument during construction.",
                    max_num_writes
                ));
            }
        }

        started_tracking = true;
        time = new_time;
        time_str = create_current_time_str();
        if (not regular) {
            times.push_back(time);
        }
    }


    // .. Getters .............................................................

    /// Returns const reference to the current time of the time manager
    const Time& current_time () const {
        throw_if_tracking_has_not_started_yet();
        return time;
    }

    /// Returns a string representation of the current time
    const std::string& current_time_str () const {
        throw_if_tracking_has_not_started_yet();
        return time_str;
    }

    /// Returns the number of possible remaining write operations
    /** In the regular case, this depends on the current time, assuming that
     *  a write operation has *not* yet occurred at that time.
     *
     *  \FIXME There are a bunch of things wrong with this one, mostly one-off
     *         errors ...
     */
    std::size_t num_remaining_writes () const {
        if (regular) {
            if (time > start) {
                return (stop - time) / step;
                // FIXME prematurely returns zero
            }
            else {
                // before reaching start time, should always be >=1
                // FIXME (unless stop == start)
                return std::max(
                    static_cast<std::size_t>(1),
                    static_cast<std::size_t>((stop - start) / step)
                );
            }
        }
        else {
            return max_num_writes - times.size();
        }
    }

    /// Return a range expression (start, stop, step) for regular writing times
    std::array<Time, 3> get_time_range_expr () const {
        if (not regular) {
            throw std::invalid_argument(
                "Can't get time range expression if the writing "
                "times are not regular!"
            );
        }
        return {start, stop, step};
    }

    /// Return a copy of the write times container
    std::vector<Time> get_times () const {
        if (regular) {
            throw std::invalid_argument(
                "Retrieving the times container is not supported for regular "
                "TimeManager instances. Use get_time_range_expr() instead!"
            );
        }
        return times;
    }


    // .. Other methods .......................................................

    /// Writes time coordinate attributes to the dataset
    /** \detail Writes two attributes:
      *            - ``coords__time``: (time, step)
      *            - ``coords_mode__time`` : 'start_and_step', i.e. how to
      *              interpret the above.
      *         The ``stop`` value can and should be inferred from the shape
      *         of the dataset.
      *
      * \note   For attributing the time coordinates to an axis of the
      *         dataset, the corresponding axis-specifying attributes need to
      *         be set separately.
      *
      * \param  dset         The dataset to add attributes to
      *
      * \returns void
      */
    void add_time_coords_attributes (const std::shared_ptr<HDFDataset>& dset) {
        throw_if_tracking_has_not_started_yet();

        if (regular) {
            dset->add_attribute(
                "coords__time", std::array<Time, 2>{time, step}
            );
            dset->add_attribute(
                "coords_mode__time", "start_and_step"
            );
            // stop is inferred from the size of the dataset
        }
        else {
            throw std::invalid_argument(
                "Time coordinate attributes cannot yet be written from the "
                "non-regular TimeManager!"
            );
        }
    }


private:
    /// Returns left-side-zero-padded string representation of the current time
    std::string create_current_time_str () const {
        using Utopia::Utils::pad_left;

        if constexpr (padding == Padding::on) {
            return pad_left(std::to_string(time), time_str_len, "0");
        }
        else {
            return std::to_string(time);
        }
    }

    /// True if the given time is part of this TimeManager's regular range
    bool time_in_range (Time t) const {
        return (
            t >= start and
            (t - start) % step == 0 and
            t < stop
        );
    }

    void throw_if_tracking_has_not_started_yet () const {
        if (not started_tracking) {
            throw std::runtime_error(
                "Time tracking has not started yet! "
                "TimeManager::track_write_time needs to be invoked at least "
                "once before the TimeManager can be used."
            );
        }
    }
};

/// A TimeManager with paddings
using PaddedTimeManager = TimeManager<Padding::on>;


} // namespace Utopia::DataIO::DataWriter

#endif // UTOPIA_DATA_IO_DATA_WRITER_TIME_MANAGER_HH
