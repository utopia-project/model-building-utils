#ifndef UTOPIA_DATA_IO_DATA_WRITER_DATA_WRITER_HH
#define UTOPIA_DATA_IO_DATA_WRITER_DATA_WRITER_HH

#include <string>
#include <unordered_map>
#include <unordered_set>

#include <utopia/core/logging.hh>
#include <utopia/core/exceptions.hh>
#include <utopia/data_io/cfg_utils.hh>
#include <utopia/data_io/hdfgroup.hh>
#include <utopia/data_io/hdfdataset.hh>

#include "utils.hh"
#include "property.hh"
#include "time_manager.hh"


namespace Utopia::DataIO::DataWriter {


/// Make Config type available in this namespace
using Config = Utopia::DataIO::Config;

/// Type of name -> property mapping
using PropDict = std::unordered_map<std::string, Property>;


/// Gathers different kinds of properties that are used in the DataWriter
struct Properties {
    /// Properties that are not to be used
    PropDict disabled;

    /// The standalone properties to write
    PropDict standalone;

    /// The mapped properties to write
    PropDict mapped;

    /// Properties attached to ID mapping properties
    PropDict attached;

    /// Buffered properties, attached to buffer-mappings
    PropDict buffered;

    /// Any kind of mapping properties, i.e. mappings *and* buffer-mappings
    PropDict mapping;

    /// Whether properties are locked, i.e. can/should not be added to any more
    bool locked = false;

    /// Adds a property instance to the corresponding dict
    template<class Prop=Property>
    void add (Prop&& prop) {
        if (locked) {
            throw std::invalid_argument(fmt::format(
                "Properties are already locked, cannot register {}!",
                prop.logstr
            ));
        }

        // Distinguish by mode to which map to add the property
        if (prop.mode == PropMode::standalone) {
            standalone.insert({prop.path, prop});
        }
        else if (prop.mode == PropMode::mapped) {
            mapped.insert({prop.path, prop});
        }
        else if (prop.mode == PropMode::attached_to_mapping) {
            attached.insert({prop.path, prop});
        }
        else if (prop.mode == PropMode::buffered) {
            buffered.insert({prop.path, prop});
        }
        else if (   prop.mode == PropMode::mapping
                 or prop.mode == PropMode::buffer_mapping)
        {
            mapping.insert({prop.path, prop});
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Unsupported property mode for {}!", prop.logstr
            ));
        }
    }

    /// Total number of registered properties (including disabled)
    auto size () const {
        return (
            disabled.size()
            + standalone.size()
            + mapped.size()
            + attached.size()
            + buffered.size()
            + mapping.size()
        );
    }
};




/// A class to write variably-sized data in a streamlined fashion
/**
 *
 *  \note While the task of this data writer is similar to that of the
 *        Utopia::DataIO::DataManager, it has a different architecture:
 *        This data writer allows registering properties at run time, thus
 *        allowing for simpler and expandable setup; the downside of this is
 *        that it cannot make use of compile-time optimizations of the write
 *        tasks and other logic.
 */
class DataWriter {
private:
    // -- Members -------------------------------------------------------------
    /// The configuration
    const Config cfg;

    /// A local logger
    const std::shared_ptr<spdlog::logger> log;

    /// Time manager
    const std::shared_ptr<TimeManager<>> tm;

    /// The base group to write all properties of this DataWriter to
    const std::shared_ptr<HDFGroup> base_group;

    /// All registered properties
    Properties props;

    /// Container of invalidated ID mapping names
    std::unordered_set<std::string> invalidated_mappings;

    /// Which properties to selectively enable
    std::set<std::string> enable_only;

    /// Which properties to selectively disable
    std::set<std::string> disable_only;

public:
    /// Whether writing is enabled
    bool enabled;

    /// Whether to throw an exception on write errors (or just log it)
    bool throw_on_write_error;

    /// The default compression level, stored in property
    unsigned short int compress_level;


public:
    // -- Constructor and methods ---------------------------------------------
    /// Construct a DataWriter instance
    /** The DataWriter can be populated with properties, which can be adjusted
     *  before the first write operation.
     *  Behavior is controlled via a configuration node, allowing the following
     *  keys:
     *
     *  - `enabled`: whether the DataManager should write any data at all
     *  - `enable_only` or `disable_only`: sequences of names that enable or
     *    disable certain properties. Entries are names. Can only give either
     *    of the two, not both. These are evaluated only at the first call
     *    to the `write` method.
     *  - `throw_on_write_error`: if false, will log errors instead of throwing
     *  - `compress_level`: the shared compression level for the HDF5 datasets
     *  - `log_level`: The log level of the embedded logger instance
     *
     *  \param parent_model  The associated model instance which is used to
     *                       retrieve the config (key: `data_writer`), a logger
     *                       and timing information for the TimeManager.
     *  \param custom_cfg    If given, will use this configuration instead of
     *                       retrieving it from the parent model
     *  \param props         Properties to register directly at construction.
     *                       These are optional and can also be provided via
     *                       the registration methods.
     */
    template<class Model, class PropertyContainer=std::vector<Property>>
    DataWriter (
        const Model& parent_model,
        const Config& custom_cfg = {},
        const PropertyContainer& props = {}
    )
    :
        // .. Infrastructure ..................................................
        cfg([&parent_model, &custom_cfg](){
            if (custom_cfg.size() > 0) return custom_cfg;

            auto cfg = parent_model.get_cfg()["data_writer"];
            if (not cfg) {
                // Allow fallback to old key for backwards compatibility
                cfg = parent_model.get_cfg()["data_manager"];
                if (not cfg) {
                    throw KeyError("data_writer", parent_model.get_cfg());
                }
            }
            return cfg;
        }())
    ,   log(init_logger(
            parent_model.get_logger()->name() + ".dw",
            parent_model.get_logger()->level(),
            false   // do not throw on existing logger of the same name
        ))
    ,   tm(std::make_shared<TimeManager<>>(parent_model))
    ,   base_group(parent_model.get_hdfgrp())

        // .. Property containers and flags ...................................
    ,   props()
    ,   invalidated_mappings{}

        // .. Behaviour-controlling members ...................................
    ,   enable_only([&](){
            std::set<std::string> _enable_only{};
            for (auto&& prop_path
                 : get_as<std::vector<std::string>>("enable_only", cfg, {}))
            {
                _enable_only.insert(prop_path);
            }
            return _enable_only;
        }())
    ,   disable_only([&](){
            std::set<std::string> _disable_only{};
            for (auto&& prop_path
                 : get_as<std::vector<std::string>>("disable_only", cfg, {}))
            {
                _disable_only.insert(prop_path);
            }
            return _disable_only;
        }())

        // .. Other parameters ................................................
    ,   enabled(get_as<bool>("enabled", cfg, true))
    ,   throw_on_write_error(get_as<bool>("throw_on_write_error", cfg, true))
    ,   compress_level(get_as<unsigned short int>("compress_level", cfg, 1))
    {
        // Configure logger
        if (cfg["log_level"]) {
            const auto lvl = get_as<std::string>("log_level", cfg);
            log->debug("Setting log level to '{}' ...", lvl);
            log->set_level(spdlog::level::from_str(lvl));
        }
        else {
            // No config value given; use level of the parent model's logger
            log->set_level(parent_model.get_logger()->level());
        }

        log->info("DataWriter ready.");

        // Register any properties that were given to the constructor
        for (const auto& prop : props) {
            register_property(prop);
        }
    }

    /// Returns the total number of registered properties (including disabled)
    auto num_properties () const {
        return props.size();
    }

    /// Can be called to invoke saving of the ID map
    /** If the ID map of the underlying properties got invalidated, this
      * method should be called to inform the DataWriter about the need to
      * save the IDs anew.
      *
      * As the invalidated mappings are stored in an unordered set, it is no
      * problem to call this function multiple times with the same argument.
      */
    template<bool write_to_log=true>
    void mark_mapping_invalidated (const std::string& mapping_path) {
        // Add it to the set of invalidated mappings
        invalidated_mappings.insert(mapping_path);

        if constexpr (write_to_log) {
            log->debug("Marked mapping '{}' as invalidated.", mapping_path);
        }
    }

    /// Shorthand to mark multiple mappings as invalidated
    template<bool write_to_log=true>
    void mark_mappings_invalidated (const std::vector<std::string>& paths) {
        for (const auto& path : paths) {
            mark_mapping_invalidated<write_to_log>(path);
        }
    }

    /// Writes out all registered properties
    /** Writing operations occur in the following order:
      *
      *    1. Invalidated mappings are updated, also triggering the update of
      *       buffer-mapped properties
      *    2. (Regular) mapped properties
      *    3. Standalone properties
      *    4. The `last_update_time` is stored in the base group attributes
      */
    template<class Time>
    void write (const Time time) {
        if (not enabled) {
            return;
        }
        log->debug("Writing data for time {:d} ...", time);

        // Update the time in the time manager
        tm->track_write_time(time);

        // On first write, need to make sure properties are locked
        if (not props.locked) {
            // Set them up. Also sets props.locked flag
            setup_properties();
        }

        // If needed, update mappings & open new datasets for mapped
        // properties.
        // This also writes out all properties that are attached to these
        // mappings, i.e. mapping-attached and buffered properties.
        if (invalidated_mappings.size() > 0) {
            update_invalidated_mappings();
        }

        // Write out mapped properties
        for (const auto& [prop_path, prop] : props.mapped) {
            prop.write();
        }

        // Last: Write out standalone properties
        for (const auto& [prop_path, prop] : props.standalone) {
            prop.write();
        }

        // Store the time of (this) last update in the base group
        base_group->add_attribute("last_update_time", time);
        log->debug("Writing finished successfully.");
    }

    /// Triggers writing of buffered properties
    /** Note that this still requires the corresponding mapping to be
      * invalidated first, otherwise no writing will occur.
      */
    void write_buffered_properties () {
        log->debug("Updating {} invalidated buffer-mappings(s) ...",
                   invalidated_mappings.size());

        for (const auto& mapping_name : invalidated_mappings) {
            log->trace("mapping name: {}", mapping_name);

            // Update the mapping, if it was a buffered one
            try {
                auto& mapping = props.mapping.at(mapping_name);
                if (mapping.mode != PropMode::buffer_mapping) {
                    continue;
                }
                mapping.set_head();
                mapping.write();
            }
            catch (const std::out_of_range&) {
                // mapping was disabled, so the `.at` failed.
                // Can still continue with the rest though ...
            }

            // Update buffered properties
            // Unlike in update_invalidated_mappings, make sure the head is set
            for_each_if_mapping_matches(props.buffered, mapping_name,
                [](auto& prop){
                    prop.set_head();
                    prop.write();
                }
            );
        }
    }

protected:

    /// Updates invalidated mappings and associated properties
    void update_invalidated_mappings () {
        log->debug("Updating {} invalidated mapping(s) ...",
                   invalidated_mappings.size());

        for (const auto& mapping_name : invalidated_mappings) {
            log->trace("mapping name: {}", mapping_name);

            // .. Update mappings .............................................
            // Invalidated mappings might be regular ones or buffer-mappings.
            // If the corresponding property is enabled, update its head and
            // invoke writing of the new map.
            // Note that the buffer-mapping might ignore setting the head, as
            // it might already have a head dataset (which does not change).
            try {
                auto& mapping = props.mapping.at(mapping_name);
                mapping.set_head();
                mapping.write();
            }
            catch (const std::out_of_range&) {
                // mapping was disabled, so the `.at` failed. No biggy, though.
            }

            // .. Update associated properties ................................
            // For all properties that use this mapping, update the head, write
            // out data, or do both.
            // These actions should be carried out even if the mapping itself
            // is disabled, as the properties may exist independently from the
            // associated mapping.

            // Attached properties
            // Need a new head and write into it (not called in write method)
            for_each_if_mapping_matches(props.attached, mapping_name,
                [](auto& prop){
                    prop.set_head();
                    prop.write();
                }
            );

            // Buffered properties
            // Don't need a new head, write into the existing, which is ensured
            // to be set if setup_properties was called.
            for_each_if_mapping_matches(props.buffered, mapping_name,
                [](auto& prop){
                    prop.write();
                    // TODO Safety check: does extent match that of the map?!
                }
            );

            // Mapped properties
            // Need a new head, writing happens in `write` method itself
            for_each_if_mapping_matches(props.mapped, mapping_name,
                [](auto& prop){
                    prop.set_head();
                }
            );
        }

        // ID mapping valid again now
        invalidated_mappings.clear();
        log->debug("All mappings valid again.");
    }



public:
    // -- Property Registration -----------------------------------------------

    /// Register a new property
    template<class Prop=Property>
    void register_property (Prop&& prop) {
        auto prop_mode = prop.mode;
        auto prop_path = prop.path;
        auto prop_logstr = prop.logstr;

        props.add(std::move(prop));

        if (prop_mode == PropMode::mapping or
            prop_mode == PropMode::buffer_mapping)
        {
            // Need always be invalid after registration such that the
            // structure gets set up after first invocation
            mark_mapping_invalidated<false>(prop_path); // false: no logging
        }
        log->trace("Registered {}.", prop_logstr);
    }


    /// Register a property
    /** Use this method to register a mapped or buffered property, linked to
      * a mapping or buffer-mapping, respectively.
      *
      * \note  If you intend to create a buffered property that flushes an
      *        existing container, consider using the specialized overloads.
      */
    void register_property (
        std::string property_path,
        PropMode mode,
        std::string id_map_path,
        WriteFunc write_func,
        ShapeFunc max_extent_func,
        AttrFunc attr_func = empty_attrs,
        bool use_default_attrs = true,
        GrpAttrFunc grp_attr_func = empty_grp_attrs,
        bool use_default_grp_attrs = true,
        ShapeFunc chunks_func = shape_0d
    ){
        // NOTE The Property object itself takes a very different argument
        //      order, so be careful if changing anything here ...
        //      The other register_property* methods all call this method in
        //      order to not have to be careful with argument order in multiple
        //      places ... this one is enough.
        //      Exposing the interface and the defaults here explicitly is done
        //      deliberately though in order to not have to dive deeper into
        //      the other code ...
        register_property(Property(
            mode, log, tm, base_group, property_path,
            write_func, id_map_path,
            attr_func, use_default_attrs,
            grp_attr_func, use_default_grp_attrs,
            max_extent_func, chunks_func, compress_level
        ));
    }


    /// Register a standalone, mapping, or buffer-mapping property
    /** This is a property that does not require an `id_map_path`.
      *
      * \note For a buffer mapping, consider using the specialized overloads.
      */
    template<class ...Args>
    void register_property (
        std::string property_path,
        PropMode mode,
        WriteFunc write_func,
        Args&&... args
    ){
        register_property(property_path, mode, "", write_func,
                          std::forward<Args>(args)...);
    }


    /// Register a buffer-mapping property
    /** Use this method to register a buffer-mapping property, i.e. a property
      * that serves as the mapping for a buffered property. Otherwise, a
      * buffer-mapping property behaves much the same as a buffered property.
      *
      * Currently, these are expected to be 1D and the size is set unlimited.
      *
      * \note  If the reference returned by `get_cont_ref` is not expected to
      *        become invalid over the course of the run, consider using the
      *        overload that constructs the reference retrieval function itself
      */
    template<class ContT, class ...Args>
    void register_buffer_mapping (
        std::string property_path,
        GetContRef<ContT> get_cont_ref,
        ShapeFunc max_extent_func = shape_1d_unlimited,
        Args&&... args
    ){
        register_property(property_path, PropMode::buffer_mapping, "",
                          make_buffer_writer(get_cont_ref), max_extent_func,
                          std::forward<Args>(args)...);
    }


    /// Register a buffer-mapping property
    /** Use this method to register a buffer-mapping property, i.e. a property
      * that serves as the mapping for a buffered property. Otherwise, a
      * buffer-mapping property behaves much the same as a buffered property.
      *
      * Currently, these are expected to be 1D and the size is set unlimited.
      *
      * This method creates a container retrieval function by itself, using the
      * specified container reference.
      *
      * \warning  The `cont` is expected to never be invalidated! If that is
      *           not the case, you should use the overload that lets you
      *           specify the container retrieval function manually.
      */
    template<class ContT, class ...Args>
    void register_buffer_mapping (
        std::string property_path,
        ContT& cont,
        ShapeFunc max_extent_func = shape_1d_unlimited,
        Args&&... args
    ){
        register_property(property_path, PropMode::buffer_mapping,
                          make_buffer_writer_from_ref(cont), max_extent_func,
                          std::forward<Args>(args)...);
    }


    /// Register a buffered property
    /** Use this method to register a buffered property, i.e. with a write
      * function that flushes the referenced container after writing.
      *
      * Currently, these are expected to be 1D and the size is set unlimited.
      *
      * \note  If the reference returned by `get_cont_ref` is not expected to
      *        become invalid over the course of the run, consider using the
      *        overload that constructs the reference retrieval function itself
      */
    template<class ContT, class ...Args>
    void register_buffered_property (
        std::string property_path,
        GetContRef<ContT> get_cont_ref,
        std::string id_map_path,
        ShapeFunc max_extent_func = shape_1d_unlimited,
        Args&&... args
    ){
        register_property(property_path, PropMode::buffered, id_map_path,
                          make_buffer_writer(get_cont_ref), max_extent_func,
                          std::forward<Args>(args)...);
    }


    /// Register a buffered property
    /** Use this method to register a buffered property, i.e. with a write
      * function that flushes the referenced container after writing.
      *
      * Currently, these are expected to be 1D and the size is set unlimited.
      *
      * \warning  The `cont` is expected to never be invalidated! If that is
      *           not the case, you should use the overload that lets you
      *           specify the container retrieval function manually.
      */
    template<class ContT, class ...Args>
    void register_buffered_property (
        std::string property_path,
        ContT& cont,
        std::string id_map_path,
        ShapeFunc max_extent_func = shape_1d_unlimited,
        Args&&... args
    ){
        register_property(property_path, PropMode::buffered, id_map_path,
                          make_buffer_writer_from_ref(cont), max_extent_func,
                          std::forward<Args>(args)...);
    }


protected:
    // -- Helper functions ----------------------------------------------------

    /// Set up properties and lock them; is called when write is first invoked
    void setup_properties () {
        // .. Evaluate the enable_only option .................................
        if (enable_only.size() or disable_only.size()) {
            update_active_properties(props.standalone);
            update_active_properties(props.mapped);
            update_active_properties(props.attached);
            update_active_properties(props.buffered);
            update_active_properties(props.mapping);

            log->info(
                "Active properties updated from {:d} `enable_only` "
                "and {:d} `disable_only` entries.",
                enable_only.size(), disable_only.size()
            );
        }

        // .. Setup properties that always write into the same dataset ........
        for (auto& [prop_path, prop] : props.standalone) {
            prop.set_head();
        }
        for (auto& [prop_path, prop] : props.buffered) {
            prop.set_head();
        }

        // .. Lock them now ...................................................
        props.locked = true;
        log->info("Properties all set up and locked now.");

        log->info("Enabled properties:");
        log->info("  standalone:         {}", props.standalone.size());
        log->info("  mapped:             {}", props.mapped.size());
        log->info("  attached:           {}", props.attached.size());
        log->info("  buffered:           {}", props.buffered.size());
        log->info("  (buffer-/)mapping:  {}", props.mapping.size());
        // TODO Emit more info here?!
    }


    /// Moves disabled properties from given container into props.disabled
    /** \todo Improve this. It's not very elegant, nor intuitive... mainly
      *       because the properties are distributed over several containers.
      *       Perhaps a pointer-based approach may be useful, allowing grouping
      *       into multiple containers ...
      *       Also: Should warn if trying to enable/disable a property that
      *       is not named at all (possible typo).
      */
    void update_active_properties (PropDict& pd) {
        if (not enable_only.size() and not disable_only.size()) {
            // Nothing to do, all remain enabled
            return;
        }
        else if (enable_only.size() and disable_only.size()) {
            throw std::invalid_argument(
                "Both `enable_only` and `disable_only` arguments were given; "
                "however, it is only valid to specify either of them!"
            );
        }
        const bool default_enabled = (disable_only.size() > 0);

        for (auto it = pd.begin(); it != pd.end(); ) {
            // Unpack pair
            auto& [prop_path, prop] = *it;

            // Check whether to remain enabled
            if (contains(enable_only, prop_path) or
                (default_enabled and not contains(disable_only, prop_path))
            ){
                // Remains enabled. Increment iterator, and continue ...
                log->debug("{} remains enabled.", prop.logstr);
                ++it;
                continue;
            }
            // Otherwise: Not enabled.

            // As the references prop_path and prop will become invalid after
            // the erase operation, but the log message should really be after
            // all has been finished, copy the logstring to emit it later.
            const auto _prop_logstr = prop.logstr;

            // Copy the property to the container of disabled properties
            props.disabled.insert({prop_path, prop});

            // Erase it from the container and update the iterator
            it = pd.erase(it);

            // NOTE Would like to use the below ... but it is not available
            //      in libc++ !! https://stackoverflow.com/a/52887708/1827608
            // props.disabled.insert(std::move(nh));

            log->debug("Disabled {}.", _prop_logstr);
        }
    }


    /// Helper function to call a functor on all properties using the mapping
    /** \TODO Potential (minor) speed improvement: Store the association
      *       between the mapping and the corresponding properties such
      *       that the repeatedly invoked string comparison is not needed...
      *       But might need different containers for that to distinguish the
      *       types? Unsure...
      */
    template<class F>
    static void for_each_if_mapping_matches (
        PropDict& props,
        const std::string mapping_name,
        F func
    ){
        for (auto& [prop_path, prop] : props) {
            if (prop.id_map_path == mapping_name) {
                func(prop);
            }
        }
    }

};


} // namespace Utopia::DataIO::DataWriter

#endif // UTOPIA_DATA_IO_DATA_WRITER_DATA_WRITER_HH
