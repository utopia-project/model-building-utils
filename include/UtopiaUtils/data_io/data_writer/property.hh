#ifndef UTOPIA_DATA_IO_DATA_WRITER_PROPERTY_HH
#define UTOPIA_DATA_IO_DATA_WRITER_PROPERTY_HH

#include <string>
#include <vector>

#include <hdf5.h>
#include <fmt/core.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <utopia/data_io/hdfgroup.hh>
#include <utopia/data_io/hdfdataset.hh>

#include "utils.hh"
#include "time_manager.hh"


namespace Utopia::DataIO::DataWriter {

/// The possible modes a property can operate in
enum class PropMode {
    /// A standalone dataset, no associations to anything else
    /** First dimension: ``time``
      */
    standalone,

    /// A dataset that is associated with some form of mapping along one dim
    /** First dimension: ``time``, second dimension from mapping, e.g. ``id``
      */
    mapped,

    /// A dataset that represents one form of mapping
    /** 1D dataset (dimension: ``id``), which is written only when invalidated.
      */
    mapping,

    /// A dataset that is attached to a mapping, but is itself not a mapping
    /** First dimension: ``id``
      *
      * This is written when the attached mapping is invalidated.
      */
    attached_to_mapping,

    /// A dataset that is repeatedly populated with values from a buffer
    /** First dimension: that of the attached buffer_mapping
      */
    buffered,

    /// A dataset that represents the mapping of a buffered data stream
    /** A single 1D dataset (dimension: ``id``), which is extended and written
      * to only when invalidated.
      */
    buffer_mapping
};

std::string mode2str (PropMode mode) {
    switch (mode) {
        case PropMode::standalone:          return "standalone";
        case PropMode::mapped:              return "mapped";
        case PropMode::mapping:             return "mapping";
        case PropMode::attached_to_mapping: return "mapping-attached";
        case PropMode::buffered:            return "buffered";
        case PropMode::buffer_mapping:      return "buffer-mapping";
        default:
            throw std::invalid_argument("Invalid mode!");
    };
}


/// A construct holding all information needed to write out one data property
/** In a broad sense, a property is "some data that's meant to be written".
 *  It consists of a path or a name that determines where to write the data to
 *  and a number of additional configuration parameters.
 *
 *  For instance, the property mode determines whether the property is to be
 *  written independently from other properties ("standalone") or whether it is
 *  coupled to other properties ("mapped" & "mapping"). Another class of modes
 *  is that of "buffered" properties, where data is first buffered and only
 *  written out at a later point.
 *
 *  This class is meant to write HDF5 data that is later read in by the dantro
 *  Python package. Also, it specializes on time-series data, assuming that
 *  some properties (depending on mode) are written to at certain regular or
 *  non-regular times.
 */
class Property {
public:
    // -- Members -------------------------------------------------------------
    /// Whether to throw on errors (true) or only log the errors (false)
    bool throw_on_error;

    /// In which mode this property is
    const PropMode mode;

    /// The path under which this property is stored
    /** \note The path is seen relative to the base group of the DataWriter!
      */
    const std::string path;

    /// The path of an ID mapping property (in the DataWriter)
    /** \note Relative to the base group of the DataWriter!
      */
    const std::string id_map_path;

    /// A short description string, including only the mode
    const std::string desc;

    /// A longer description string, e.g. for logging, including mode and path
    const std::string logstr;

    /// The logger instance for this property
    const std::shared_ptr<spdlog::logger> log;


protected:
    /// The time manager
    const std::shared_ptr<TimeManager<>> tm;

    /// The current head dataset's name
    std::string head_name;

    /// The current dataset this property writes data to
    std::shared_ptr<HDFDataset> head_dset;

    /// The base group where to create the actual group in
    const std::shared_ptr<HDFGroup> base_group;

    /// The group this property writes datasets to
    std::shared_ptr<HDFGroup> group;

    /// The dataset-writing function
    const WriteFunc write_func;

    /// Attribute-writing function
    const AttrFunc write_attributes;

    /// Whether to write default attributes
    const bool use_default_attributes;

    /// Group attribute writing function
    const GrpAttrFunc write_group_attributes;

    /// Whether to write default group attributes
    const bool use_default_group_attributes;

    /// A function to calculate the max extent of a dataset
    const ShapeFunc calc_max_extent;

    /// A function to calculate the chunks of a dataset
    const ShapeFunc calc_chunks;

    /// Compression level
    const unsigned short int compress_level;


    // .. State helpers .......................................................
    /// Whether this property will always write into the same dataset object
    /** This is the case for standalone, buffered, and buffer_mapping modes
      */
    const bool _fixed_head;

public:
    // -- Public interface ----------------------------------------------------
    /// Construct a data writing property
    /**
      *
      * \param mode              The mode of this Property
      * \param dw_logger         The DataWriter's logger
      * \param tm                The TimeManager instance
      * \param base_group        The group that's used as the base for the
      *                          given property path.
      * \param property_path     The path inside the base group. For properties
      *                          that create multiple head datasets, this will
      *                          be the path to the group these datasets will
      *                          be created in. Otherwise, it will be the path
      *                          to the dataset itself.
      * \param write_func        A functional that performs the data writing.
      * \param id_map_path       For mapped properties, the name of the mapped
      *                          property, i.e. its property_path.
      * \param write_attrs       A functional that writes attributes to the
      *                          head dataset
      * \param use_default_attrs Whether to write the default attributes
      * \param write_grp_attrs   A functional that writes attributes to the
      *                          group that is created for properties with
      *                          changing head datasets.
      * \param use_default_grp_attrs   Whether to write the default group
      *                          attributes.
      * \param calc_max_extent   A functional to compute the maximum extent of
      *                          all involved datasets. Receives the number of
      *                          remaining write operations as input. Note that
      *                          buffered or buffer-mapping properties should
      *                          typically set the max extend to be unlimited.
      * \param calc_chunks       A functional to compute the chunk sizes for
      *                          all involved datasets.
      * \param compress_level    The HDF5 compression level.
      * \param throw_on_error    If False, will only log writing errors.
      */
    Property (
        PropMode mode,
        const std::shared_ptr<spdlog::logger>& dw_logger,
        const std::shared_ptr<TimeManager<>>& tm,
        const std::shared_ptr<HDFGroup>& base_group,
        std::string property_path,
        WriteFunc write_func,
        std::string id_map_path = "",
        AttrFunc write_attrs = empty_attrs,
        bool use_default_attrs = true,
        GrpAttrFunc write_grp_attrs = empty_grp_attrs,
        bool use_default_grp_attrs = true,
        ShapeFunc calc_max_extent = shape_0d,
        ShapeFunc calc_chunks = shape_0d,
        unsigned short int compress_level = 1,
        bool throw_on_error = true
    )
    :
        // .. Key properties: mode and path, which will act as identifiers ....
        throw_on_error(throw_on_error)
    ,   mode(mode)
    ,   path(property_path)
    ,   id_map_path(id_map_path)

        // .. Infrastructure: description strings, logger, time manager .......
    ,   desc(mode2str(mode))
    ,   logstr([this](){
            return fmt::format("{} property '{}'", this->desc, this->path);
        }())
    ,   log(
            spdlog::stdout_color_mt(
                fmt::format("{}[{}]", dw_logger->name(), this->path)
            )
        )
    ,   tm(tm)

        // .. State-describing members and functionals ........................
    ,   head_name("")
    ,   head_dset()
    ,   base_group(base_group)
    ,   group()
    ,   write_func(write_func)
    ,   write_attributes(write_attrs)
    ,   use_default_attributes(use_default_attrs)
    ,   write_group_attributes(write_grp_attrs)
    ,   use_default_group_attributes(use_default_grp_attrs)
    ,   calc_max_extent(calc_max_extent)
    ,   calc_chunks(calc_chunks)
    ,   compress_level(compress_level)

        // .. Pre-evaluations of some repeatedly invoked expressions ..........
    ,   _fixed_head(
            mode == PropMode::standalone or
            mode == PropMode::buffer_mapping or
            mode == PropMode::buffered
        )
    {
        if (not tm->regular) {
            throw std::invalid_argument(
                "TimeManagers with non-regular times are not supported yet!"
            );
        }

        log->set_level(dw_logger->level());
        log->debug("Constructed as {} property.", desc);

        if (not id_map_path.empty()) {
            log->debug("Associated mapping: '{}'", id_map_path);
        }

        // Make a dimensionality check of max extent for buffered properties
        if (mode == PropMode::buffered or mode == PropMode::buffer_mapping) {
            auto max_extent = calc_max_extent(1);
            if (max_extent.size() > 1) {
                throw std::invalid_argument(fmt::format(
                    "Invalid max_extent dimensionality for {}! Need be 0D or "
                    "1D but was {:d}D. Check this property's max_extent "
                    "function.", logstr, max_extent.size()
                ));
            }
        }
    }

    /// Whether the head dataset is currently valid
    bool head_dset_valid () const {
        return bool(head_dset);
    }

    /// Whether the property will have a single (and fixed) head dataset
    /** This depends on mode alone and is determined during construction.
     *  The head dataset will be fixed for standalone or buffered modes.
     */
    bool fixed_head () const {
        return _fixed_head;
    }

    /// Invoke writing to the head dataset using the write function
    void write () const {
        log->trace("Writing ...");
        try {
            write_func(head_dset);
        }
        catch (std::exception& exc) {
            auto msg = fmt::format(
                "Writing {} failed! (Head valid? {})\nError: {}",
                logstr, (head_dset_valid() ? "Yes" : "No"), exc.what()
            );
            if (throw_on_error) {
                throw std::runtime_error(msg);
            }
            log->error(msg);
        }
        catch (...) {
            auto msg = fmt::format(
                "Writing {} failed with unknown error! (Head valid? {})",
                logstr, (head_dset_valid() ? "Yes" : "No")
            );
            if (throw_on_error) {
                throw std::runtime_error(msg);
            }
            log->error(msg);
        }
        log->trace("Writing succeeded.");
    }

    /// Set a new head dataset
    void set_head () {
        if (_fixed_head and head_dset_valid()) {
            // Nothing to do here, head was already set up and need not change
            return;
        }

        // Make sure there is a group associated
        if (not group) {
            set_group();
        }
        log->trace("Setting head ...");

        // Query the time manager for the remaining number of write operations
        const auto num_remaining_writes = tm->num_remaining_writes() + 1;
        // FIXME +1 is a workaround for error in TimeManager's calculation of
        //       the number of remaining write operations that sometimes leads
        //       to one-off errors...

        // Prepare for opening the dataset; this will only succeed if the
        // calculated max extent is nonzero for all its values
        const auto max_extent = calc_max_extent(num_remaining_writes);

        if (contains(max_extent, 0)) {
            // Reset the head dataset pointer to indicate that writing is
            // currently not possible.
            head_dset.reset();
            log->warn("The max_extent contained zeros; resetting head.");
            return;
        }
        // All good. Will be able to write.

        // Might need to set a new head name, depending on mode. In modes that
        // are standalone-like, head name is not used -> need not be set
        if (not _fixed_head) {
            head_name = tm->current_time_str();
        }

        // Open the new head dataset and store the returned shared pointer
        head_dset = group->open_dataset(head_name,
                                        std::move(max_extent),
                                        calc_chunks(num_remaining_writes),
                                        compress_level);
        log->trace("Set head to '{}'.", head_name);

        // Write default and user-specified attributes
        if (use_default_attributes) {
            write_default_attributes(head_dset);
        }
        write_attributes(head_dset);
    }


protected:
    // .. Helper methods ......................................................

    /// Sets the group this property write to depending on mode and path
    /** For fixed-head property modes, the group needs is the "parent group"
      * of the specified path and the head name is the last path segment.
      */
    void set_group () {
        log->trace("Setting group ...");

        if (not _fixed_head) {
            // Easy: Just open a new group inside the base group
            // Can already use the initialized path member
            group = base_group->open_group(path);
            log->trace("Set group to '{}' within base group.", path);

            // Write default and custom group attributes
            if (use_default_group_attributes) {
                // If multiple write operations will occur _per dataset_ in
                // this group, this should be marked as containing potentially
                // variably-sized data, written irregularly, a so-called
                // heterogeneous time series.
                // If only a single write operation will occur, a simple time
                // series suffices.
                if (   mode == PropMode::mapping
                    or mode == PropMode::attached_to_mapping)
                {
                    group->add_attribute("content", "time_series");
                }
                else {
                    group->add_attribute("content",
                                         "time_series_heterogeneous");
                }
            }
            write_group_attributes(group);

            return;
        }
        // else: we are in standalone-like modes, where it's not so easy:
        // Separate off the final segement of the path to use for the head
        // name; the base group is the path up to the final segment.

        // Can directly set the head name; in standalone mode, this will not
        // be changed again.
        head_name = path.substr(path.rfind("/") + 1, path.size());

        // Now, determine the group to write to
        const auto group_path = path.substr(0, path.size() - head_name.size());

        if (not group_path.length()) {
            // Write directly to the given base group
            group = base_group;
        }
        else {
            // Open a new group
            group = base_group->open_group(group_path);
        }
        log->trace("Set group to '{}' within base group.", group_path);
    }

    /// Writes the default attributes to the given dataset
    /** \note The defaults written here can be overwritten by user-specified
      *       attribute write functions.
      */
    void write_default_attributes (const std::shared_ptr<HDFDataset>& dset) {
        log->trace("Writing default attributes ...");

        // Depending on the mode, write default dimension names and coordinates
        if (mode == PropMode::standalone) {
            dset->add_attribute("dim_name__0", "time");
            tm->add_time_coords_attributes(dset);
        }
        else if (mode == PropMode::mapped) {
            dset->add_attribute("dim_name__0", "time");
            tm->add_time_coords_attributes(dset);

            dset->add_attribute("dim_name__1", "id");
            add_id_coords_attributes(dset);
        }
        else if (mode == PropMode::attached_to_mapping) {
            dset->add_attribute("dim_name__0", "id");
            add_id_coords_attributes(dset);
        }
        else if (mode == PropMode::mapping) {
            dset->add_attribute("dim_name__0", "idx");
            dset->add_attribute("coords_mode__idx", "trivial");
        }
        else if (mode == PropMode::buffered) {
            dset->add_attribute("dim_name__0", "id");
            add_id_coords_attributes(dset);
        }
        else if (mode == PropMode::buffer_mapping) {
            dset->add_attribute("dim_name__0", "idx");
            dset->add_attribute("coords_mode__idx", "trivial");
        }
        else {
            throw std::invalid_argument("Missing case for property mode!");
        }
    }

    /// Write info to attributes to specify coordinates of the ``id`` dimension
    void add_id_coords_attributes (const std::shared_ptr<HDFDataset>& dset) {
        // Compute the relative path, depending on mode
        std::string rel_path;
        if (mode == PropMode::buffered) {
            // Don't have changing head names, so the relative path needs to
            // be between the actual head datasets
            rel_path = make_rel_path("/" + id_map_path,
                                     "/" + path + "/..");
            // TODO Check that this ok with dantro?! Seems hacky ...
        }
        else {
            // Default case:
            // Start from the current property's group path (not from the head,
            // i.e. the dataset!) to the corresponding ID map. This relies on
            // the mapping having the same head name (which is expected).
            rel_path = make_rel_path("/" + id_map_path + "/" + head_name,
                                     "/" + path + "/");
        }

        // Set the coordinates mode to use another dataset for that info,
        // namely the one that is specified at the given relative path
        dset->add_attribute("coords_mode__id", "linked");
        dset->add_attribute("coords__id", std::move(rel_path));
    }
};


} // namespace Utopia::DataIO::DataWriter

#endif // UTOPIA_DATA_IO_DATA_WRITER_PROPERTY_HH
