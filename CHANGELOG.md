# Changelog

⚠️ This package is a test-bed for the [Utopia framework][Utopia] and functionality may move to the Utopia C++ library at any time, regardless of version numbers.

Semantics used for version numbering (`X.Y.Z`) in this project:

- Major version (`X`) will remain at 0 to denote the fluctuating state of this package.
- Minor version (`Y`) is incremented upon non-trivial additions or changes; these *may* be breaking changes.
- Patch version (`Z`) is incremented upon bug fixes and minor changes.


## v0.1.0 *(work in progress)*
- !2 migrates existing code from a previously used package with the same purpose.

### Internal
- !3 differentiates tests of each module into their own test module
- !1 sets up Cmake essentials


[Utopia]: https://gitlab.com/utopia-project/utopia
