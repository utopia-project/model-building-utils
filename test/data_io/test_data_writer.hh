#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <utopia/core/logging.hh>
#include <utopia/data_io/hdfgroup.hh>
#include <utopia/data_io/hdfdataset.hh>

#include <UtopiaUtils/data_io/data_writer.hh>

namespace Utopia::DataIO::DataWriter {

using Time = uint_fast32_t;


class MockModel {
private:
    std::string _name;
    Config _cfg;
    std::shared_ptr<spdlog::logger> _log;
    std::shared_ptr<HDFGroup> _hdfgrp;

public:
    Time time;
    Time write_start;
    Time time_max;
    Time write_every;

public:
    MockModel (
        std::string name,
        Config cfg,
        std::shared_ptr<HDFGroup> base_group
    )
    :   _name(name)
    ,   _cfg(cfg[name])
    ,   _log(init_logger(name, spdlog::level::trace, false))
    ,   _hdfgrp(base_group->open_group(_name))
    {
        time = 0;
        time_max = get_as<Time>("time_max", _cfg);
        write_start = get_as<Time>("write_start", _cfg);
        write_every = get_as<Time>("write_every", _cfg);
    }

    Config get_cfg () const {
        return _cfg;
    }

    auto get_logger () const {
        return _log;
    }

    auto get_hdfgrp () const {
        return _hdfgrp;
    }

    auto get_time () const {
        return time;
    }

    auto get_time_max () const {
        return time_max;
    }

    auto get_write_start () const {
        return write_start;
    }

    auto get_write_every () const {
        return write_every;
    }
};


} // namespace Utopia::DataIO::DataWriter
