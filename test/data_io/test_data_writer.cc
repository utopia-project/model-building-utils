#define BOOST_TEST_MODULE test data_writer

#include <cstdio>
#include <random>
#include <limits>
#include <string>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"

#include <utopia/data_io/hdffile.hh>
#include <utopia/data_io/hdfgroup.hh>

#include "test_data_writer.hh"




// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

using Utopia::DataIO::HDFFile;

using namespace Utopia::DataIO::DataWriter;


/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    const std::string hdffile_path;
    std::shared_ptr<HDFFile> hdffile;
    MockModel model;
    MockModel model_no_writer;

    Infrastructure ()
    :   BaseInfrastructure<>("data_writer.yml")
    ,   hdffile_path([&](){
            auto dist = std::uniform_int_distribution<int>(0, 999999);
            return fmt::format("test_data_writer_{}.h5", dist(*this->rng));
        }())
    ,   hdffile(std::make_shared<HDFFile>(hdffile_path, "w"))
    ,   model(
            "MockModel",
            this->cfg,
            hdffile->open_group("base")
        )
    ,   model_no_writer(
            "MockModel_no_writer",
            this->cfg,
            hdffile->open_group("base")
    )
    {}

    ~Infrastructure () {
        // Destroy the HDFFile object and remove the associated file
        hdffile.reset();
        std::remove(hdffile_path.c_str());
    }
};





// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// -- data_writer/utils.hh ----------------------------------------------------
BOOST_FIXTURE_TEST_SUITE (test_utils, Infrastructure)

BOOST_AUTO_TEST_CASE (shape_and_attribute_funcs) {
    // shape functions
    BOOST_TEST(
        shape_0d(42) == ShapeVec({}), tt::per_element()
    );
    BOOST_TEST(
        shape_1d(42) == ShapeVec({42}), tt::per_element()
    );
    BOOST_TEST(
        shape_1d_unlimited(42) == ShapeVec({H5S_UNLIMITED}), tt::per_element()
    );
    BOOST_TEST(
        shape_2d_unlimited(42) == ShapeVec({H5S_UNLIMITED, H5S_UNLIMITED}),
        tt::per_element()
    );

    // attribute functions
    auto dset = hdffile->open_dataset("test_dset");
    auto grp = hdffile->open_group("test_group");

    empty_attrs(dset);
    set_attrs_contains_yaml_data(dset);

    empty_grp_attrs(grp);
    set_grp_attrs_time_series(grp);
}

BOOST_AUTO_TEST_SUITE_END() // "test_utils"




// -- data_writer/time_manager.hh ---------------------------------------------
BOOST_FIXTURE_TEST_SUITE (test_TimeManager, Infrastructure)

BOOST_AUTO_TEST_CASE (construction) {
    // Can construct explicitly (without model)
    {
    auto tm = TimeManager(10, 21);

    BOOST_TEST(tm.regular);
    BOOST_TEST(tm.start == 10);
    BOOST_TEST(tm.stop == 21);
    BOOST_TEST(tm.step == 1);
    BOOST_TEST(tm.num_remaining_writes() == 11);
    }

    // Can construct from a (mock) model instance
    {
    auto tm = TimeManager(model);

    BOOST_TEST(tm.regular);
    BOOST_TEST(tm.start == 10);
    BOOST_TEST(tm.stop == 21);
    BOOST_TEST(tm.step == 1);
    BOOST_TEST(tm.num_remaining_writes() == 11);
    }

    // … and that one can also be irregular
    {
    auto tm = TimeManager(model, false, 100);

    BOOST_TEST(not tm.regular);
    BOOST_TEST(tm.start == 0);
    BOOST_TEST(tm.stop == 0);
    BOOST_TEST(tm.step == 0);
    BOOST_TEST(tm.num_remaining_writes() == 100);
    }

    // May fail
    BOOST_TEST_PASSPOINT();
    check_exception<std::invalid_argument>(
        [&](){
            TimeManager(2, 1);
        },
        "Got invalid start (2), stop (1)"
    );
    BOOST_TEST_PASSPOINT();
    check_exception<std::invalid_argument>(
        [&](){
            TimeManager(1, 2, 0);
        },
        "Got invalid start (1), stop (2), and/or step (0) argument"
    );
    BOOST_TEST_PASSPOINT();
    check_exception<std::invalid_argument>(
        [&](){
            TimeManager(model, false, 100, 1);
        },
        "With Padding::off, cannot specify non-zero value for argument `pad_"
    );
    BOOST_TEST_PASSPOINT();
}

BOOST_AUTO_TEST_CASE (regular_and_irregular) {
    // Regular case: can only get time range expression
    auto tm_r = TimeManager(model);

    BOOST_TEST(tm_r.regular);
    BOOST_TEST(tm_r.get_time_range_expr().size() == 3);
    check_exception<std::invalid_argument>(
        [&](){
            tm_r.get_times();
        },
        "not supported for regular"
    );

    // Irregular case
    auto tm_i = TimeManager(model, false);

    BOOST_TEST(not tm_i.regular);
    BOOST_TEST(tm_i.get_times().empty());
    check_exception<std::invalid_argument>(
        [&](){
            tm_i.get_time_range_expr();
        },
        "Can't get time range expression if the writing times are not regular"
    );
}

BOOST_AUTO_TEST_CASE (tracking_regular) {
    auto tm = TimeManager(10, 22, 3);

    // Cannot retrieve time before tracking has started
    check_exception<std::runtime_error>(
        [&](){ tm.current_time(); }, "Time tracking has not started yet!"
    );
    check_exception<std::runtime_error>(
        [&](){ tm.current_time_str(); }, "Time tracking has not started yet!"
    );

    // Invalid (i.e. not matching range) tracking times will throw
    check_exception<std::invalid_argument>(
        [&](){
            tm.track_write_time(0);
        },
        "Check that it is part of the regular range"
    );
    check_exception<std::invalid_argument>(
        [&](){
            tm.track_write_time(11);
        },
        "Check that it is part of the regular range"
    );

    // This will work
    tm.track_write_time(10);
    BOOST_TEST(tm.current_time() == 10);
    BOOST_TEST(tm.current_time_str() == "10");
    BOOST_TEST(tm.num_remaining_writes() == 4);

    // Still cannot choose invalid times or jump ahead
    check_exception<std::invalid_argument>(
        [&](){
            tm.track_write_time(11);
        },
        "Check that it is part of the regular range"
    );

    check_exception<std::invalid_argument>(
        [&](){
            tm.track_write_time(16);
        },
        "next valid regular time is 13"
    );

    // This works
    tm.track_write_time(13);
    BOOST_TEST(tm.current_time() == 13);
    BOOST_TEST(tm.current_time_str() == "13");
    BOOST_TEST(tm.num_remaining_writes() == 3);

    tm.track_write_time(16);
    BOOST_TEST(tm.num_remaining_writes() == 2);

    tm.track_write_time(19);
    BOOST_TEST(tm.num_remaining_writes() == 1);

    check_exception<std::invalid_argument>(
        [&](){
            tm.track_write_time(22);
        },
        "part of the regular range (start: 10, stop: 22, step: 3)"
    );

    // And it's also possible to deactivate the checks
    // (Should only be used when it's ensured that times are valid, though!)
    tm.track_write_time<CheckTime::off>(11);
    BOOST_TEST(tm.current_time() == 11);
    BOOST_TEST(tm.current_time_str() == "11");
}

BOOST_AUTO_TEST_CASE (tracking_irregular) {
    auto tm = TimeManager(model, false, 100);

    BOOST_TEST(not tm.regular);
    BOOST_TEST(tm.start == 0);
    BOOST_TEST(tm.stop == 0);
    BOOST_TEST(tm.step == 0);
    BOOST_TEST(tm.num_remaining_writes() == 100);
    BOOST_TEST(tm.get_times().empty());

    // Cannot retrieve time before tracking has started
    check_exception<std::runtime_error>(
        [&](){ tm.current_time(); }, "Time tracking has not started yet!"
    );
    check_exception<std::runtime_error>(
        [&](){ tm.current_time_str(); }, "Time tracking has not started yet!"
    );

    // Can store arbitrary (but monotonically increasing times)
    tm.track_write_time(123);
    tm.track_write_time(234);
    BOOST_TEST(tm.current_time() == 234);
    BOOST_TEST(tm.current_time_str() == "234");
    BOOST_TEST(tm.num_remaining_writes() == 98);
}

BOOST_AUTO_TEST_CASE (time_str) {
    using Utopia::Utils::pad_left;
    using Utopia::DataIO::DataWriter::Time;

    auto tm = TimeManager(0, 101, 50);
    auto tm_pad = PaddedTimeManager(0, 101, 50);
    auto tm_apad = PaddedTimeManager(model, false, 3);
    auto tm_cpad = PaddedTimeManager(model, false, 3, 5);

    const auto pw = std::to_string(std::numeric_limits<Time>::max()).length();

    tm.track_write_time(0);
    tm_pad.track_write_time(0);
    tm_apad.track_write_time(0);
    tm_cpad.track_write_time(0);

    BOOST_TEST(tm.current_time_str() == "0");
    BOOST_TEST(tm_pad.current_time_str() == "000");
    BOOST_TEST(tm_apad.current_time_str() == pad_left("0", pw, "0"));
    BOOST_TEST(tm_cpad.current_time_str() == "00000");

    tm.track_write_time(50);
    tm_pad.track_write_time(50);
    tm_apad.track_write_time(50);
    tm_cpad.track_write_time(50);

    BOOST_TEST(tm.current_time_str() == "50");
    BOOST_TEST(tm_pad.current_time_str() == "050");
    BOOST_TEST(tm_apad.current_time_str() == pad_left("50", pw, "0"));
    BOOST_TEST(tm_cpad.current_time_str() == "00050");

    tm.track_write_time(100);
    tm_pad.track_write_time(100);
    tm_apad.track_write_time(100);
    tm_cpad.track_write_time(100);

    BOOST_TEST(tm.current_time_str() == "100");
    BOOST_TEST(tm_pad.current_time_str() == "100");
    BOOST_TEST(tm_apad.current_time_str() == pad_left("100", pw, "0"));
    BOOST_TEST(tm_cpad.current_time_str() == "00100");
}

BOOST_AUTO_TEST_SUITE_END() // "test_TimeManager"





// -- data_writer/property.hh -------------------------------------------------
BOOST_FIXTURE_TEST_SUITE (test_Property, Infrastructure)

BOOST_AUTO_TEST_CASE (test_mode2str) {
    BOOST_TEST(mode2str(PropMode::standalone) == "standalone");
    BOOST_TEST(mode2str(PropMode::mapped) == "mapped");
    BOOST_TEST(mode2str(PropMode::mapping) == "mapping");
    BOOST_TEST(mode2str(PropMode::attached_to_mapping) == "mapping-attached");
    BOOST_TEST(mode2str(PropMode::buffered) == "buffered");
    BOOST_TEST(mode2str(PropMode::buffer_mapping) == "buffer-mapping");
}


BOOST_AUTO_TEST_CASE (construction_and_interface) {
    auto tm = std::make_shared<TimeManager<>>(0, 100);
    auto num_writes = 0u;

    auto p1 = Property(
        PropMode::standalone,
        log,
        tm,
        hdffile->open_group("p1"),
        "prop/one",
        [&](const auto& dset){
            dset->write(num_writes++);
        }
    );

    BOOST_TEST(p1.fixed_head());
    BOOST_TEST(p1.throw_on_error);
    BOOST_TEST(bool(p1.mode == PropMode::standalone));
    BOOST_TEST(p1.path == "prop/one");
    BOOST_TEST(p1.id_map_path == "");
    BOOST_TEST(p1.desc == "standalone");
    BOOST_TEST(p1.logstr == "standalone property 'prop/one'");

    tm->track_write_time(0);
    BOOST_TEST(not p1.head_dset_valid());
    p1.set_head();
    BOOST_TEST(p1.head_dset_valid());

    BOOST_TEST(num_writes == 0);
    p1.write();
    BOOST_TEST(num_writes == 1);
}


BOOST_AUTO_TEST_CASE (write_errors) {
    auto tm = std::make_shared<TimeManager<>>(0, 100);

    auto p2 = Property(
        PropMode::standalone,
        log,
        tm,
        hdffile->open_group("p2"),
        "prop/two",
        [&](const auto&){
            throw std::invalid_argument("foo");
        }
    );
    tm->track_write_time(0);
    p2.set_head();

    BOOST_TEST(p2.throw_on_error);
    check_exception<std::runtime_error>(
        [&](){
            p2.write();
        },
        "Writing standalone property 'prop/two' failed! (Head valid? Yes)\n"
        "Error: foo"
    );

    p2.throw_on_error = false;
    BOOST_TEST(not p2.throw_on_error);
    p2.write();
}


BOOST_AUTO_TEST_CASE (construction_and_interface_irregular) {
    auto tm = std::make_shared<TimeManager<>>(model, false);

    check_exception<std::invalid_argument>(
        [&](){
            Property(
                PropMode::standalone,
                log,
                tm,
                hdffile->open_group("p3"),
                "prop/three",
                [&](const auto&){}
            );
        },
        "not supported yet!"
    );
}

BOOST_AUTO_TEST_SUITE_END() // "test_Property"





// -- data_writer/data_writer.hh ----------------------------------------------
BOOST_FIXTURE_TEST_SUITE (test_DataWriter, Infrastructure)


BOOST_AUTO_TEST_CASE (construction) {
    // Can initialize from a model alone ...
    auto dwm = DataWriter(model);

    BOOST_TEST(dwm.enabled);
    BOOST_TEST(dwm.throw_on_write_error);
    BOOST_TEST(dwm.compress_level == 2);
    BOOST_TEST(dwm.num_properties() == 0);

    // ... if it includes the corresponding key
    check_exception<Utopia::KeyError>(
        [&](){
            auto dwm = DataWriter(model_no_writer);
        },
        "data_writer"
    );

    // Can initialize from a custom configuration
    auto dw = DataWriter(model, cfg["DataWriter"]);
    auto dw2 = DataWriter(model_no_writer, cfg["DataWriter"]);

    // Test different configs
    test_config_callable(
        [&](auto custom_cfg){
            DataWriter(model, custom_cfg);
        },
        cfg["construction"]
    );

    // Construct with properties
    auto tm = std::make_shared<TimeManager<>>(model);
    auto dwp = DataWriter(
        model,
        cfg["DataWriter"],
        {Property(
            PropMode::standalone,
            log,
            tm,
            hdffile->open_group("p1"),
            "some_prop",
            [&](const auto& dset){
                dset->write(123);
            }
        )}
    );
    BOOST_TEST(dwp.num_properties() == 1);
}

/// Tests basics of registering properties and writing
BOOST_AUTO_TEST_CASE (basics) {
    auto dw = DataWriter(model);

    // Counter to keep track of number of write operations
    auto num_writes = 0u;

    // .. Dummy write functions
    WriteFunc write_func = [&](const auto& dset){
        dset->write(num_writes++);
    };
    WriteFunc write_func_2d = [&](const auto& dset){
        int n = num_writes++;
        auto data = std::array<int, 3>{n, n, n};
        dset->write(data);
        // dset->write(data.begin(), data.end(), [](auto e){ return e; });
    };

    // .. Register different kinds of properties
    // Standalone
    dw.register_property(
        "prop/standalone",
        PropMode::standalone,
        write_func_2d,
        [](auto) -> ShapeVec { return {11, 3}; }
    );

    // With ID mapping: mapping and mapped and attached to mapping
    dw.register_property(
        "prop/ids",
        PropMode::mapping,  // -> only written when invalidated
        write_func,
        shape_1d_unlimited
    );
    dw.register_property(
        "prop/one",
        PropMode::mapped,
        "prop/ids",
        write_func,
        shape_1d_unlimited
    );
    dw.register_property(
        "prop/attached",
        PropMode::attached_to_mapping,  // -> only written when invalidated
        "prop/ids",
        write_func,
        shape_1d_unlimited
    );

    // Write for pre-determined time steps
    BOOST_TEST(num_writes == 0);
    dw.write(10);
    BOOST_TEST(num_writes == 4);

    // mappings (and attached properties) are only written when invalidated,
    // so the increment is down to two here
    dw.write(11);
    BOOST_TEST(num_writes == 6);
    dw.write(12);
    BOOST_TEST(num_writes == 8);

    // mark mappings invalidated such that it is written to a new head dataset
    dw.mark_mapping_invalidated("prop/ids");
    dw.write(13);
    BOOST_TEST(num_writes == 12);
    dw.write(14);
    BOOST_TEST(num_writes == 14);

    dw.mark_mappings_invalidated({"prop/ids", "prop/ids"}); // no effect
    dw.write(15);
    BOOST_TEST(num_writes == 18);

    // If disabled altogether, writing does nothing
    dw.enabled = false;
    dw.write(123123);
    dw.write(123123);
    dw.write(123123);
}


/// Tests registering buffered properties and writing them
BOOST_AUTO_TEST_CASE (buffered) {
    auto dw = DataWriter(model);

    // Buffer container to write data from
    auto i = 0u;
    auto N = 3u;
    auto ids = std::vector<unsigned>();
    auto data = std::vector<std::string>();

    auto repopulate = [&](){
        for (auto j = 0u; j < N; j++) {
            ids.push_back(++i);
            data.push_back(std::to_string(i));
        }
    };
    repopulate();
    BOOST_TEST(ids.size() == N);
    BOOST_TEST(data.size() == N);

    // -- Register a buffered mapping and a connected property
    dw.register_buffer_mapping("buf/ids", ids);
    dw.register_buffered_property("buf/data", data, "buf/ids");

    // Test writing to it, which should flush the corresponding containers
    dw.write(10);
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());

    // Empty and not invalidated -> will not write
    dw.write(11);
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());

    // Not empty but not invalidated -> will not write
    repopulate();
    dw.write(12);
    BOOST_TEST(not ids.empty());
    BOOST_TEST(not data.empty());

    // Not empty and invalidated -> will write
    dw.mark_mapping_invalidated("buf/ids");
    dw.write(13);
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());

    // Empty and invalidated -> nothing to write
    dw.mark_mapping_invalidated("buf/ids");
    dw.write(14);
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());

    // -- Can also write buffered properties separately
    repopulate();
    dw.mark_mapping_invalidated("buf/ids");
    dw.write_buffered_properties();
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());

    repopulate();
    dw.write_buffered_properties();
    BOOST_TEST(ids.empty());
    BOOST_TEST(data.empty());


    // -- Can also register with a container retrieval function instead
    // TODO How does this need to be called?!
    // dw.register_buffer_mapping(
    //     "buf/ref/ids", get_ids
    // );
    // dw.register_buffered_property(
    //     "buf/ref/data", get_data, "buf/by_ref/ids"
    // );
}

/// Tests writing with disabled properties present
BOOST_AUTO_TEST_CASE (disabled_properties) {
    auto dw = DataWriter(model, cfg["DataWriter_disable_some"]);

    // Counter to keep track of number of write operations
    auto num_writes = 0u;

    // .. Dummy write functions
    WriteFunc write_func = [&](const auto& dset){
        dset->write(num_writes++);
    };
    WriteFunc write_func_2d = [&](const auto& dset){
        int n = num_writes++;
        auto data = std::array<int, 3>{n, n, n};
        dset->write(data);
        // dset->write(data.begin(), data.end(), [](auto e){ return e; });
    };

    // .. Register different kinds of properties
    // Standalone
    dw.register_property(
        "disabled_standalone_prop",
        PropMode::standalone,
        write_func_2d,
        [](auto) -> ShapeVec { return {11, 3}; }
    );
    dw.register_property(
        "enabled_standalone_prop",
        PropMode::standalone,
        write_func_2d,
        [](auto) -> ShapeVec { return {11, 3}; }
    );

    // With ID mapping: mapping and mapped and attached to mapping
    dw.register_property(
        "disabled/ids",
        PropMode::mapping,
        write_func,
        shape_1d_unlimited
    );
    dw.register_property(
        "disabled/prop",
        PropMode::mapped,
        "disabled/ids",
        write_func,
        shape_1d_unlimited
    );
    dw.register_property(
        "enabled/prop",
        PropMode::mapped,
        "disabled/ids",  // sic, can still write this
        write_func,
        shape_1d_unlimited
    );

    BOOST_TEST(dw.num_properties() == 5);

    // Write for pre-determined time steps
    BOOST_TEST(num_writes == 0);
    dw.write(10);
    BOOST_TEST(num_writes == 2);
    dw.write(11);
    BOOST_TEST(num_writes == 4);
    dw.write(12);
    BOOST_TEST(num_writes == 6);

    // mark mappings invalidated; this should not make a difference because
    // the property is disabled
    dw.mark_mapping_invalidated("disabled/ids");
    dw.write(13);
    BOOST_TEST(num_writes == 8);
}




BOOST_AUTO_TEST_SUITE_END() // "test_DataWriter"
