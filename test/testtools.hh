#ifndef UTOPIA_UTILS_TESTS_TESTTOOLS_HH
#define UTOPIA_UTILS_TESTS_TESTTOOLS_HH

#include <spdlog/spdlog.h>
#include <armadillo>

#include <utopia/core/testtools.hh>
#include <utopia/core/testtools/utils.hh>
#include <utopia/core/types.hh>
#include <utopia/data_io/cfg_utils.hh>

#include <UtopiaUtils/utils/testtools.hh>
#include <UtopiaUtils/utils/types.hh>

#endif // UTOPIA_UTILS_TESTS_TESTTOOLS_HH
