#define BOOST_TEST_MODULE test str_tools

#include <string>

#include <UtopiaUtils/utils/string.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"

// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests pad_left
BOOST_AUTO_TEST_CASE (test_pad_left)
{
    // Set up a test string
    const std::string s = "foo";
    BOOST_TEST(pad_left(s, 5, "o") == "oofoo");
    BOOST_TEST(pad_left(s, 6, " ") == "   foo");
    BOOST_TEST(pad_left(s, 2, " ") == "foo");

    // Test error message
    check_exception<std::invalid_argument>(
        [&](){
            pad_left(s, 10, "abc");
        },
        "Need a pad_char of length 1, but got: 'abc'"
    );
}
