#define BOOST_TEST_MODULE test cfg

#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/cfg.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::Utils::tags;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("cfg.yml") {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BOOST_FIXTURE_TEST_SUITE (test_cfg, Infrastructure)

/// Test the recursive update function
BOOST_AUTO_TEST_CASE (test_recursive_update) {
    // First, assert some assumptions about key equivalence in Config trees
    auto test = Config{};
    std::string k = "1";
    test[1] = "foo";
    test[1.0] = "bar";
    test[k] = "baz";
    test["1"] = "spam";
    BOOST_TEST(test[1].as<std::string>() == "spam");
    BOOST_TEST(test[1.0].as<std::string>() == "spam");
    BOOST_TEST(test[k].as<std::string>() == "spam");
    BOOST_TEST(test["1"].as<std::string>() == "spam");
    BOOST_TEST(test.size() == 1);

    // Now, test the actual recursive update
    test_config_callable([&](const Config& params){
        auto d = params["d"];
        auto u = params["u"];
        auto expected = params["expected"];

        auto actual = recursive_update(d, u);

        BOOST_TEST_CONTEXT("Result of recursive update:\n" << actual) {
            // With operator== not working properly, compare the string
            // representations; this is equivalent to recursively going through
            // the config tree and checking the output, as std::string is the
            // universal native type for scalar nodes...
            std::stringstream actual_s, expected_s;
            actual_s << actual;
            expected_s << expected;
            BOOST_TEST(actual_s.str() == expected_s.str());
        };
    },
    cfg["recursive_update"], "recursive_update", {__LINE__, __FILE__});

    // Finally, test that this currently fails with non-native key types
    check_exception<YAML::Exception>(
        [&](){
            auto d = Config{};
            auto u = Config{};
            u[std::make_pair(1u, 1.)] = "foo";
            recursive_update(d, u);
        },
        "bad conversion",
        {__LINE__, __FILE__}
    );
}

/// Tests recursive_getitem
BOOST_AUTO_TEST_CASE (test_recursive_getitem) {
    const Config b = YAML::Clone(cfg["recursive_getitem"]["basics"]);

    BOOST_TEST(recursive_getitem(b, "lvl").as<int>() == 0);
    BOOST_TEST(recursive_getitem(b, "deeper.lvl").as<int>() == 1);
    BOOST_TEST(recursive_getitem(b, "deeper.deeper.lvl").as<int>() == 2);
    BOOST_TEST(recursive_getitem(b, "deeper.deeper.deeper.lvl").as<int>() ==3);

    check_exception<Utopia::KeyError>(
        [&](){
            recursive_getitem(b, "deeper.deeper.bad_key.foo");
        },
        "failed for key or key sequence 'deeper -> deeper -> bad_key -> foo'",
        {__LINE__, __FILE__}
    );
}

/// Tests recursive_setitem
BOOST_AUTO_TEST_CASE (test_recursive_setitem) {
    Config b = YAML::Clone(cfg["recursive_setitem"]["basics"]);

    BOOST_TEST(b["val"].as<int>() == 0);
    BOOST_TEST(b["deeper"]["val"].as<int>() == 1);
    BOOST_TEST(b["deeper"]["deeper"]["val"].as<int>() == 2);
    BOOST_TEST(b["deeper"]["deeper"]["deeper"]["val"].as<int>() == 3);

    recursive_setitem(b, "val", 42);
    BOOST_TEST(b["val"].as<int>() == 42);

    recursive_setitem(b, "deeper.val", 43);
    BOOST_TEST(b["deeper"]["val"].as<int>() == 43);

    recursive_setitem(b, "deeper.deeper.val", "44");
    BOOST_TEST(b["deeper"]["deeper"]["val"].as<std::string>() == "44");

    recursive_setitem(b, "some.new.val", 6.4);
    BOOST_TEST(b["some"]["new"]["val"].as<double>() == 6.4);
}

BOOST_AUTO_TEST_SUITE_END()
