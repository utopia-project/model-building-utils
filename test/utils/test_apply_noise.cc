#define BOOST_TEST_MODULE test apply_noise

#include <iostream>
#include <random>

#include <UtopiaUtils/utils/apply_noise.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("apply_noise.yml") {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ............................................................................
/// Test the apply_noise_to_value function directly
BOOST_FIXTURE_TEST_CASE (test_apply_noise_to_value, Infrastructure)
{
    // Iterate over the sequence of test parameters
    for (const auto& test_cfg : cfg["test_directly"]) {
        // Prepare the callable
        const auto params = test_cfg["func_params"];

        const auto func = [&params, this](){
            return apply_noise_to_value(params["value"],
                                        params["noise_params"],
                                        rng);
        };

        // Test that sampling it creates the correct results
        sample_callable(func, test_cfg);
    }
}

BOOST_FIXTURE_TEST_CASE (test_apply_noise_to_sequence, Infrastructure)
{
    test_config_callable(
        [&](auto params){
            auto rv = apply_noise_to_sequence(params["sequence"],
                                              params["noise_params"],
                                              rng);
            auto expected = get_as<std::vector<double>>("expected", params);
            BOOST_TEST(rv == expected, tt::per_element());
        },
        cfg["test_sequences"],
        "apply_noise_to_sequence",
        {__LINE__, __FILE__}
    );
}

// ............................................................................
/// Tests the wrapper around apply_noise
BOOST_FIXTURE_TEST_CASE (test_apply_noise, Infrastructure)
{
    test_config_callable(
        [&](auto params){
            auto rv = apply_noise(params["input"], rng);

            for (auto kv_pair : get_as<Config>("expected", params)) {
                auto keyseq = kv_pair.first.template as<std::string>();
                const auto expected = kv_pair.second.template as<int>();
                BOOST_TEST_CONTEXT("Key sequence " << keyseq) {
                    BOOST_TEST(recursive_getitem(rv, keyseq).template as<int>()
                               == expected);
                }
            }
        },
        cfg["test_wrapper"],
        "apply_noise wrapper",
        {__LINE__, __FILE__}
    );
}
