#define BOOST_TEST_MODULE test odeint

#include <tuple>
#include <array>
#include <vector>

#include <armadillo>
#include <boost/numeric/odeint.hpp>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"

#include <utopia/core/type_traits.hh>

#include <UtopiaUtils/utils/odeint.hh>


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace boost::numeric::odeint;
using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("odeint.yml") {}
};


// -- Harmonic oscillator -----------------------------------------------------

/// The type of container used to hold the state vector
using harm_osc_state_types = std::tuple<
    std::vector<double>,
    std::array<double, 2>,
    arma::Col<double>,
    arma::Col<double>::fixed<2>
>;

/// The type of container for the state vector, supporting range algebra
using harm_osc_state_types_range_algebra = std::tuple<
    std::vector<double>,
    arma::Col<double>,
    arma::Col<double>::fixed<2>
>;

/// The type of container for the state vector, supporting range algebra
using harm_osc_state_types_vector_algebra = std::tuple<
    arma::Col<double>,                   // arma::vec is an alias of this
    arma::Col<double>::fixed<2>,
    arma::Row<double>
>;

/// Parameter gamma
const double gam = 0.15;

/// The rhs of x' = f(x) for the harmonic oscillator with specific state type
void harmonic_oscillator (const std::vector<double>& x,
                          std::vector<double>& dxdt,
                          [[maybe_unused]] const double t)
{
    dxdt[0] = x[1];
    dxdt[1] = -x[0] - gam*x[1];
}

template<class StateType>
StateType initial_state (double x0, double x1) {
    using namespace Utopia::Utils;

    auto state = StateType();

    if constexpr (not has_static_size<StateType>()) {
        state.resize(2);
    }
    state[0] = x0;
    state[1] = x1;

    return state;
}


/// The expected result at t1 = 10. and gam = 0.15
/** This was computed with Runge-Kutta Cash-Karp (5,4) and custom error checker
  * tolerances of e_rel=1e-20 and e_abs=1e-20.
  */
const auto harm_osc_result_t10 = std::array<double, 2>{
    {-0.42190945039184452, 0.24640789042036049}
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_examples, Infrastructure)

/// Test the boost odeint harmonic oscillator example with simple integration
BOOST_AUTO_TEST_CASE (harmonic_oscillator_simple)
{
    using StateType = std::vector<double>;

    const auto t0 = 0.;
    const auto t1 = 10.;
    const auto dt = .1;

    // state initialization: start at x=1.0, p=0.0
    StateType state1(2);
    state1[0] = 1.0;
    state1[1] = 0.0;

    // integration via function
    auto steps1 = integrate(harmonic_oscillator, state1, t0, t1, dt);

    // same again but with a lambda
    const auto gamma = 0.15;
    StateType state2(2);
    state2[0] = 1.0;
    state2[1] = 0.0;

    auto steps2 = integrate(
        [gamma](const auto& x, auto& dxdt, double){
            dxdt[0] = x[1];
            dxdt[1] = -x[0] - gamma * x[1];
        },
        state2, t0, t1, dt
    );

    BOOST_TEST(steps1 > 0);
    BOOST_TEST(steps1 == steps2);

    BOOST_TEST(state1[0] == harm_osc_result_t10[0], tt::tolerance(1e-4));
    BOOST_TEST(state1[1] == harm_osc_result_t10[1], tt::tolerance(1e-4));

    BOOST_TEST(state1 == state2, tt::per_element());
}


/// Test the boost odeint with lambda with templated state types
BOOST_AUTO_TEST_CASE_TEMPLATE (harmonic_oscillator_templated_types,
                               StateType, harm_osc_state_types)
{
    // Define parameters and initial state
    const auto t0 = 0.;
    const auto t1 = 10.;
    const auto dt = .01;

    const auto gamma = 0.15;

    auto state = initial_state<StateType>(1., 0.);
    BOOST_TEST_PASSPOINT();

    auto steps = integrate(
        [gamma](const auto& x, auto& dxdt, double){
            dxdt[0] = x[1];
            dxdt[1] = -x[0] - gamma * x[1];
        },
        state, t0, t1, dt
    );

    BOOST_TEST(steps > 0);
    BOOST_TEST(state[0] == harm_osc_result_t10[0], tt::tolerance(1e-4));
    BOOST_TEST(state[1] == harm_osc_result_t10[1], tt::tolerance(1e-4));
}


/// Test the boost odeint harmonic oscillator example with controlled stepper
BOOST_AUTO_TEST_CASE_TEMPLATE (harmonic_oscillator_controlled_stepper,
                               StateType, harm_osc_state_types)
{
    const auto t0 = 0.;
    const auto t1 = 10.;
    const auto dt = .01;  // initial value

    const auto gamma = 0.15;

    auto state = initial_state<StateType>(1., 0.);

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    // Define error stepper type, needed for controlled stepper
    using error_stepper_type = runge_kutta_cash_karp54<StateType>;
    using controlled_stepper_type = controlled_runge_kutta<error_stepper_type>;

    // ... and initialize stepper
    auto controlled_stepper = controlled_stepper_type();

    auto steps = integrate_adaptive(controlled_stepper, system,
                                    state, t0, t1, dt);

    BOOST_TEST(steps > 0);
    BOOST_TEST(state[0] == harm_osc_result_t10[0], tt::tolerance(1e-4));
    BOOST_TEST(state[1] == harm_osc_result_t10[1], tt::tolerance(1e-4));
}

BOOST_AUTO_TEST_CASE_TEMPLATE (harmonic_oscillator_custom_tolerance,
                               StateType, harm_osc_state_types_range_algebra)
{
    const auto t0 = 0.;
    const auto t1 = 10.;
    const auto dt = .01;  // initial value

    const auto gamma = 0.15;

    auto state = initial_state<StateType>(1., 0.);

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    // Define error stepper type, needed for controlled stepper
    using error_stepper_type = runge_kutta_cash_karp54<StateType>;
    using controlled_stepper_type = controlled_runge_kutta<error_stepper_type>;

    // ... and initialize stepper
    double abs_err = 1.0e-20;
    double rel_err = 1.0e-20;
    double a_x = 1.0;
    double a_dxdt = 1.0;

    auto controlled_stepper = controlled_stepper_type(
        default_error_checker<double, range_algebra, default_operations>(
            abs_err, rel_err, a_x, a_dxdt
        )
    );
    auto steps = integrate_adaptive(controlled_stepper, system,
                                    state, t0, t1, dt);

    BOOST_TEST(steps > 10000);
    BOOST_TEST(state[0] == harm_osc_result_t10[0], tt::tolerance(1e-10));
    BOOST_TEST(state[1] == harm_osc_result_t10[1], tt::tolerance(1e-10));
}


/// Tests integration result if invoking adaptive integration multiple times
BOOST_AUTO_TEST_CASE (multi_invocation)
{
    using StateType = std::vector<double>;
    using System = std::function<void(const StateType&, StateType&, double)>;

    using error_stepper_type = runge_kutta_cash_karp54<StateType>;
    using controlled_stepper_type = controlled_runge_kutta<error_stepper_type>;

    const auto dt = .01;
    auto controlled_stepper = controlled_stepper_type();

    const auto gamma = 0.15;
    System system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    auto state = StateType{{1., 0.}};

    for (double t0=0.; t0 < 10.; t0++) {
        integrate_adaptive(controlled_stepper, system,
                           state, t0, t0+1., dt);
    }

    BOOST_TEST(state[0] == harm_osc_result_t10[0], tt::tolerance(1e-4));
    BOOST_TEST(state[1] == harm_osc_result_t10[1], tt::tolerance(1e-4));
}



BOOST_AUTO_TEST_SUITE_END() // "test_examples"


// -- ODESystem ---------------------------------------------------------------


template<class State = std::vector<double>>
struct HarmonicOscillatorSystem : public BaseODESystem<State> {
    const double gamma;

    HarmonicOscillatorSystem (State initial_state, double gamma)
    :
        BaseODESystem<State>(initial_state)
    ,   gamma(gamma)
    {}

    void operator() (const State& x, State& dxdt, double) {
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    }
};


BOOST_FIXTURE_TEST_SUITE (test_ODESystem, Infrastructure)

BOOST_AUTO_TEST_CASE (interface) {
    auto sys = HarmonicOscillatorSystem({1., 0.}, 0.15);

    BOOST_TEST(sys.gamma == 0.15);
    BOOST_TEST(sys.get_state()[0] == 1.);
    BOOST_TEST(sys.get_state()[1] == 0.);
}

BOOST_AUTO_TEST_SUITE_END() // "test_ODESystem"




// -- ODEIntegrator -----------------------------------------------------------


// .. Additional code for vector_space_algebra ................................
namespace boost::numeric::odeint {

template<>
struct vector_space_norm_inf<arma::vec> {
    using result_type = double;

    double operator() (const arma::vec& p) const {
        return arma::norm(p, "inf");
    }
};

template<>
struct vector_space_norm_inf<arma::vec::fixed<2>> {
    using result_type = double;

    double operator() (const arma::vec& p) const {
        return arma::norm(p, "inf");
    }
};

template<>
struct vector_space_norm_inf<arma::rowvec> {
    using result_type = double;

    double operator() (const arma::rowvec& p) const {
        return arma::norm(p, "inf");
    }
};


// Need to provide resizing adaptors . . . . . . . . . . . . . . . . . . . . .
// ... for the vector types that don't have a fixed size
// See  https://stackoverflow.com/a/41820991/1827608  for more information
// TODO Reduce overloads by formulating it with enable_if and type traits ...

// ... for column vector
template<>
struct is_resizeable<arma::vec> {
    using type = boost::true_type;
    const static bool value = type::value;
};

template<>
struct same_size_impl<arma::vec, arma::vec> {
    static bool same_size (const arma::vec& x, const arma::vec& y) {
        return x.size() == y.size();
    }
};

template<>
struct resize_impl<arma::vec, arma::vec> {
    static void resize (arma::vec& v1, const arma::vec& v2) {
        v1.resize(v2.size());
    }
};


// ... for row vector
template<>
struct is_resizeable<arma::rowvec> {
    using type = boost::true_type;
    const static bool value = type::value;
};

template<>
struct same_size_impl<arma::rowvec, arma::rowvec> {
    static bool same_size (const arma::rowvec& x, const arma::rowvec& y) {
        return x.size() == y.size();
    }
};

template<>
struct resize_impl<arma::rowvec, arma::rowvec> {
    static void resize (arma::rowvec& v1, const arma::rowvec& v2) {
        v1.resize(v2.size());
    }
};

}


// ............................................................................

BOOST_FIXTURE_TEST_SUITE (test_ODEIntegrator, Infrastructure)

// TODO Test with different controlled stepper and error stepper types

/// Test construction and basic integration
BOOST_AUTO_TEST_CASE_TEMPLATE (basics,
                               StateType, harm_osc_state_types)
{
    const auto gamma = 0.15;

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    const auto state0 = initial_state<StateType>(1., 0.);

    auto integrator = ODEIntegrator(make_ODESystem(state0, system));

    BOOST_TEST(integrator.get_time() == 0.);
    BOOST_TEST(integrator.get_state() == state0, tt::per_element());

    // Integrate once, advancing internal time
    auto steps = integrator.integrate(4.);
    BOOST_TEST(integrator.get_time() == 4.);
    BOOST_TEST(steps == 19);

    // ... and again, now with constant dt
    steps += integrator.integrate_const(1.);
    BOOST_TEST(integrator.get_time() == 4. + 1.);
    BOOST_TEST(steps == 19 + int(1./integrator.get_default_dt()));

    // ... and again, using the adaptive stepper
    steps += integrator.integrate_adaptive(5.);
    BOOST_TEST(integrator.get_time() == 4. + 1. + 5.);
    BOOST_TEST(steps == 1036);

    // State is correct
    BOOST_TEST(integrator.get_state()[0] == harm_osc_result_t10[0],
               tt::tolerance(1e-4));
    BOOST_TEST(integrator.get_state()[1] == harm_osc_result_t10[1],
               tt::tolerance(1e-4));
}


/// Test construction and basic integration
BOOST_AUTO_TEST_CASE_TEMPLATE (derived,
                               StateType, harm_osc_state_types)
{
    const auto gamma = 0.15;
    const auto state0 = initial_state<StateType>(1., 0.);

    auto integrator = ODEIntegrator(HarmonicOscillatorSystem(state0, gamma));

    BOOST_TEST(integrator.get_time() == 0.);
    BOOST_TEST(integrator.get_state() == state0, tt::per_element());

    // Integrate once, advancing internal time
    auto steps = integrator.integrate(4.);
    BOOST_TEST(integrator.get_time() == 4.);
    BOOST_TEST(steps == 19);

    // ... and again, now with constant dt
    steps += integrator.integrate_const(1.);
    BOOST_TEST(integrator.get_time() == 4. + 1.);
    BOOST_TEST(steps == 19 + int(1./integrator.get_default_dt()));

    // ... and again, using the adaptive stepper
    steps += integrator.integrate_adaptive(5.);
    BOOST_TEST(integrator.get_time() == 4. + 1. + 5.);
    BOOST_TEST(steps == 1036);

    // State is correct
    BOOST_TEST(integrator.get_state()[0] == harm_osc_result_t10[0],
               tt::tolerance(1e-4));
    BOOST_TEST(integrator.get_state()[1] == harm_osc_result_t10[1],
               tt::tolerance(1e-4));
}


/// Test construction from config
BOOST_AUTO_TEST_CASE_TEMPLATE (cfg_construction,
                               StateType, harm_osc_state_types)
{
    const auto gamma = 0.15;

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    const auto state0 = initial_state<StateType>(1., 0.);

    auto integrator = ODEIntegrator(make_ODESystem(state0, system),
                                    cfg["from_cfg"]);

    BOOST_TEST(integrator.get_time() == 0.);
    BOOST_TEST(   integrator.get_default_dt()
               == get_as<double>("default_dt", cfg["from_cfg"]));

    // Integrate once, advancing internal time
    auto steps = integrator.integrate(4.);
    BOOST_TEST(integrator.get_time() == 4.);
    BOOST_TEST(steps == 18);

    // ... and again, now with constant dt
    steps += integrator.integrate_const(1.);
    BOOST_TEST(integrator.get_time() == 4. + 1.);
    BOOST_TEST(steps == 18 + int(1./integrator.get_default_dt()));

    // ... and again, using the adaptive stepper
    steps += integrator.integrate_adaptive(5.);
    BOOST_TEST(integrator.get_time() == 4. + 1. + 5.);
    BOOST_TEST(steps == 234);

    // State is correct
    BOOST_TEST(integrator.get_state()[0] == harm_osc_result_t10[0],
               tt::tolerance(1e-4));
    BOOST_TEST(integrator.get_state()[1] == harm_osc_result_t10[1],
               tt::tolerance(1e-4));
}


/// Test working with vector algebra
BOOST_AUTO_TEST_CASE_TEMPLATE (test_vector_algebra,
                               StateType, harm_osc_state_types_vector_algebra)
{
    const auto gamma = 0.15;

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    const auto state0 = initial_state<StateType>(1., 0.);
    BOOST_TEST_CHECKPOINT("Initial state set up.");

    // Set up the stepper with vector space algebra
    using Stepper =
        runge_kutta_dopri5<StateType, double,
                           StateType, double, vector_space_algebra>;

    auto integrator = ODEIntegrator(make_ODESystem(state0, system),
                                    make_controlled<Stepper>(1e-10, 1e-10));
    BOOST_TEST_CHECKPOINT("Integrator initialized.");

    BOOST_TEST(integrator.get_time() == 0.);
    BOOST_TEST(integrator.get_state() == state0, tt::per_element());

    // Integrate once, advancing internal time
    auto steps = integrator.integrate(4.);
    BOOST_TEST(integrator.get_time() == 4.);
    BOOST_TEST(steps == 19);

    // ... and again, now with constant dt
    steps += integrator.integrate_const(1.);
    BOOST_TEST(integrator.get_time() == 4. + 1.);
    BOOST_TEST(steps == 19 + int(1./integrator.get_default_dt()));

    // ... and again, using the adaptive stepper
    steps += integrator.integrate_adaptive(5.);
    BOOST_TEST(integrator.get_time() == 4. + 1. + 5.);
    BOOST_TEST(steps == 1130);

    // State is correct
    BOOST_TEST(integrator.get_state()[0] == harm_osc_result_t10[0],
               tt::tolerance(1e-4));
    BOOST_TEST(integrator.get_state()[1] == harm_osc_result_t10[1],
               tt::tolerance(1e-4));
}


BOOST_AUTO_TEST_CASE_TEMPLATE (benchmark_algebras,
                               StateType, harm_osc_state_types_vector_algebra)
{
    // Prepare system
    const auto gamma = 0.15;

    auto system = [gamma](const auto& x, auto& dxdt, double){
        dxdt[0] = x[1];
        dxdt[1] = -x[0] - gamma * x[1];
    };

    const auto state0 = initial_state<StateType>(1., 0.);


    // Define benchmarking functions with different steppers
    auto default_algebra = [&](){
        auto integrator = ODEIntegrator(
            make_ODESystem(state0, system),
            cfg["from_cfg"]
        );

        auto start = Clock::now();

        auto steps = integrator.integrate(4.);
        steps += integrator.integrate_const(1.);
        steps += integrator.integrate_adaptive(5.);

        return time_since(start);
    };

    auto with_vector_space_algebra = [&](){
        using Stepper =
            runge_kutta_dopri5<StateType, double,
                               StateType, double, vector_space_algebra>;

        auto integrator = ODEIntegrator(
            make_ODESystem(state0, system),
            make_controlled<Stepper>(1e-10, 1e-10)
        );

        auto start = Clock::now();

        auto steps = integrator.integrate(4.);
        steps += integrator.integrate_const(1.);
        steps += integrator.integrate_adaptive(5.);

        return time_since(start);
    };

    auto with_stdvector = [&](){
        const auto state0 = initial_state<std::vector<double>>(1., 0.);
        auto integrator = ODEIntegrator(
            make_ODESystem(state0, system),
            cfg["from_cfg"]
        );

        auto start = Clock::now();

        auto steps = integrator.integrate(4.);
        steps += integrator.integrate_const(1.);
        steps += integrator.integrate_adaptive(5.);

        return time_since(start);
    };


    // -- Start benchmark
    auto time_da = 0.;
    auto time_vsa = 0.;
    auto time_stdvec = 0.;

    const auto num_samples = get_as<int>("num_samples", cfg["benchmark"]);
    log->info("Benchmarks commencing (with {} samples each) ...\n",
              num_samples);

    {
    auto bf = default_algebra;  // benchmark function
    std::string bf_name = "default_algebra";

    auto [mean, stddev, total_time, times] = perform_benchmark(
        bf_name,
        num_samples,
        log,
        bf
    );
    time_da = mean;
    }

    {
    auto bf = with_vector_space_algebra;
    std::string bf_name = "with_vector_space_algebra";

    auto [mean, stddev, total_time, times] = perform_benchmark(
        bf_name,
        num_samples,
        log,
        bf
    );
    time_vsa = mean;
    }

    {
    auto bf = with_stdvector;
    std::string bf_name = "with_stdvector";

    auto [mean, stddev, total_time, times] = perform_benchmark(
        bf_name,
        num_samples,
        log,
        bf
    );
    time_stdvec = mean;
    }

    // Default algebra is much faster than vector space algebra for same types
    BOOST_TEST(time_da < 0.01);
    BOOST_TEST(time_vsa < 0.01);
    BOOST_TEST(time_stdvec < 0.01);
    BOOST_TEST(time_da < time_vsa);
    BOOST_TEST(time_da < time_vsa / 3);

    // Even std::vector with default algebra is slightly faster than vector
    // space algebra of any armadillo type
    BOOST_TEST(time_stdvec < time_vsa / 1.2);
}



BOOST_AUTO_TEST_SUITE_END() // "test_ODEIntegrator"
