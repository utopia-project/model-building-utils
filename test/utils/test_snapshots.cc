#define BOOST_TEST_MODULE test snapshots

#include <string>
#include <tuple>
#include <set>

#include <armadillo>

#include <UtopiaUtils/utils/snapshots.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

using Utopia::get_as;

struct SomeTestType {
    std::size_t _size = 0;
    int _some_value = 0;

    auto size () const {
        return _size;
    }
};

/// Some container types for testing snapshotting, all default-constructible
using test_types = std::tuple<
    SomeTestType,
    std::vector<int>,
    arma::uvec,
    arma::vec
>;



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/// Tests construction of the MultiSnapshotManager with different types
BOOST_AUTO_TEST_CASE_TEMPLATE (test_multi_snapshot_construction,
                               SnapType, test_types)
{
    auto snaps = MultiSnapshotManager<SnapType, std::size_t>(
        [](const auto& s){ return s.size(); }, 1, 4
    );
    BOOST_TEST(snaps.size() == 0);
    BOOST_TEST(snaps.threshold() == 1);
    BOOST_TEST(snaps.max_size() == 4);

    // Add some elements that are below the minimum index value
    BOOST_TEST(not snaps.add({}));
    BOOST_TEST(not snaps.add({}));
    BOOST_TEST(snaps.size() == 0);

    // ... and some that are above, which get actually added
    BOOST_TEST(snaps.add({1}));
    BOOST_TEST(snaps.add({2}));
    BOOST_TEST(snaps.add({3}));
    BOOST_TEST(snaps.size() == 3);

    // These ones get added as well, but snapshots size stops growing
    BOOST_TEST(snaps.add({4}));
    BOOST_TEST(snaps.size() == 4);
    BOOST_TEST(snaps.add({5}));
    BOOST_TEST(snaps.size() == 4);
    BOOST_TEST(snaps.add({6}));
    BOOST_TEST(snaps.size() == 4);
}


/// Test MultiSnapshot life cycle
BOOST_AUTO_TEST_CASE (test_multi_snapshot_life_cycle) {
    auto snaps = MultiSnapshotManager<SomeTestType, int>(
        [](const auto& s){ return s._some_value; }, 42, 3
    );

    BOOST_TEST(snaps.size() == 0);
    BOOST_TEST(snaps.threshold() == 42);
    BOOST_TEST(snaps.max_size() == 3);

    // Only elements >= 42 are added
    BOOST_TEST(not snaps.add({0, 41}));
    BOOST_TEST(snaps.add({1, 43}));
    BOOST_TEST(snaps.add({2, 42}));
    BOOST_TEST(snaps.add({3, 44}));
    BOOST_TEST(snaps.size() == 3);

    BOOST_TEST(snaps.front().first == 42);
    BOOST_TEST(snaps.back().first == 44);
    BOOST_TEST(snaps.front().second._some_value == 42);
    BOOST_TEST(snaps.back().second._some_value == 44);

    BOOST_TEST(snaps.front_index() == 42);
    BOOST_TEST(snaps.back_index() == 44);

    BOOST_TEST(snaps.front_snapshot()._some_value == 42);
    BOOST_TEST(snaps.back_snapshot()._some_value == 44);

    // Adding more elements leads to earlier added ones with the same index
    // being removed
    BOOST_TEST(snaps.add({123, 42}));
    BOOST_TEST(snaps.size() == 3);
    BOOST_TEST(snaps.front_index() == 42);
    BOOST_TEST(snaps.front_snapshot()._size == 123);

    BOOST_TEST(snaps.add({234, 43}));
    BOOST_TEST(snaps.size() == 3);
    BOOST_TEST(snaps.front_index() == 43);
    BOOST_TEST(snaps.front_snapshot()._size == 1);  // from before

    // Can const-iterate over the snapshots
    int last_idx = 0;
    for (const auto& [idx, snap] : snaps.snapshots()) {
        BOOST_TEST(snap._some_value == idx);
        BOOST_TEST(idx >= last_idx);
        last_idx = idx;
    }
    BOOST_TEST(last_idx == 44);
}
