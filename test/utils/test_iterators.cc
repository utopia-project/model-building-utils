#define BOOST_TEST_MODULE test iterators

#include <string>

#include <UtopiaUtils/utils/iterators.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;


/// Calls in_range and compares the collection with an expected collection
template<class T=int, class... Args>
void test_range(const std::vector<T> expected, Args&&... args) {
    auto vals = std::vector<int>{};
    for (auto i : in_range(std::forward<Args>(args)...)) {
        vals.push_back(i);
    }
    BOOST_TEST(vals == expected, tt::per_element());
}


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests the in_range function, a wrapper around boost irange iterators
BOOST_AUTO_TEST_CASE (test_in_range)
{
    //          expected           in_range input parameters
    test_range({0, 1, 2, 3, 4},    5);
    BOOST_TEST_PASSPOINT();

    test_range({5, 6, 7, 8, 9},    5, 10);
    BOOST_TEST_PASSPOINT();

    test_range({5, 7, 9, 11, 13},  5, 15, 2);
    BOOST_TEST_PASSPOINT();

    // NOTE Do not need to test other irange properties, e.g. first <= last,
    //      because irange already does that.
}
