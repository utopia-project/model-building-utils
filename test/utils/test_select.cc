#define BOOST_TEST_MODULE test select

#include <vector>

#include <UtopiaUtils/utils/select.hh>
#include <UtopiaUtils/utils/iterators.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    const int N = 100000;
    std::vector<int> elements;

    Infrastructure ()
    :   BaseInfrastructure<>("select.yml")
    ,   elements{}
    {
        // Populate the elements container
        for (auto i : in_range(N)) {
            elements.push_back(i);
        }
    }
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_select, Infrastructure)

/// Test selection via the config interface
BOOST_AUTO_TEST_CASE (config_interface)
{
    // TODO
}

/// Tests selection mode "all"
BOOST_AUTO_TEST_CASE (all)
{
    auto selected = select(elements, 3, "all", rng); // NOTE: num ignored
    BOOST_TEST(selected.size() == N);
    BOOST_TEST(selected == elements, tt::per_element());

    selected = select(elements, -1, "all", rng);
    BOOST_TEST(selected.size() == N);
    BOOST_TEST(selected == elements, tt::per_element());
}

/// Tests selection mode "first_n"
BOOST_AUTO_TEST_CASE (first_n)
{
    auto selected = select(elements, 3, "first_n", rng);
    BOOST_TEST(selected.size() == 3);
    BOOST_TEST(selected == std::vector<int>({0, 1, 2}), tt::per_element());

    selected = select(elements, 0, "first_n", rng);
    BOOST_TEST(selected.size() == 0);

    selected = select(elements, -1, "first_n", rng);
    BOOST_TEST(selected.size() == N);
}

/// Tests selection mode "last_n"
BOOST_AUTO_TEST_CASE (last_n)
{
    auto selected = select(elements, 3, "last_n", rng);
    BOOST_TEST(selected.size() == 3);
    BOOST_TEST(selected == std::vector<int>({N-1, N-2, N-3}),
               tt::per_element());

    selected = select(elements, 0, "last_n", rng);
    BOOST_TEST(selected.size() == 0);

    selected = select(elements, -1, "last_n", rng);
    BOOST_TEST(selected.size() == N);
}

/// Tests selection mode "slics"
BOOST_AUTO_TEST_CASE (slice)
{
    Config params = {}; // use defaults for now

    auto selected = select(elements, 3, "slice", rng, params);
    BOOST_TEST(selected.size() == 3);
    BOOST_TEST(selected == std::vector<int>({0, 1, 2}), tt::per_element());

    selected = select(elements, 0, "slice", rng, params);
    BOOST_TEST(selected.size() == 0);

    selected = select(elements, -1, "slice", rng, params);
    BOOST_TEST(selected.size() == N);

    params["start"] = 2;
    selected = select(elements, 3, "slice", rng, params);
    BOOST_TEST(selected == std::vector<int>({2, 3, 4}), tt::per_element());

    params["stop"] = 4;  // not included
    selected = select(elements, 3, "slice", rng, params);
    BOOST_TEST(selected == std::vector<int>({2, 3}), tt::per_element());

    params["step"] = 2;
    selected = select(elements, 3, "slice", rng, params);
    BOOST_TEST(selected == std::vector<int>({2}), tt::per_element());

    params["start"] = 1;
    params["stop"] = 10;  // not included
    params["step"] = 3;
    selected = select(elements, 10, "slice", rng, params);
    BOOST_TEST(selected == std::vector<int>({1, 4, 7}), tt::per_element());

    // Can also have negative arguments
    params["start"] = -7;
    params["stop"] = -2;
    params["step"] = 2;
    selected = select(elements, 123, "slice", rng, params);
    BOOST_TEST(selected.size() == 3);
    BOOST_TEST(selected == std::vector<int>({N-7, N-5, N-3}),
               tt::per_element());

    params["stop"] = 1;  // stop before start -> empty selection
    selected = select(elements, 123, "slice", rng, params);
    BOOST_TEST(selected.size() == 0);

    // Check exceptions
    check_exception<std::invalid_argument>(
        [&](){
            params["step"] = -1;
            select(elements, 10, "slice", rng, params);
        },
        "needs step >= 1, but got -1",
        {__LINE__, __FILE__}
    );
}

/// Tests selection mode "random"
BOOST_AUTO_TEST_CASE (random)
{
    // Select some from them
    auto selected = select(elements, 3, "random", rng);
    BOOST_TEST(selected.size() == 3);

    // Also works directly with an iterator range
    auto selected2 = select(in_range(N), 3, "random", rng);
    BOOST_TEST(selected2.size() == 3);

    // ... these should be different; very likely at least
    BOOST_TEST(selected != selected2, tt::per_element());

    // When trying to sample more than available, fails
    check_exception<std::invalid_argument>(
        [&](){
            select(elements, N + 1, "random", rng);
        },
        "Invalid entity number 100001 specified for selection from pool of "
        "100000 available entities! 100001 is out of range [0, 100000]!",
        {__LINE__, __FILE__}
    );

    // Bad selection mode
    check_exception<std::invalid_argument>(
        [&](){
            select(elements, 1, "foo", rng);
        },
        "Invalid selection mode: foo",
        {__LINE__, __FILE__}
    );
}

BOOST_AUTO_TEST_CASE (do_nothing) {

}

BOOST_AUTO_TEST_SUITE_END() // "test_select"
