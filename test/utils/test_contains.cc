#define BOOST_TEST_MODULE test contains

#include <list>
#include <map>
#include <tuple>
#include <vector>
#include <deque>

#include <UtopiaUtils/utils/contains.hh>

#include "../testtools.hh"

// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// Some container types
using cont_types =
    std::tuple<std::vector<int>, std::list<int>, std::deque<int>>;

/// Some map types
using map_types = std::tuple<std::map<std::string, int>,
                             std::unordered_map<std::string, int>>;

// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests contains
BOOST_AUTO_TEST_CASE_TEMPLATE(test_contains, ContType, cont_types) {
    // Set up some container with entities
    const auto cont = ContType{1, 2, 4, 8, 16, 32};

    BOOST_TEST(contains(cont, 1));
    BOOST_TEST(contains(cont, 2));
    BOOST_TEST(contains(cont, 4));
    BOOST_TEST(contains(cont, 8));
    BOOST_TEST(contains(cont, 16));
    BOOST_TEST(contains(cont, 32));
    BOOST_TEST(not contains(cont, 0));
    BOOST_TEST(not contains(cont, 1.5));
}

/// Tests contains_key
BOOST_AUTO_TEST_CASE_TEMPLATE(test_contains_key, MapType, map_types) {
    // Set up some container with entities
    const auto map = MapType{{"1", 1}, {"2", 2},   {"4", 4},
                             {"8", 8}, {"16", 16}, {"32", 32}};

    BOOST_TEST(contains_key(map, "1"));
    BOOST_TEST(contains_key(map, "2"));
    BOOST_TEST(contains_key(map, "4"));
    BOOST_TEST(contains_key(map, "8"));
    BOOST_TEST(contains_key(map, "16"));
    BOOST_TEST(contains_key(map, "32"));
    BOOST_TEST(not contains_key(map, "foo"));
    BOOST_TEST(not contains_key(map, "bar"));
}
