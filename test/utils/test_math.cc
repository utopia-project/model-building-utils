#define BOOST_TEST_MODULE test utils

#include <cmath>
#include <vector>
#include <tuple>
#include <limits>
#include <sstream>
#include <numeric>
#include <functional>

#include <UtopiaUtils/utils/math.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::Utils::tags;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>() {}
};


// -- Definitions -------------------------------------------------------------

/// Some signed and unsigned integer types
using int_types = std::tuple<int, long long, uint, std::size_t>;

/// Some armadillo vector types to test
using vec_types = std::tuple<arma::vec::fixed<5>, arma::vec, arma::fvec>;

/// A helper method to initialize a vector, regardless of being fixed or not
template<class Vec, class elem_type = typename Vec::elem_type>
Vec get_vec (elem_type fill_value = 0., std::size_t size = 5) {
    Vec v;
    v.set_size(size);
    v.fill(fill_value);
    return v;
}


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BOOST_FIXTURE_TEST_SUITE (test_math, Infrastructure)

/// Test the map_index
BOOST_AUTO_TEST_CASE_TEMPLATE (test_map_into_range, Int, int_types)
{
    BOOST_TEST(map_into_range<index_like>(0, 10) == 0);
    BOOST_TEST(map_into_range<size_like>(0, 10) == 0);

    BOOST_TEST(map_into_range<index_like>(3, 10) == 3);
    BOOST_TEST(map_into_range<size_like>(3, 10) == 3);

    BOOST_TEST(map_into_range<index_like>(-1, 10) == 9);
    BOOST_TEST(map_into_range<size_like>(-1, 10) == 10);

    BOOST_TEST(map_into_range<index_like>(-10, 10) == 0);
    BOOST_TEST(map_into_range<size_like>(-11, 10) == 0);

    check_exception<std::invalid_argument>([&](){
        map_into_range<index_like>(-11, 10);
    }, "Cannot map -11 into range [0, 10)", {__LINE__, __FILE__});

    check_exception<std::invalid_argument>([&](){
        map_into_range<size_like>(-12, 10);
    }, "Cannot map -12 into range [0, 10]", {__LINE__, __FILE__});

    check_exception<std::invalid_argument>([&](){
        map_into_range<index_like>(10, 10);
    }, "10 is out of range [0, 10)", {__LINE__, __FILE__});

    check_exception<std::invalid_argument>([&](){
        map_into_range<size_like>(11, 10);
    }, "11 is out of range [0, 10]", {__LINE__, __FILE__});
}

/// Test the smallest_quotient function
BOOST_AUTO_TEST_CASE_TEMPLATE (test_smallest_quotient, Vec, vec_types)
{
    const auto NaN = std::numeric_limits<typename Vec::elem_type>::quiet_NaN();
    const auto INF = std::numeric_limits<typename Vec::elem_type>::infinity();

    // Construct some vectors
    auto zeros = get_vec<Vec>();
    auto ones = get_vec<Vec>(1.);
    auto all_NaN = get_vec<Vec>(NaN);
    auto all_INF = get_vec<Vec>(INF);

    auto nonzero = get_vec<Vec>();
    nonzero[0] = 0.125;
    nonzero[1] = 0.25;
    nonzero[2] = 0.5;
    nonzero[3] = 1.;
    nonzero[4] = 2.;

    auto with_zero = get_vec<Vec>();
    with_zero[0] = 0.;
    with_zero[1] = 0.25;
    with_zero[2] = 0.5;
    with_zero[3] = 1.;
    with_zero[4] = 2.;

    auto with_zero2 = get_vec<Vec>();
    with_zero2[0] = 1.;
    with_zero2[1] = 0.25;
    with_zero2[2] = 0.;
    with_zero2[3] = 1.;
    with_zero2[4] = 2.;

    auto with_NaN = get_vec<Vec>();
    with_NaN[0] = 1.;
    with_NaN[1] = NaN;
    with_NaN[2] = 0.5;
    with_NaN[3] = NaN;
    with_NaN[4] = 2.;

    auto with_INF = get_vec<Vec>();
    with_INF[0] = 1.;
    with_INF[1] = 0.5;
    with_INF[2] = INF;
    with_INF[3] = 2.;
    with_INF[4] = INF;

    // Assert some armadillo behaviour
    BOOST_TEST(all_NaN.min() == INF);
    BOOST_TEST(all_INF.min() == INF);
    BOOST_TEST((-1 * all_INF).min() == -INF);


    // The totally nice cases
    BOOST_TEST(smallest_quotient(ones, ones)                    == 1);
    BOOST_TEST(smallest_quotient(with_zero, ones)               == 0);
    BOOST_TEST(smallest_quotient(zeros, ones)                   == 0);

    BOOST_TEST(smallest_quotient(ones, nonzero)                 == 0.5);
    BOOST_TEST(smallest_quotient(with_zero, nonzero)            == 0);
    BOOST_TEST(smallest_quotient(zeros, nonzero)                == 0);

    BOOST_TEST(smallest_quotient(ones, .5 * ones)               == 2.);
    BOOST_TEST(smallest_quotient(ones, -.5 * ones)              == -2.);

    // Both with zeros, but in different positions
    BOOST_TEST(smallest_quotient(with_zero, with_zero2)         == 0);
    BOOST_TEST(smallest_quotient(with_zero2, with_zero)         == 0);

    // NaNs are ignored
    BOOST_TEST(smallest_quotient(ones, with_NaN)                == 0.5);
    BOOST_TEST(smallest_quotient(with_zero, with_NaN)           == 0);
    BOOST_TEST(smallest_quotient(zeros, with_NaN)               == 0);
    BOOST_TEST(smallest_quotient(nonzero, with_NaN)             == 0.125);

    BOOST_TEST(smallest_quotient(with_NaN, ones)                == 0.5);
    BOOST_TEST(smallest_quotient(with_NaN, with_zero)           == 1.);
    BOOST_TEST(smallest_quotient(with_NaN, zeros)               == INF);
    BOOST_TEST(smallest_quotient(with_NaN, nonzero)             == 1.);


    // The division-by-zero cases; should lead to +/- inf as smallest quotient
    BOOST_TEST(smallest_quotient(ones, zeros)                   == INF);
    BOOST_TEST(smallest_quotient(nonzero, zeros)                == INF);
    BOOST_TEST(smallest_quotient(with_zero, zeros)              == INF);

    BOOST_TEST(smallest_quotient(-1 * ones, zeros)              == -INF);
    BOOST_TEST(smallest_quotient(-1 * nonzero, zeros)           == -INF);
    BOOST_TEST(smallest_quotient(-1 * with_zero, zeros)         == -INF);

    BOOST_TEST(smallest_quotient(ones, -1 * zeros)              == -INF);
    BOOST_TEST(smallest_quotient(nonzero, -1 * zeros)           == -INF);
    BOOST_TEST(smallest_quotient(with_zero, -1 * zeros)         == -INF);

    BOOST_TEST(smallest_quotient(-1 * ones, -1 * zeros)         == INF);
    BOOST_TEST(smallest_quotient(-1 * nonzero, -1 * zeros)      == INF);
    BOOST_TEST(smallest_quotient(-1 * with_zero, -1 * zeros)    == INF);

    // 0/0 is always +inf, regardless of sign of zero
    BOOST_TEST(smallest_quotient(zeros, zeros)                  == INF);
    BOOST_TEST(smallest_quotient(-1 * zeros, zeros)             == INF);
    BOOST_TEST(smallest_quotient(zeros, -1 * zeros)             == INF);
    BOOST_TEST(smallest_quotient(-1 * zeros, -1 * zeros)        == INF);

    // When dividing by all-infinity vectors, the result is 0
    BOOST_TEST(smallest_quotient(ones, all_INF)                 == 0);
    BOOST_TEST(smallest_quotient(-1 * ones, all_INF)            == 0);
    BOOST_TEST(smallest_quotient(-1 * ones, -1 * all_INF)       == 0);
    BOOST_TEST(smallest_quotient(ones, -1 * all_INF)            == 0);

    BOOST_TEST(smallest_quotient(with_zero, all_INF)            == 0);
    BOOST_TEST(smallest_quotient(-1 * with_zero, all_INF)       == 0);
    BOOST_TEST(smallest_quotient(-1 * with_zero, -1 * all_INF)  == 0);
    BOOST_TEST(smallest_quotient(with_zero, -1 * all_INF)       == 0);

    // When the dividend includes negative infinity, that result is retained
    BOOST_TEST(smallest_quotient(all_INF, ones)                 == INF);
    BOOST_TEST(smallest_quotient(-1 * all_INF, ones)            == -INF);

    BOOST_TEST(smallest_quotient(with_INF, ones)                == 0.5);
    BOOST_TEST(smallest_quotient(-1 * with_INF, ones)           == -INF);
    BOOST_TEST(smallest_quotient(with_INF, -1 * ones)           == -INF);

    // When everything is NaN, the smallest quotient always returns INF,
    // because arma::min always returns INF
    BOOST_TEST(smallest_quotient(all_NaN, ones)                 == INF);
    BOOST_TEST(smallest_quotient(all_NaN, zeros)                == INF);
    BOOST_TEST(smallest_quotient(ones, all_NaN)                 == INF);
    BOOST_TEST(smallest_quotient(zeros, all_NaN)                == INF);
    BOOST_TEST(smallest_quotient(-1 * ones, all_NaN)            == INF);
    BOOST_TEST(smallest_quotient(-1 * zeros, all_NaN)           == INF);
    BOOST_TEST(smallest_quotient(all_NaN, -1 * ones)            == INF);
    BOOST_TEST(smallest_quotient(all_NaN, -1 * zeros)           == INF);
}

/// Test the clean_elements tag of the smallest_quotient function
BOOST_AUTO_TEST_CASE_TEMPLATE (test_smallest_quotient_clean, Vec, vec_types)
{
    const auto NaN = std::numeric_limits<typename Vec::elem_type>::quiet_NaN();
    const auto INF = std::numeric_limits<typename Vec::elem_type>::infinity();

    auto zeros = get_vec<Vec>();
    auto ones = get_vec<Vec>(1.);
    auto twos = get_vec<Vec>(2.);

    auto all_NaN = get_vec<Vec>(NaN);
    auto all_INF = get_vec<Vec>(INF);

    auto eps = get_vec<Vec>(PREC/10.);

    BOOST_TEST(smallest_quotient(eps, ones) > 0.);
    BOOST_TEST(smallest_quotient<clean_elements>(eps, ones) == 0.);

    // Test overload that allows specifying a precision
    BOOST_TEST(smallest_quotient<clean_elements>((ones-eps).eval(), twos, 1.)
               == 0.);

    // Can also clean with NaNs and INFs
    BOOST_TEST(smallest_quotient(all_NaN, ones) == INF);
    BOOST_TEST(smallest_quotient<clean_elements>(all_NaN, ones) == INF);

    BOOST_TEST(smallest_quotient(all_INF, ones) == INF);
    BOOST_TEST(smallest_quotient<clean_elements>(all_INF, ones) == INF);
}

/// Test the use_absolute tag of the smallest_quotient function
BOOST_AUTO_TEST_CASE_TEMPLATE (test_smallest_quotient_abs, Vec, vec_types)
{
    const auto NaN = std::numeric_limits<typename Vec::elem_type>::quiet_NaN();
    const auto INF = std::numeric_limits<typename Vec::elem_type>::infinity();

    auto zeros = get_vec<Vec>();
    auto ones = get_vec<Vec>(1.);
    auto twos = get_vec<Vec>(2.);

    auto all_NaN = get_vec<Vec>(NaN);
    auto all_INF = get_vec<Vec>(INF);

    auto with_INF = get_vec<Vec>();
    with_INF[0] = 1.;
    with_INF[1] = 0.5;
    with_INF[2] = INF;
    with_INF[3] = 2.;
    with_INF[4] = INF;

    auto mixed = get_vec<Vec>();
    mixed[0] = -1;
    mixed[1] = +2;
    mixed[2] = -3;
    mixed[3] = +4;
    mixed[4] = -5;

    // Simple cases
    BOOST_TEST(smallest_quotient(-1 * ones, twos) == -0.5);
    BOOST_TEST(smallest_quotient<use_absolute>(-1 * ones, twos) == +0.5);

    BOOST_TEST(smallest_quotient(mixed, ones) == -5);
    BOOST_TEST(smallest_quotient<use_absolute>(mixed, ones) == 1);

    BOOST_TEST(smallest_quotient(ones, mixed) == -1);
    BOOST_TEST((smallest_quotient<use_absolute>(ones, mixed) - 0.2) < 1e-8);
    // ... low precision value necessary due to float type

    // Result is positive even for -INF
    BOOST_TEST(smallest_quotient<use_absolute>(all_INF, ones) == INF);
    BOOST_TEST(smallest_quotient<use_absolute>(-1 * all_INF, ones) == INF);

    // -INF is no longer regarded as smallest quotient
    BOOST_TEST(smallest_quotient<use_absolute>(with_INF, ones) == 0.5);
    BOOST_TEST(smallest_quotient<use_absolute>(-1 * with_INF, ones) == 0.5);
    BOOST_TEST(smallest_quotient<use_absolute>(with_INF, -1 * ones) == 0.5);
}

BOOST_AUTO_TEST_SUITE_END()  // utils/math.hh test suite
