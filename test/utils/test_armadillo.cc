#define BOOST_TEST_MODULE test armadillo

#include <tuple>

#include <armadillo>

#include <UtopiaUtils/utils/armadillo.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"




// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("armadillo.yml") {}
};

using matrix_types = std::tuple<
    arma::mat,
    arma::Mat<float>,
    arma::umat
>;

using vector_types = std::tuple<
    arma::vec,
    arma::uvec,
    arma::rowvec
>;



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (imbue_operations, Infrastructure)

/// Test the imbue overload for vector-like types
BOOST_AUTO_TEST_CASE_TEMPLATE (test_imbue_index, Vector, vector_types)
{
    Vector vec(5, arma::fill::zeros);
    imbue_index(vec, [](auto idx){ return 2*idx; });
    BOOST_TEST(vec == Vector({0, 2, 4, 6, 8}), tt::per_element());
}

/// Test the imbue overload for matrix-like types
BOOST_AUTO_TEST_CASE_TEMPLATE (test_imbue_row_col, Matrix, matrix_types)
{
    Matrix mat(3, 3, arma::fill::zeros);
    imbue_row_col(mat, [](auto row, auto col){ return 10*row + col; });

    Matrix expected(3, 3, arma::fill::zeros);
    expected(0, 0) =  0;
    expected(1, 0) = 10;
    expected(2, 0) = 20;
    expected(0, 1) =  1;
    expected(1, 1) = 11;
    expected(2, 1) = 21;
    expected(0, 2) =  2;
    expected(1, 2) = 12;
    expected(2, 2) = 22;
    BOOST_TEST(mat == expected, tt::per_element());
}


BOOST_AUTO_TEST_SUITE_END() // "imbue_operations"



BOOST_FIXTURE_TEST_SUITE (resize_operations, Infrastructure)

/// Test appending a row or column to a vector
BOOST_AUTO_TEST_CASE_TEMPLATE (test_append_element, Vector, vector_types)
{
    Vector vec(3, arma::fill::zeros);
    append_element(vec, 1);
    BOOST_TEST(vec == Vector({0, 0, 0, 1}), tt::per_element());
    append_element(vec, 2);
    BOOST_TEST(vec == Vector({0, 0, 0, 1, 2}), tt::per_element());
    append_element(vec);
    BOOST_TEST(vec == Vector({0, 0, 0, 1, 2, 0}), tt::per_element());

    // Need not be initialized
    vec = Vector();
    BOOST_TEST(vec.size() == 0);
    append_element(vec, 1);
    BOOST_TEST(vec == Vector({1}), tt::per_element());
}


/// Test appending a row and column to a matrix
BOOST_AUTO_TEST_CASE_TEMPLATE (test_append_row_col, Matrix, matrix_types)
{
    using eT = typename Matrix::elem_type;
    using Vec = std::vector<eT>;

    Matrix mat(2, 2, arma::fill::ones);
    mat.print("(2, 2)");
    BOOST_TEST(mat.size() == 4);

    append_row_col(mat);
    mat.print("(3, 3)");
    BOOST_TEST(mat.size() == 9);
    BOOST_TEST(mat.col(2) == Vec({0, 0, 0}), tt::per_element());
    BOOST_TEST(mat.row(2) == Vec({0, 0, 0}), tt::per_element());

    append_row_col(mat, 2);
    mat.print("(4, 4)");
    BOOST_TEST(mat.size() == 16);
    BOOST_TEST(mat.col(3) == Vec({2, 2, 2, 2}), tt::per_element());
    BOOST_TEST(mat.row(3) == Vec({2, 2, 2, 2}), tt::per_element());

    // Need not be square
    mat = Matrix(2, 3, arma::fill::ones);
    append_row_col(mat, 1);
    BOOST_TEST(mat.size() == 3*4);
    BOOST_TEST(arma::accu(mat) == 3*4);

    // Need not be initialized
    mat = Matrix();
    BOOST_TEST(mat.size() == 0);
    append_row_col(mat, 1);
    BOOST_TEST(mat.size() == 1);
    BOOST_TEST(mat == Matrix(1, 1, arma::fill::ones), tt::per_element());
}

BOOST_AUTO_TEST_CASE_TEMPLATE (test_filled_like_vec, Vector, vector_types)
{
    Vector vec(3, arma::fill::ones);
    auto vec2 = filled_like(vec, 42);
    BOOST_TEST(arma::all(vec2 == 42));

    auto vec3 = zeros_like(vec);
    BOOST_TEST(arma::all(vec3 == 0));

    auto vec4 = ones_like(vec);
    BOOST_TEST(arma::all(vec4 == 1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE (test_filled_like_mat, Matrix, matrix_types)
{
    Matrix mat(3, 4, arma::fill::ones);
    auto mat2 = filled_like(mat, 42);
    BOOST_TEST(mat2.min() == 42);
    BOOST_TEST(mat2.max() == 42);

    auto mat3 = zeros_like(mat);
    BOOST_TEST(mat3.min() == 0);
    BOOST_TEST(mat3.max() == 0);

    auto mat4 = ones_like(mat);
    BOOST_TEST(mat4.min() == 1);
    BOOST_TEST(mat4.max() == 1);
}


BOOST_AUTO_TEST_SUITE_END() // "resize_operations"
