#define BOOST_TEST_MODULE test meta

#include <vector>
#include <tuple>
#include <functional>
#include <string>

#include <UtopiaUtils/utils/meta.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

using namespace Utopia::Utils::tags;


/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>() {}
};


/// Some argument types to test with
using exp_arg_types = std::tuple<arma::vec, int, float, uint, bool>;

/// A taggable function with deduced argument types and variable number of tags
/** \NOTE It fully ignores the value of its input arguments. The return value
  *       only decodes which path was taken through the function.
  */
template<class... tags, class T1, class T2>
std::size_t taggable_func (T1, T2) {
    std::cout << std::endl
              << "got " << size<tags...>() << " tags" << std::endl;
    std::size_t ret_v = (100 * size<tags...>());

    if constexpr (is_empty<tags...>()) {
        std::cout << "no tags given" << std::endl;
        ret_v += 10;
    }

    if constexpr (contains_type<deep_copy, tags...>()) {
        std::cout << "got tag: deep_copy" << std::endl;
        ret_v += 2;
    }

    if constexpr (contains_type<shallow_copy, tags...>()) {
        std::cout << "got tag: shallow_copy" << std::endl;
        ret_v += 1;
    }

    return ret_v;
}




// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BOOST_FIXTURE_TEST_SUITE (test_meta, Infrastructure)


/// Test the is_same_template meta function
BOOST_AUTO_TEST_CASE (test_is_same_template) {
    static_assert(is_same_template<std::vector, std::vector>());
    static_assert(is_same_template<Void1, Void1>());
    static_assert(is_same_template<Void2, Void2>());
    static_assert(not is_same_template<Void1, Void2>());
    static_assert(not is_same_template<std::vector, Void1>());
    static_assert(not is_same_template<std::vector, Void2>());
    static_assert(not is_same_template<std::vector, std::list>());
}

/// Test the contains_type meta function; indirectly tests has_type
BOOST_AUTO_TEST_CASE (test_contains_type) {
    static_assert(contains_type<double, double, float, int>());
    static_assert(not contains_type<double, float, int>());
}

/// Test the size-related meta functions
BOOST_AUTO_TEST_CASE (test_size) {
    static_assert(size<double, float, int>() == 3);
    static_assert(size<>() == 0);
    static_assert(size<double, double, double, double>() == 4);

    static_assert(is_empty<>());
    static_assert(not is_empty<double>());
    static_assert(not is_empty<double, float>());
}

/// Test taggable_func and related metaprogramming constructs
BOOST_AUTO_TEST_CASE_TEMPLATE (test_taggable_func, ArgType, exp_arg_types)
{
    const auto arg1 = ArgType();
    const auto arg2 = 0.;  // ... just as to have it different than arg1
    std::size_t res;

    // Should be invokable without template arguments
    BOOST_TEST(taggable_func(arg1, arg2) == 10);
    BOOST_TEST(taggable_func<>(arg2, arg1) == 10);

    // ... or with irrelevant or repeated types
    // NOTE Need to store the result separately due to Boost Macro stuff
    res = taggable_func<void>(arg1, arg2);
    BOOST_TEST(res == 1*100);

    res = taggable_func<void, void>(arg1, arg2);
    BOOST_TEST(res == 2*100);

    res = taggable_func<void, exclude, void, exclude>(arg1, arg2);
    BOOST_TEST(res == 4*100);

    // And with different number of (valid) tags
    res = taggable_func<shallow_copy>(arg1, arg2);
    BOOST_TEST(res == 1*100 + 1);

    res = taggable_func<shallow_copy, deep_copy>(arg1, arg2);
    BOOST_TEST(res == 2*100 + 1 + 2);

    res = taggable_func<shallow_copy, deep_copy, deep_copy, exclude>(arg1, arg2);
    BOOST_TEST(res == 4*100 + 1 + 2);
}


BOOST_AUTO_TEST_SUITE_END()  // utils/meta.hh test suite
