#define BOOST_TEST_MODULE test formatters

#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/formatters.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::Utils::tags;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("formatters.yml") {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BOOST_FIXTURE_TEST_SUITE (test_formatters, Infrastructure)

/// Tests the Config-type fmtlib formatter
BOOST_AUTO_TEST_CASE (type_Config) {
    log->info("Type: Config (i.e.: YAML Node)\n{}", cfg["formatters_Config"]);
}

BOOST_AUTO_TEST_SUITE_END()
