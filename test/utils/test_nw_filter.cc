#define BOOST_TEST_MODULE test nw_filter

#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/nw_filter.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::Utils::tags;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>() {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_nw_filters, Infrastructure)

BOOST_AUTO_TEST_CASE (construct_NetworkFilter) {
    using NW::FilterMode;
    using NW::NetworkFilter;

    // ... and define a predicate
    const auto keep_all = [](auto){ return true; };

    // Need some dummy network type and instance
    using Network = boost::adjacency_list<>;
    auto nw = Network();

    // Should be able to instantiate NetworkFilter and specialisations without
    // the need to specify the network template argument
    auto unfiltered = NetworkFilter<>::unfiltered(nw);
    auto edge_filter = NetworkFilter<>::edges(nw, keep_all);
    auto vertex_filter = NetworkFilter<>::vertices(nw, keep_all);
    auto network_filter = NetworkFilter(nw, keep_all, keep_all);

    BOOST_TEST((unfiltered.mode == FilterMode::unfiltered));
    BOOST_TEST((edge_filter.mode == FilterMode::edges_only));
    BOOST_TEST((vertex_filter.mode == FilterMode::vertices_only));
    BOOST_TEST((network_filter.mode == FilterMode::edges_and_vertices));

    BOOST_TEST(not unfiltered.has_edge_filter());
    BOOST_TEST(edge_filter.has_edge_filter());
    BOOST_TEST(not vertex_filter.has_edge_filter());
    BOOST_TEST(network_filter.has_edge_filter());

    BOOST_TEST(not unfiltered.has_vertex_filter());
    BOOST_TEST(not edge_filter.has_vertex_filter());
    BOOST_TEST(vertex_filter.has_vertex_filter());
    BOOST_TEST(network_filter.has_vertex_filter());
}

BOOST_AUTO_TEST_SUITE_END()
