#define BOOST_TEST_MODULE test graph_iterators

#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/graph_iterators.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::Utils::tags;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>() {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BOOST_FIXTURE_TEST_SUITE (test_graph_iterators, Infrastructure)

/// Test the FilteredRange itertor
BOOST_AUTO_TEST_CASE(test_FilteredRange) {
    using V = std::vector<int>;
    V v(10);
    std::iota(v.begin(), v.end(), 1);
    BOOST_TEST(v == V({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}),
               tt::per_element());

    // Some predicates
    auto keep_all = [](auto){ return true; };
    auto keep_none = [](auto){ return false; };
    auto mod_3 = [](auto e){ return e % 3 == 0; };

    // Unfiltered iteration
    {
    auto frg = make_filtered_range(keep_all, v.begin(), v.end());
    BOOST_TEST(get_iter_size(frg) == 10);
    BOOST_TEST(get_elements<int>(frg) == v, tt::per_element());
    }

    // Completely filtered iteration
    {
    auto frg = make_filtered_range(keep_none, v.begin(), v.end());
    BOOST_TEST(get_iter_size(frg) == 0);
    BOOST_TEST(get_elements<int>(frg) == V{}, tt::per_element());
    }

    // mod3-filtered
    {
    auto frg = make_filtered_range(mod_3, v.begin(), v.end());
    BOOST_TEST(get_iter_size(frg) == 3);
    BOOST_TEST(get_elements<int>(frg) == V({3, 6, 9}),
               tt::per_element());
    }

    // Manual iteration
    {
    auto frg = make_filtered_range(mod_3, v.begin(), v.end());
    BOOST_TEST(*frg == 3);
    ++frg;
    BOOST_TEST(*frg == 6);
    ++frg;
    BOOST_TEST(*frg == 9);
    ++frg;
    BOOST_TEST(frg == frg.end());
    }
    {
    auto frg = make_filtered_range(mod_3, v.begin(), v.end());
    BOOST_TEST(*frg == 3);
    frg++;
    BOOST_TEST(*frg == 6);
    frg++;
    BOOST_TEST(*frg == 9);
    frg++;
    BOOST_TEST(frg == frg.end());
    }

    // with const iterators as input
    {
    auto frg = make_filtered_range(mod_3, v.cbegin(), v.cend());
    BOOST_TEST(*frg == 3);
    ++frg;
    BOOST_TEST(*frg == 6);
    ++frg;
    BOOST_TEST(*frg == 9);
    ++frg;
    BOOST_TEST(frg == frg.end());
    }

    // in for loops
    for (auto e : make_filtered_range(mod_3, v.begin(), v.end())) {
        BOOST_TEST(mod_3(e));
    }
    for (const auto e : make_filtered_range(mod_3, v.begin(), v.end())) {
        BOOST_TEST(mod_3(e));
    }
    for (auto& e : make_filtered_range(mod_3, v.begin(), v.end())) {
        BOOST_TEST(mod_3(e));
    }
    for ([[maybe_unused]] auto e
         : make_filtered_range(keep_none, v.begin(), v.end()))
    {
        BOOST_FAIL("This point should not be reached!");
    }

    // type traits are those of an iterator
    {
    auto frg = make_filtered_range(mod_3, v.begin(), v.end());
    using FR = decltype(frg);

    static_assert(std::is_same<FR::value_type, int>());
    static_assert(std::is_same<FR::difference_type, std::ptrdiff_t>());
    static_assert(std::is_same<FR::pointer, int*>());
    static_assert(std::is_same<FR::reference, int&>());
    static_assert(std::is_same<FR::iterator_category, std::forward_iterator_tag>());
    }

    // Comparison operators are implicitly available (and correct)
    {
    auto frg1a = make_filtered_range(mod_3, v.begin(), v.end());
    auto frg1b = make_filtered_range(mod_3, v.begin(), v.end());
    auto frg2 = make_filtered_range(keep_all, v.begin(), v.end());

    BOOST_TEST(frg1a == frg1a);
    BOOST_TEST(frg1b == frg1b);
    BOOST_TEST(frg2 == frg2);

    BOOST_TEST(frg1a == frg1b);

    BOOST_TEST(frg1a != frg2);
    BOOST_TEST(frg1b != frg2);
    }
}


BOOST_AUTO_TEST_SUITE_END()
