#define BOOST_TEST_MODULE test action_manager

#include <utopia/core/types.hh>
#include <utopia/core/testtools.hh>
#include <utopia/data_io/cfg_utils.hh>

#include <UtopiaUtils/utils/action_manager.hh>

#include <boost/test/unit_test.hpp>
#include "../testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Utils;
using namespace Utopia::TestTools;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("action_manager.yml") {}
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_procedure, Infrastructure)

/// A MOCK map that never throws std::out_of_range but returns a default value
template<class RT>
struct DefaultMap {
    using key_type = std::string;

    using Func = std::function<RT(const Config&)>;
    Func default_value;

    std::unordered_map<std::string, Func> map = {};

    template<class Key>
    Func at (Key&& k) const {
        try {
            return map.at(k);
        }
        catch (std::out_of_range&) {
            return default_value;
        }
    }

    std::size_t size () const { return map.size() + 1; }
    auto begin () const { return map.begin(); }
    auto end () const { return map.end(); }
};

/// Tests construction of Procedure objects
BOOST_AUTO_TEST_CASE (construction) {
    test_config_callable([&](const Config& params){
        const auto proc = Procedure("test", params);

        // Test that basic parameters are carried over
        BOOST_TEST(proc.enabled == get_as<bool>("enabled", params, true));
        BOOST_TEST(proc.name == "test");
        BOOST_TEST(proc.description ==
                   get_as<std::string>("description", params, ""));
    },
    cfg["procedure"]["construction"],
    "Procedure Construction", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_CASE (interface) {
    test_config_callable([&](const Config& params){
        auto proc = Procedure("test", params);

        proc.enabled = false;
        BOOST_TEST(not proc.enabled);
        proc.enabled = true;
        BOOST_TEST(proc.enabled);

        // Some mock elements to test the public interface
        auto triggers = DefaultMap<bool>{[](const auto&){ return true; }};
        auto actions = DefaultMap<void>{[](const auto&){ return; }};
        BOOST_TEST(triggers.at("foo")(Config{}));
        actions.at("foo")(Config{});  // void

        // This should be carried out
        BOOST_TEST(proc.evaluate_condition(triggers));
        BOOST_TEST(proc(triggers, actions));

        // Disable the triggers; these should not perform any action now
        triggers.default_value = [](const auto&){ return false; };
        BOOST_TEST(not proc.evaluate_condition(triggers));
        BOOST_TEST(not proc(triggers, actions));
    },
    cfg["procedure"]["construction"],
    "Procedure Interface", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_CASE (evaluate_condition) {
    test_config_callable([&](const Config& params){
        auto proc = Procedure("test", params["init"]);

        // Mock actions map
        auto actions = DefaultMap<void>{[](const auto&){ return; }};
        actions.at("foo")(Config{});  // void

        // Triggers: use the ActionManager's static defaults
        auto triggers = ActionManager::default_triggers;
        triggers["from_params"] = [](const auto& cfg){
            return get_as<bool>("return", cfg);
        };

        // Evaluate the condition
        const bool expected = get_as<bool>("expected", params);
        BOOST_TEST(proc.evaluate_condition(triggers) == expected);
    },
    cfg["procedure"]["conditions"],
    "Procedure Condition Evaluation", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_CASE (missing_action_or_trigger) {
    auto init_params = cfg["procedure"]["construction"]["simple"]["params"];
    auto proc = Procedure("test", init_params);

    // Empty maps
    auto actions = ActionManager::ActionMap{};
    auto triggers = ActionManager::TriggerMap{};

    // Cannot evaluate triggers nor invoke actions because none are available
    check_exception<std::out_of_range>(
        [&](){
            proc.evaluate_condition(triggers);
        },
        "A trigger with name 'trigger1' was not found! Available triggers:",
        {__LINE__, __FILE__}
    );
    check_exception<std::out_of_range>(
        [&](){
            proc.perform_action_sequence(actions);
        },
        "An action with name 'foo' was not found! Available actions:",
        {__LINE__, __FILE__}
    );
}

BOOST_AUTO_TEST_SUITE_END() // "test_procedure"



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_action_manager, Infrastructure)

BOOST_AUTO_TEST_CASE (construction) {
    test_config_callable([&](const Config& params){
        auto am = ActionManager(log, params);

        // Always have default triggers and default procedure groups
        BOOST_TEST(contains_key(am.triggers(), "always"));
        BOOST_TEST(contains_key(am.triggers(), "never"));
        BOOST_TEST(contains_key(am.hooks(), "all"));

        // Check that procedure objects were created
        auto proc_specs = Config{};
        if (params["procedures"]) {
            proc_specs = params["procedures"];
        }
        BOOST_TEST(am.procedures().size() == proc_specs.size());

        // ... and were registered with hooks "all"
        BOOST_TEST(am.hooks().at("all").size() == proc_specs.size());
    },
    cfg["action_manager"]["construction"],
    "ActionManager Construction", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_CASE (action_and_trigger_registration) {
    test_config_callable([&](const Config& params){
        auto am = ActionManager(log, params);

        am.register_trigger("my_trigger", [](auto&){ return true; });
        am.register_action("my_action", [](auto&){ return; });

        BOOST_TEST(am.triggers().size() == 3);
        BOOST_TEST(am.actions().size() == 1);
    },
    cfg["action_manager"]["construction"],
    "ActionManager Action and Trigger Registration", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_CASE (invocation) {
    auto am = ActionManager(log, cfg["action_manager"]["invocation"]);

    std::string my_str = "initial";
    am.register_action("set_string_value",
        [&my_str](const Config& cfg) mutable {
            my_str = get_as<std::string>("new_value", cfg, my_str);
        }
    );
    BOOST_TEST(my_str == "initial");

    BOOST_TEST(am.perform_procedures({"one"}) == 1);
    BOOST_TEST(my_str == "one");

    BOOST_TEST(am.perform_procedures({"two"}) == 0);  // not triggered
    BOOST_TEST(my_str == "one");

    BOOST_TEST(am.perform_procedures({"one", "three"}) == 2);
    BOOST_TEST(my_str == "three");

    my_str = "initial";
    BOOST_TEST(am.invoke_hook("all") == 2);  // 'one' and 'three'
    BOOST_TEST(my_str != "initial");  // order is not guaranteed
}

BOOST_AUTO_TEST_CASE (action_defaults) {
    auto am = ActionManager(log, cfg["action_manager"]["action_defaults"]);

    std::string my_str = "initial";
    am.register_action("set_string_value",
        [&my_str](const Config& cfg) mutable {
            my_str = get_as<std::string>("new_value", cfg["sub"], my_str);
        }
    );
    BOOST_TEST(my_str == "initial");

    am.perform_procedures({"one"});
    BOOST_TEST(my_str == "from defaults");

    am.perform_procedures({"two"});
    BOOST_TEST(my_str == "updated action parameter");
}

BOOST_AUTO_TEST_SUITE_END() // "test_action_manager"
