# Model Building Tools

This project supplies a C++ library that extends [`Utopia`][Utopia]'s core and data I/O library.
Essentially, it is a test-bed for functionality that may at a later stage migrate into Utopia itself.

[[_TOC_]]


## Usage
🚧



## Developing
### Run tests locally
```bash
mkdir build
cd build
cmake ..
make test_all -j8
```



---

## Copyright Notice

    Utopia Model Building Tools
    Copyright (C) 2018 – 2022  Utopia developers

    The term "Utopia developers" refers to all direct contributors to this
    software package. A full list of copyright holders and information about
    individual contributions can be retrieved from the version-controlled
    development history of this software package.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

A copy of the [GNU General Public License Version 3][GPL], and the [GNU Lesser General Public License Version 3][LGPL] extending it, is distributed with the source code of this program.


## Copyright Holders

The copyright holders of this software package are collectively referred to as "Utopia developers" in the respective copyright notices.

* Harald Mack (@mackharald89)
* Lukas Riedel (@peanutfun)
* Yunus Sevinchan (@blsqr, *maintainer*)


<!-- start: links -->

[GPL]: https://www.gnu.org/licenses/gpl-3.0.en.html
[LGPL]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[Utopia]: https://gitlab.com/utopia-project/utopia
[Utopia-Project]: https://utopia-project.org/
